#import bevy_pbr::forward_io::VertexOutput
#import bevy_pbr::mesh_bindings::mesh
#import bevy_pbr::mesh_functions::{get_model_matrix, mesh_position_local_to_clip}
#import bevy_pbr::mesh_view_bindings::{view, globals}

struct CustomMaterial {
    color: vec4<f32>,
};

@group(1) @binding(0)
var<uniform> material: CustomMaterial;

@fragment
fn fragment(in: VertexOutput) -> @location(0) vec4<f32> {
    let uv = vec2<f32>(in.uv.x, in.uv.y);
    let center = vec2<f32>(0.5, 0.5);
    // Negative distance here so it goes *towards* the target
    let dist = -distance(uv, center);
    let outline_color = material.color;

    // Apply a sin function based on time to create an oscillating effect
    let ripple = sin(globals.time * 30.0 + dist * 50.0) * 0.5 + 0.5;

    // Create a new color by adjusting with the ripple effect
    var modified_color = vec4<f32>(outline_color.rgb * ripple, outline_color.a);

    // Fade out the further we are from the center
    let fade = 1.0 - dist;

    // Make the effect "blurry" by decreasing alpha based on the distance from center and ripple
    modified_color.a *= fade * ripple;

    return modified_color;
}

struct Vertex {
    @builtin(instance_index) instance_index: u32,
    @location(0) position: vec3<f32>,
    @location(1) normal: vec3<f32>,
    @location(2) uv: vec2<f32>,
};

@vertex
fn vertex(vertex: Vertex) -> VertexOutput {
    var out: VertexOutput;

    // unchanged
//    out.uv = vertex.uv;
//    out.position = mesh_position_local_to_clip(
//        get_model_matrix(vertex.instance_index),
//        vec4<f32>(vertex.position, 1.0),
//    );
//    return out;

    // Apply a sin function based on time and uv.y to create a ripple effect
    let ripple = sin(globals.time * -30.0 + vertex.uv.y * 20.0) * 0.05; // Adjust the multipliers for frequency and amplitude
    let displaced_position = vertex.position + vec3<f32>(vertex.normal.xy * ripple, 0.0);

    out.position = mesh_position_local_to_clip(
        get_model_matrix(vertex.instance_index),
        vec4<f32>(displaced_position, 1.0)
    );

    out.uv = vertex.uv;
    return out;
}
