#import bevy_pbr::forward_io::VertexOutput
#import bevy_pbr::mesh_view_bindings::view

struct CustomMaterial {
    color: vec4<f32>,
    thickness: f32,
    main_alpha: f32,
    outline_alpha: f32,
};

@group(1) @binding(0)
var<uniform> material: CustomMaterial;

@fragment
fn fragment(in: VertexOutput) -> @location(0) vec4<f32> {
    let uv = vec2<f32>(in.uv.x, in.uv.y);
    let box_size = vec2<f32>(0.5, 0.5);
    let d = sdBox(uv - vec2<f32>(0.5, 0.5), box_size);

    // Convert pixel thickness to normalized screen coordinates
    let adjusted_thickness = material.thickness / view.viewport.w; // Use the viewport height to normalize

    let is_inside_outline = d <= adjusted_thickness && d >= -adjusted_thickness;
    let alpha = select(material.main_alpha, material.outline_alpha, is_inside_outline);

    return vec4<f32>(material.color.rgb, alpha);
}

// An antialised outline that doesn't work the way i want it:
// it completely hides the inner (non-outline) part

//@fragment
//fn fragment(in: VertexOutput) -> @location(0) vec4<f32> {
//    let uv = vec2<f32>(in.uv.x, in.uv.y);
//    let box_size = vec2<f32>(0.5, 0.5);
//    let d = sdBox(uv - vec2<f32>(0.5, 0.5), box_size);
//
//    // Convert pixel thickness to normalized screen coordinates
//    let adjusted_thickness = material.thickness / view.viewport.w;
//
//    // Anti-aliasing range
//    let aa_range = 0.001; // Adjust for desired smoothness
//
//    // Calculate the distance to the edge for the main body and the outline
//    let edge_distance = abs(d) - adjusted_thickness;
//
//    // Apply smoothstep for anti-aliasing the outline
//    let outline_alpha = 1.0 - smoothstep(-aa_range, aa_range, edge_distance);
//
//    // Determine the alpha for the main shape
//    let main_alpha = 1.0 - step(0.0, edge_distance);
//
//    // Combine the two alphas
//    let final_alpha = max(outline_alpha * material.outline_alpha, main_alpha * material.main_alpha);
//
//    let is_inside_outline = d <= adjusted_thickness && d >= -adjusted_thickness;
//    let alpha = select(material.main_alpha, material.outline_alpha, is_inside_outline);
//
//    return vec4<f32>(material.color.rgb, final_alpha);
//}

fn sdBox(p: vec2<f32>, b: vec2<f32>) -> f32 {
    let d = abs(p) - b;
    return length(max(d, vec2<f32>(0.0, 0.0))) + min(max(d.x, d.y), 0.0);
}
