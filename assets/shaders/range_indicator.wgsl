#import bevy_pbr::forward_io::VertexOutput
#import bevy_pbr::mesh_view_bindings::view

struct CustomMaterial {
    color: vec4<f32>,
};

@group(1) @binding(0)
var<uniform> material: CustomMaterial;

@fragment
fn fragment(in: VertexOutput) -> @location(0) vec4<f32> {
    let uv = vec2<f32>(in.uv.x, in.uv.y);
    let center = vec2<f32>(0.5, 0.5);
    let dist = distance(uv, center);
    let outline_color = material.color;

    let distance_to_camera = abs(view.world_position.y - in.world_position.y);
    let adjusted_distance = max(distance_to_camera, 50.0);
    let outline_thickness = 0.03 / pow(adjusted_distance, 0.3);

    if (dist < 0.5 - outline_thickness || dist > 0.5 + outline_thickness) {
      discard;
    }

    return outline_color;
}
