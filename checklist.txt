scenes
  loading
  menu
  playing

resource
  gathering, via building
  consuming, purchasing buildings and units

tilegrid + passability grid
  terrain
    grass (passable)
    tree (unpassable)
  buildings (unpassable)
    hq
    resource gathering building
    unit spawning building

units
  soldier

ui
  movement
    mass select unit
    move unit
    attack on target
  building
    units
    buildings

collision
  one unit per tile  

pathfinding
  for movement  

combat
    dmg
    health

AI
  duno how to make AI, but somekind of behaviour enum ? (moving, attacking, idle)
  pathfinding

map / world
  generation
  exploration