use bevy::math::Vec3Swizzles;
use bevy::prelude::*;
use rand::distributions::{Distribution, Standard};
use rand::seq::SliceRandom;
use rand::Rng;

use crate::constants::{
    COST_BARRACKS, COST_GATHERER, COST_LASER_TOWER, COST_ROCKET_DUDE, COST_SOLDIER,
};
use crate::core::building::{Building, BuildingType, Construction, SpawnBuildingEvent};
use crate::core::collision::collide;
use crate::core::unit::{SpawnUnitEvent, UnitType};
use crate::core::{find_position_around_building, GameState, PlayingState};
use crate::player::{PaymentEvent, Player, PlayerAlignment};

pub struct RtsAiPlugin;

impl Plugin for RtsAiPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<AiTick>().add_systems(
            Update,
            (ai_timers, ai_tick)
                .run_if(in_state(GameState::InGame))
                .run_if(in_state(PlayingState::Playing)),
        );
    }
}

#[derive(Component)]
pub struct BrainContainer;

#[derive(Clone, Copy, Debug, Reflect)]
pub enum BuildingActionCategory {
    BuildRandomBuilding,
    BuildRandomUnit,
}

impl Distribution<BuildingActionCategory> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> BuildingActionCategory {
        match rng.gen_range(1..=5) {
            1 => BuildingActionCategory::BuildRandomBuilding,
            _ => BuildingActionCategory::BuildRandomUnit,
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, Reflect, PartialEq)]
pub enum BuildingAction {
    BuildGatherer,
    BuildBarracks,
    BuildLaserTower,
    BuildSoldier,
    BuildRocketDude,
}

impl BuildingAction {
    pub fn associated_resource_cost(&self) -> u32 {
        match self {
            BuildingAction::BuildGatherer => COST_GATHERER,
            BuildingAction::BuildBarracks => COST_BARRACKS,
            BuildingAction::BuildLaserTower => COST_LASER_TOWER,
            BuildingAction::BuildSoldier => COST_SOLDIER,
            BuildingAction::BuildRocketDude => COST_ROCKET_DUDE,
        }
    }

    pub fn building_type(&self) -> BuildingType {
        match self {
            BuildingAction::BuildGatherer => BuildingType::Gatherer,
            BuildingAction::BuildBarracks => BuildingType::Barracks,
            BuildingAction::BuildLaserTower => BuildingType::LaserTower,
            _ => BuildingType::Gatherer,
        }
    }

    pub fn unit_type(&self) -> UnitType {
        match self {
            BuildingAction::BuildSoldier => UnitType::Soldier,
            BuildingAction::BuildRocketDude => UnitType::RocketDude,
            _ => UnitType::Soldier,
        }
    }
}

impl BuildingActionCategory {
    pub fn possible_associated_actions(&self) -> Vec<BuildingAction> {
        match self {
            BuildingActionCategory::BuildRandomBuilding => vec![
                BuildingAction::BuildGatherer,
                BuildingAction::BuildLaserTower,
                BuildingAction::BuildBarracks,
            ],
            BuildingActionCategory::BuildRandomUnit => vec![
                BuildingAction::BuildSoldier,
                BuildingAction::BuildRocketDude,
            ],
        }
    }

    pub fn random_action(&self) -> BuildingAction {
        *self
            .possible_associated_actions()
            .choose(&mut rand::thread_rng())
            .unwrap()
    }
}

#[derive(Component, Debug, Reflect)]
pub struct Brain {
    action_category: BuildingActionCategory,
    next_action: BuildingAction,
    timer: Timer,
}

impl Default for Brain {
    fn default() -> Self {
        Self {
            action_category: BuildingActionCategory::BuildRandomBuilding,
            next_action: BuildingAction::BuildGatherer,
            timer: {
                let mut timer = Timer::from_seconds(4.0, TimerMode::Once);
                timer.set_elapsed(timer.duration());
                timer
            },
        }
    }
}

fn ai_timers(
    mut query: Query<(Entity, &mut Brain)>,
    time: Res<Time>,
    mut ai_tick_event_writer: EventWriter<AiTick>,
) {
    for (player_entity, mut brain) in query.iter_mut() {
        brain.timer.tick(time.delta());

        if brain.timer.just_finished() {
            brain.timer.reset();
            ai_tick_event_writer.send(AiTick { player_entity })
        }
    }
}

fn ai_tick(
    buildings_query: Query<(&Building, &PlayerAlignment, &Transform, Has<Construction>)>,
    mut player_query: Query<(Entity, &Player, &mut Brain)>,
    mut ai_tick_event_reader: EventReader<AiTick>,
    mut spawn_building_event_writer: EventWriter<SpawnBuildingEvent>,
    mut spawn_unit_event_writer: EventWriter<SpawnUnitEvent>,
    mut payment_event_writer: EventWriter<PaymentEvent>,
) {
    for ai_tick in ai_tick_event_reader.read() {
        // We don't delete player entities so we can unwrap
        let (player_entity, ai_player, mut brain) =
            player_query.get_mut(ai_tick.player_entity).unwrap();

        let player_buildings: Vec<(&Building, &PlayerAlignment, &Transform, bool)> =
            buildings_query
                .iter()
                .filter(|(_, player_alignment, _transform, _)| {
                    *player_alignment == &ai_player.player_alignment
                })
                .collect();

        let is_valid = ai_player.resource.amount > brain.next_action.associated_resource_cost()
            && !player_buildings.is_empty();

        if !is_valid {
            continue;
        }

        let maybe_barracks = buildings_query.iter().find(
            |(building, player_alignment, _transform, has_construction)| {
                *player_alignment == &ai_player.player_alignment
                    && ***building == BuildingType::Barracks
                    && !*has_construction
            },
        );
        match brain.next_action {
            BuildingAction::BuildBarracks
            | BuildingAction::BuildLaserTower
            | BuildingAction::BuildGatherer => {
                let mut attempts: u32 = 0;
                while attempts < 10 {
                    attempts += 1;
                    let random_building = player_buildings.choose(&mut rand::thread_rng());
                    if let Some((building, _player_alignment, transform, _)) = random_building {
                        let building_type = brain.next_action.building_type();
                        let combined_size = building.size().xz() + building_type.size().xz();
                        let result_size = combined_size.extend(building.size().y).xzy();
                        let position = find_position_around_building(transform, result_size);
                        if buildings_query
                            .iter()
                            .any(|(building, _alignment, transform, _)| {
                                collide(
                                    position,
                                    building_type.size().xz(),
                                    transform.translation,
                                    building.size().xz(),
                                )
                            })
                        {
                            continue;
                        }
                        spawn_building_event_writer.send(SpawnBuildingEvent {
                            position,
                            player_team: ai_player.player_team,
                            player_alignment: ai_player.player_alignment,
                            building_type,
                        });
                        payment_event_writer.send(PaymentEvent {
                            player_entity,
                            cost: brain.next_action.associated_resource_cost(),
                        });
                    }
                    break;
                }
            }
            BuildingAction::BuildSoldier | BuildingAction::BuildRocketDude => {
                if let Some((_building, _player_alignment, transform, _)) = maybe_barracks {
                    let unit_type = brain.next_action.unit_type();
                    let position = find_position_around_building(
                        transform,
                        unit_type.purchase_requirement().size(),
                    );

                    spawn_unit_event_writer.send(SpawnUnitEvent {
                        unit_type,
                        position,
                        player_alignment: ai_player.player_alignment,
                        player_team: ai_player.player_team,
                        is_controlled: true,
                        health: unit_type.health(),
                        target: Some(position),
                    });

                    payment_event_writer.send(PaymentEvent {
                        player_entity,
                        cost: unit_type.cost(),
                    });
                }
            }
        }

        if maybe_barracks.is_none() {
            brain.action_category = BuildingActionCategory::BuildRandomBuilding;
            brain.next_action = BuildingAction::BuildBarracks;
        } else {
            let mut rng = rand::thread_rng();
            let new_goal: BuildingActionCategory = rng.gen();
            brain.action_category = new_goal;
            brain.next_action = brain.action_category.random_action();
        }
    }
}

#[derive(Event)]
pub struct AiTick {
    player_entity: Entity,
}
