use std::time::Duration;

use bevy::prelude::*;
use bevy_kira_audio::{AudioApp, AudioChannel, AudioControl, AudioPlugin, AudioSource};

use crate::camera::MainCamera;
use crate::combat::{DamageType, Weapon};
use crate::core::GameState;
use crate::ui::UI_PANEL_WIDTH;

pub const ATTACK_SOUND_COOLDOWN: f32 = 0.1;
pub const DAMAGE_SOUND_COOLDOWN: f32 = 0.17;
pub const DEATH_SOUND_COOLDOWN: f32 = 0.23;
// sound volume is relative to a channel's volume, i.e. default value is "1.0"
pub const DEATH_SOUND_VOLUME: f64 = 0.4;
pub const MAIN_CHANNEL_VOLUME: f64 = 0.2;
pub const BGM_CHANNEL_VOLUME: f64 = 0.5;
pub const UI_CHANNEL_VOLUME: f64 = 0.2;
pub const MAX_SOUND_DISTANCE: f32 = 500.0;

pub struct RtsAudioPlugin;

#[derive(Resource)]
pub struct BgmChannel;

#[derive(Resource)]
pub struct MainChannel;

#[derive(Resource)]
pub struct UiChannel;

#[derive(Event)]
pub struct AttackSoundEvent {
    pub attack_type: Weapon,
    pub position: Vec3,
}

#[derive(Event)]
pub struct DamageSoundEvent {
    pub damage_type: DamageType,
    pub position: Vec3,
}

#[derive(Event)]
pub struct DeathSoundEvent {
    pub position: Vec3,
}

#[derive(Event)]
pub struct ErrorSoundEvent;

#[derive(Resource)]
pub struct AttackSoundState {
    laser_timer: Timer,
    rocket_timer: Timer,
}

#[derive(Resource)]
pub struct DamageSoundState {
    timer: Timer,
}

#[derive(Resource)]
pub struct DeathSoundState {
    timer: Timer,
}

impl AttackSoundState {
    fn new() -> Self {
        let mut sound_state = AttackSoundState {
            laser_timer: Timer::from_seconds(ATTACK_SOUND_COOLDOWN, TimerMode::Once),
            rocket_timer: Timer::from_seconds(ATTACK_SOUND_COOLDOWN, TimerMode::Once),
        };

        sound_state
            .laser_timer
            .set_elapsed(Duration::from_secs_f32(ATTACK_SOUND_COOLDOWN));
        sound_state
            .rocket_timer
            .set_elapsed(Duration::from_secs_f32(ATTACK_SOUND_COOLDOWN));

        sound_state
    }
}

impl DamageSoundState {
    fn new() -> Self {
        let mut sound_state = DamageSoundState {
            timer: Timer::from_seconds(DAMAGE_SOUND_COOLDOWN, TimerMode::Once),
        };

        sound_state
            .timer
            .set_elapsed(Duration::from_secs_f32(DAMAGE_SOUND_COOLDOWN));

        sound_state
    }
}

impl DeathSoundState {
    fn new() -> Self {
        let mut sound_state = DeathSoundState {
            timer: Timer::from_seconds(DEATH_SOUND_COOLDOWN, TimerMode::Once),
        };

        sound_state
            .timer
            .set_elapsed(Duration::from_secs_f32(DEATH_SOUND_COOLDOWN));

        sound_state
    }
}

#[derive(Resource)]
pub struct AudioState {
    pub bgm_handle: Handle<AudioSource>,
    laser_handle: Handle<AudioSource>,
    rocket_handle: Handle<AudioSource>,
    error_handle: Handle<AudioSource>,
    explosion_handle: Handle<AudioSource>,
    death_handle: Handle<AudioSource>,
}

#[derive(Resource)]
pub struct AudioConfig {
    bgm_volume: f64,
    is_muted: bool,
}

impl Default for AudioConfig {
    fn default() -> Self {
        AudioConfig {
            bgm_volume: BGM_CHANNEL_VOLUME,
            is_muted: true,
        }
    }
}

impl Plugin for RtsAudioPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins(AudioPlugin)
            .add_event::<AttackSoundEvent>()
            .add_event::<DamageSoundEvent>()
            .add_event::<DeathSoundEvent>()
            .add_event::<ErrorSoundEvent>()
            .add_audio_channel::<MainChannel>()
            .add_audio_channel::<BgmChannel>()
            .add_audio_channel::<UiChannel>()
            .init_resource::<AudioConfig>()
            .add_systems(PreStartup, load_audio)
            .add_systems(OnEnter(GameState::InGame), reset_audio)
            .add_systems(OnExit(GameState::InGame), stop_playing_bgm)
            .add_systems(
                Update,
                (
                    play_attack_sound,
                    play_damage_sound,
                    play_death_sound,
                    bgm_volume_control,
                    play_error_sound,
                )
                    .run_if(in_state(GameState::InGame)),
            );
    }
}

fn bgm_volume_control(
    keyboard: Res<Input<KeyCode>>,
    bgm_channel: Res<AudioChannel<BgmChannel>>,
    mut audio_config: ResMut<AudioConfig>,
) {
    if keyboard.just_pressed(KeyCode::M) {
        if audio_config.is_muted {
            bgm_channel.set_volume(audio_config.bgm_volume);
        } else {
            bgm_channel.set_volume(0.0);
        }

        audio_config.is_muted = !audio_config.is_muted;
    }
}

fn load_audio(mut commands: Commands, assets: Res<AssetServer>) {
    let bgm_handle = assets.load("sounds/ambience.ogg");
    let laser_handle = assets.load("sounds/laserSmall_002.ogg");
    let rocket_handle = assets.load("sounds/26 - Gun, Anti Tank, Single Blast.wav");
    let error_handle = assets.load("sounds/error_005.ogg");
    let explosion_handle = assets.load("sounds/24 - Grenade Launcher, Contained Blast.wav");
    let death_handle = assets.load("sounds/2scream.wav");

    commands.insert_resource(AudioState {
        bgm_handle,
        laser_handle,
        rocket_handle,
        error_handle,
        explosion_handle,
        death_handle,
    });

    commands.insert_resource(AttackSoundState::new());
    commands.insert_resource(DamageSoundState::new());
    commands.insert_resource(DeathSoundState::new());
}

fn reset_audio(
    audio: Res<AudioState>,
    mut audio_config: ResMut<AudioConfig>,
    bgm_channel: Res<AudioChannel<BgmChannel>>,
    main_channel: Res<AudioChannel<MainChannel>>,
    ui_channel: Res<AudioChannel<UiChannel>>,
) {
    *audio_config = AudioConfig::default();

    bgm_channel.set_volume(0.0);
    main_channel.set_volume(MAIN_CHANNEL_VOLUME);
    ui_channel.set_volume(UI_CHANNEL_VOLUME);

    // Start playing it pre-muted!
    bgm_channel.play(audio.bgm_handle.clone()).looped();
}

fn stop_playing_bgm(bgm_channel: Res<AudioChannel<BgmChannel>>) {
    bgm_channel.stop();
}

fn play_attack_sound(
    mut attack_sound_event_reader: EventReader<AttackSoundEvent>,
    main_channel: Res<AudioChannel<MainChannel>>,
    audio: Res<AudioState>,
    mut attack_sound_state: ResMut<AttackSoundState>,
    time: Res<Time>,
    camera_query: Query<(&GlobalTransform, &Camera), With<MainCamera>>,
    window_query: Query<&Window>,
) {
    attack_sound_state.laser_timer.tick(time.delta());
    attack_sound_state.rocket_timer.tick(time.delta());

    for ev in attack_sound_event_reader.read() {
        let (camera_transform, camera) = camera_query.single();
        let window = window_query.get_single().unwrap();
        let volume = get_volume_from_distance(window, camera, camera_transform, ev.position);
        let panning = get_panning_with_ui(window, camera, camera_transform, ev.position);

        match ev.attack_type {
            Weapon::LaserEmitter { .. } => {
                if attack_sound_state.laser_timer.finished() {
                    attack_sound_state.laser_timer.reset();
                    main_channel
                        .play(audio.laser_handle.clone())
                        .with_volume(volume)
                        .with_panning(panning);
                }
            }
            Weapon::RocketLauncher { .. } => {
                if attack_sound_state.rocket_timer.finished() {
                    attack_sound_state.rocket_timer.reset();
                    main_channel
                        .play(audio.rocket_handle.clone())
                        .with_volume(volume)
                        .with_panning(panning);
                }
            }
        }
    }
}

fn get_volume_from_distance(
    window: &Window,
    camera: &Camera,
    camera_transform: &GlobalTransform,
    position: Vec3,
) -> f64 {
    let mut camera_pos = camera_transform.translation();
    let x = (window.width() - UI_PANEL_WIDTH) / window.width() - 1.0;
    let world_pos = camera
        .ndc_to_world(camera_transform, Vec3::new(x, 0.0, 1.0))
        .unwrap();
    camera_pos.x = world_pos.x;

    let distance = camera_pos.distance(position);
    let volume = 1.0 - (distance / MAX_SOUND_DISTANCE) as f64;
    volume.max(0.05)
}

fn get_panning(camera: &Camera, camera_transform: &GlobalTransform, position: Vec3) -> Option<f64> {
    let ndc = camera.world_to_ndc(camera_transform, position)?;
    Some((ndc.x as f64 / 2.0 + 0.5).clamp(0.0, 1.0))
}

fn get_panning_with_ui(
    window: &Window,
    camera: &Camera,
    camera_transform: &GlobalTransform,
    position: Vec3,
) -> f64 {
    let game_width_ratio = (window.width() / (window.width() - UI_PANEL_WIDTH)) as f64;
    let panning = get_panning(camera, camera_transform, position).unwrap_or(0.5);
    (panning * game_width_ratio).clamp(0.0, 1.0)
}

fn play_damage_sound(
    mut damage_sound_event_reader: EventReader<DamageSoundEvent>,
    main_channel: Res<AudioChannel<MainChannel>>,
    audio: Res<AudioState>,
    mut damage_sound_state: ResMut<DamageSoundState>,
    time: Res<Time>,
    camera_query: Query<(&GlobalTransform, &Camera), With<MainCamera>>,
    window_query: Query<&Window>,
) {
    damage_sound_state.timer.tick(time.delta());

    for ev in damage_sound_event_reader.read() {
        let (camera_transform, camera) = camera_query.single();
        let window = window_query.get_single().unwrap();
        let volume = get_volume_from_distance(window, camera, camera_transform, ev.position);
        let panning = get_panning_with_ui(window, camera, camera_transform, ev.position);

        match ev.damage_type {
            DamageType::Explosive => {
                if damage_sound_state.timer.finished() {
                    damage_sound_state.timer.reset();
                    main_channel
                        .play(audio.explosion_handle.clone())
                        .with_volume(volume)
                        .with_panning(panning);
                }
            }
            _ => {}
        }
    }
}

fn play_death_sound(
    mut death_sound_event_reader: EventReader<DeathSoundEvent>,
    main_channel: Res<AudioChannel<MainChannel>>,
    audio: Res<AudioState>,
    mut death_sound_state: ResMut<DeathSoundState>,
    time: Res<Time>,
    camera_query: Query<(&GlobalTransform, &Camera), With<MainCamera>>,
    window_query: Query<&Window>,
) {
    death_sound_state.timer.tick(time.delta());

    for ev in death_sound_event_reader.read() {
        if death_sound_state.timer.finished() {
            let (camera_transform, camera) = camera_query.single();
            let window = window_query.get_single().unwrap();
            let volume = get_volume_from_distance(window, camera, camera_transform, ev.position)
                * DEATH_SOUND_VOLUME;
            let panning = get_panning_with_ui(window, camera, camera_transform, ev.position);

            death_sound_state.timer.reset();
            main_channel
                .play(audio.death_handle.clone())
                .with_volume(volume)
                .with_panning(panning);
        }
    }
}

fn play_error_sound(
    mut error_sound_events: EventReader<ErrorSoundEvent>,
    ui_channel: Res<AudioChannel<UiChannel>>,
    audio: Res<AudioState>,
) {
    for _ev in error_sound_events.read() {
        ui_channel.play(audio.error_handle.clone());
    }
}
