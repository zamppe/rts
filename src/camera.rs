use bevy::input::mouse::MouseWheel;
use bevy::prelude::*;
use bevy::window::{CursorGrabMode, PrimaryWindow};
use bevy_flycam::prelude::*;
use bevy_tweening::component_animator_system;

use crate::constants::{CAMERA_SPEED, CAMERA_ZOOM_STEP_SCROLL, MAX_FOV, MIN_FOV};
use crate::core::GameState;
use crate::graphics::animations::{camera_fov_animator, camera_transform_animator};
use crate::input::mouse::{MousePanHorizontalDirection, MousePanState, MousePanVerticalDirection};

pub struct RtsCameraPlugin;

impl Plugin for RtsCameraPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins(NoCameraPlayerPlugin)
            .insert_resource(MovementSettings {
                sensitivity: 0.00006, // default: 0.00012
                speed: 48.0,          // default: 12.0
            })
            .add_systems(
                Update,
                (
                    move_camera_kb,
                    zoom_camera_mouse,
                    move_camera_mouse,
                    reset_camera_kb,
                    component_animator_system::<Projection>,
                )
                    .run_if(in_state(GameState::InGame)),
            );
    }
}

#[derive(Component)]
pub struct MainCamera;

fn move_camera_kb(
    mut camera_query: Query<&mut Transform, With<MainCamera>>,
    keyboard: Res<Input<KeyCode>>,
    time: Res<Time>,
) {
    if !keyboard.any_pressed([
        KeyCode::Up,
        KeyCode::Down,
        KeyCode::Right,
        KeyCode::Left,
        KeyCode::NumpadAdd,
        KeyCode::NumpadSubtract,
        KeyCode::PageUp,
        KeyCode::PageDown,
    ]) {
        return;
    }

    let mut camera_transform = camera_query.single_mut();
    let mut velocity = Vec3::ZERO;
    let local_z = camera_transform.local_z();
    let forward = -Vec3::new(local_z.x, 0., local_z.z);
    let right = Vec3::new(local_z.z, 0., -local_z.x);
    if keyboard.pressed(KeyCode::Up) {
        velocity += forward;
    }
    if keyboard.pressed(KeyCode::Down) {
        velocity -= forward;
    }
    if keyboard.pressed(KeyCode::Right) {
        velocity += right;
    }
    if keyboard.pressed(KeyCode::Left) {
        velocity -= right;
    }

    let current_height = camera_transform.translation.y.clamp(10.0, 1000.0);
    let height_factor = current_height / INITIAL_CAMERA_TRANSLATION.y;

    velocity = velocity.normalize_or_zero();

    // Avoid change detection
    if velocity != Vec3::ZERO {
        camera_transform.translation +=
            velocity * time.delta_seconds() * CAMERA_SPEED * height_factor;
    }

    if keyboard.pressed(KeyCode::NumpadAdd) {
        let forward = camera_transform.forward();
        camera_transform.translation += forward * time.delta_seconds() * CAMERA_SPEED;
    }
    if keyboard.pressed(KeyCode::NumpadSubtract) {
        let back = camera_transform.back();
        camera_transform.translation += back * time.delta_seconds() * CAMERA_SPEED;
    }

    if keyboard.pressed(KeyCode::PageUp) {
        camera_transform.rotate_y(1.0 * time.delta_seconds());
    }
    if keyboard.pressed(KeyCode::PageDown) {
        camera_transform.rotate_y(-1.0 * time.delta_seconds());
    }
}

fn zoom_camera_mouse(
    mut commands: Commands,
    camera_query: Query<(Entity, &Projection), With<MainCamera>>,
    mut mouse_wheel_event_reader: EventReader<MouseWheel>,
) {
    for ev in mouse_wheel_event_reader.read() {
        let (entity, projection) = camera_query.single();
        if let Projection::Perspective(ref proj) = projection {
            let new_fov = if ev.y < 0.0 {
                (proj.fov * CAMERA_ZOOM_STEP_SCROLL).clamp(MIN_FOV, MAX_FOV)
            } else {
                (proj.fov / CAMERA_ZOOM_STEP_SCROLL).clamp(MIN_FOV, MAX_FOV)
            };

            let animator = camera_fov_animator(proj.fov, new_fov, 100);
            commands.entity(entity).insert(animator);
        }
    }
}

const INITIAL_CAMERA_TRANSLATION: Vec3 = Vec3::new(0.0, 170.0, 80.0);

pub fn get_default_camera_transform() -> Transform {
    Transform::from_translation(INITIAL_CAMERA_TRANSLATION).looking_at(Vec3::ZERO, Vec3::Y)
}

fn get_camera_transform_based_on_translation(translation: Vec3) -> Transform {
    Transform::from_translation(translation)
        .looking_at(translation - INITIAL_CAMERA_TRANSLATION, Vec3::Y)
}

fn reset_camera_kb(
    mut commands: Commands,
    camera_query: Query<(Entity, &Transform, &Projection), With<MainCamera>>,
    kb: Res<Input<KeyCode>>,
) {
    if kb.just_pressed(KeyCode::Home) {
        let (camera, transform, projection) = camera_query.single();

        if let Projection::Perspective(ref proj) = projection {
            let new_fov = PerspectiveProjection::default().fov;
            let animator = camera_fov_animator(proj.fov, new_fov, 200);
            commands.entity(camera).insert(animator);
        }

        let new_transform = if kb.any_pressed([KeyCode::ControlLeft, KeyCode::ControlRight]) {
            get_default_camera_transform()
        } else {
            let mut new_translation = transform.translation;
            new_translation.y = INITIAL_CAMERA_TRANSLATION.y;
            get_camera_transform_based_on_translation(new_translation)
        };

        let animator = camera_transform_animator(*transform, new_transform, 200);
        commands.entity(camera).insert(animator);
    }
}

fn move_camera_mouse(
    mut camera_query: Query<&mut Transform, With<MainCamera>>,
    time: Res<Time>,
    mouse_pan_state: Res<MousePanState>,
    primary_window: Query<&Window, With<PrimaryWindow>>,
) {
    if let Ok(window) = primary_window.get_single() {
        if let CursorGrabMode::Confined = window.cursor.grab_mode {
            return;
        }
    }

    let mut camera_transform = camera_query.single_mut();
    let mut velocity = Vec3::ZERO;
    let local_z = camera_transform.local_z();
    let forward = -Vec3::new(local_z.x, 0., local_z.z);
    let right = Vec3::new(local_z.z, 0., -local_z.x);

    match mouse_pan_state.horizontal {
        MousePanHorizontalDirection::None => {}
        MousePanHorizontalDirection::Left => {
            velocity -= right;
        }
        MousePanHorizontalDirection::Right => {
            velocity += right;
        }
    }

    match mouse_pan_state.vertical {
        MousePanVerticalDirection::None => {}
        MousePanVerticalDirection::Top => {
            velocity += forward;
        }
        MousePanVerticalDirection::Bottom => {
            velocity -= forward;
        }
    }

    let current_height = camera_transform.translation.y.clamp(10.0, 1000.0);
    let height_factor = current_height / INITIAL_CAMERA_TRANSLATION.y;

    velocity = velocity.normalize_or_zero();

    // Avoid change detection
    if velocity != Vec3::ZERO {
        camera_transform.translation +=
            velocity * time.delta_seconds() * CAMERA_SPEED * 3.0 * height_factor
    }
}
