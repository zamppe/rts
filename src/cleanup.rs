use std::borrow::Cow;

use bevy::prelude::*;

use crate::core::{GameState, PlayingState};

pub struct CleanupPlugin;

impl Plugin for CleanupPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            OnExit(GameState::MainMenu),
            cleanup_system::<CleanupMainMenu>,
        )
        .add_systems(OnExit(GameState::Loading), cleanup_system::<CleanupLoading>)
        .add_systems(OnExit(GameState::InGame), cleanup_system::<CleanupInGame>)
        .add_systems(
            OnExit(GameState::GameOver),
            cleanup_system::<CleanupGameOver>,
        )
        .add_systems(
            OnExit(PlayingState::Paused),
            cleanup_system::<CleanupPaused>,
        );
    }
}

#[derive(Bundle)]
pub struct CleanupBundle<T: Cleanup + Component> {
    name: Name,
    cleanup: T,
}

impl CleanupBundle<CleanupMainMenu> {
    pub fn main_menu(name: impl Into<Cow<'static, str>>) -> Self {
        CleanupBundle {
            name: Name::new(name),
            cleanup: CleanupMainMenu,
        }
    }
}

impl CleanupBundle<CleanupLoading> {
    pub fn loading(name: impl Into<Cow<'static, str>>) -> Self {
        CleanupBundle {
            name: Name::new(name),
            cleanup: CleanupLoading,
        }
    }
}

impl CleanupBundle<CleanupInGame> {
    pub fn in_game(name: impl Into<Cow<'static, str>>) -> Self {
        CleanupBundle {
            name: Name::new(name),
            cleanup: CleanupInGame,
        }
    }
}

impl CleanupBundle<CleanupPaused> {
    pub fn paused(name: impl Into<Cow<'static, str>>) -> Self {
        CleanupBundle {
            name: Name::new(name),
            cleanup: CleanupPaused,
        }
    }
}

impl CleanupBundle<CleanupGameOver> {
    pub fn game_over(name: impl Into<Cow<'static, str>>) -> Self {
        CleanupBundle {
            name: Name::new(name),
            cleanup: CleanupGameOver,
        }
    }
}

pub trait Cleanup {}

macro_rules! impl_cleanup {
    ($($t:ty),*) => {
        $(impl Cleanup for $t {})*
    };
}

#[derive(Component)]
pub struct CleanupMainMenu;
#[derive(Component)]
pub struct CleanupLoading;
#[derive(Component)]
pub struct CleanupInGame;
#[derive(Component)]
pub struct CleanupPaused;
#[derive(Component)]
pub struct CleanupGameOver;

impl_cleanup!(
    CleanupMainMenu,
    CleanupLoading,
    CleanupInGame,
    CleanupPaused,
    CleanupGameOver
);

fn cleanup_system<T: Component>(mut commands: Commands, query: Query<Entity, With<T>>) {
    for entity in query.iter() {
        commands.entity(entity).despawn_recursive();
    }
}
