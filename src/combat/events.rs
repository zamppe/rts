use bevy::prelude::*;
use bevy::utils::HashSet;

use crate::audio::{AttackSoundEvent, DamageSoundEvent, DeathSoundEvent};
use crate::combat::hitscan::CreateLaserBeamEvent;
use crate::combat::projectiles::{CreateProjectileEvent, ProjectileType};
use crate::combat::{AttackTarget, Combat, DamageType, Weapon};
use crate::core::{Collider, DespawnEvent, Health};
use crate::player::PlayerAlignment;

#[derive(Event)]
pub struct DeathEvent {
    entity: Entity,
    position: Vec3,
}

#[derive(Event)]
pub struct AttackEvent {
    pub attacker: Entity,
    pub target: Entity,
}

#[derive(Event)]
pub struct DamageReceivedEvent {
    pub target: Entity,
    pub damage: i32,
    pub damage_type: DamageType,
}

pub fn handle_attack_events(
    mut attack_event_reader: EventReader<AttackEvent>,
    mut damage_event_writer: EventWriter<DamageReceivedEvent>,
    mut projectile_event_writer: EventWriter<CreateProjectileEvent>,
    mut attack_sound_event_writer: EventWriter<AttackSoundEvent>,
    mut laser_beam_event_writer: EventWriter<CreateLaserBeamEvent>,
    mut attacker_query: Query<(&mut Combat, &Transform, &PlayerAlignment)>,
    target_query: Query<(&Transform, &Collider), With<Health>>,
) {
    let mut laser_attack_position: Option<Vec3> = None;
    let mut rocket_attack_position: Option<Vec3> = None;
    for ev in attack_event_reader.read() {
        if let Ok((target_transform, target_collider)) = target_query.get(ev.target) {
            // println!("{:?} is attacking {:?}", ev.attacker, ev.target);
            if let Ok((mut attacker_combat, attacker_transform, attacker_alignment)) =
                attacker_query.get_mut(ev.attacker)
            {
                let attacker_start_point =
                    attacker_combat.get_shooting_point_with_transform(*attacker_transform);
                let target_contact_point =
                    target_collider.get_center_with_transform(*target_transform);
                let distance = target_contact_point.distance(attacker_start_point);

                attacker_combat.attack_timer.reset();
                match attacker_combat.weapon {
                    Weapon::LaserEmitter {
                        beam_width,
                        light_power,
                    } => {
                        laser_attack_position = Some(attacker_start_point);

                        let transform = Transform::from_translation(attacker_start_point)
                            .with_scale(Vec3::new(beam_width, beam_width, distance))
                            .looking_at(target_contact_point, Vec3::Y);
                        laser_beam_event_writer.send(CreateLaserBeamEvent {
                            transform,
                            attacker_alignment: *attacker_alignment,
                            light_power,
                        });

                        damage_event_writer.send(DamageReceivedEvent {
                            target: ev.target,
                            damage: attacker_combat.damage,
                            damage_type: DamageType::Energy,
                        });
                    }
                    Weapon::RocketLauncher {
                        rocket_size,
                        rocket_speed,
                        rocket_turn_speed,
                    } => {
                        rocket_attack_position = Some(attacker_start_point);
                        projectile_event_writer.send(CreateProjectileEvent {
                            proj_type: ProjectileType::Homing,
                            position: attacker_start_point,
                            target: ev.target,
                            damage: attacker_combat.damage,
                            speed: rocket_speed,
                            size: rocket_size,
                            turn_speed: rocket_turn_speed,
                        });
                    }
                }
            }
        }
    }

    // If there are multiple attacks during the same frame, only send one event per attack type
    if let Some(position) = laser_attack_position {
        attack_sound_event_writer.send(AttackSoundEvent {
            attack_type: Weapon::default_laser_emitter(),
            position,
        });
    }
    if let Some(position) = rocket_attack_position {
        attack_sound_event_writer.send(AttackSoundEvent {
            attack_type: Weapon::default_rocket_launcher(),
            position,
        })
    }
}

pub fn handle_damage_events(
    mut damage_event_reader: EventReader<DamageReceivedEvent>,
    mut death_event_writer: EventWriter<DeathEvent>,
    mut damage_sound_event_writer: EventWriter<DamageSoundEvent>,
    mut target_query: Query<(&mut Health, &Transform)>,
) {
    let mut explosive_hit_position: Option<Vec3> = None;
    for ev in damage_event_reader.read() {
        if let Ok((mut target_health, target_transform)) = target_query.get_mut(ev.target) {
            if target_health.current <= 0 {
                // Avoid sending multiple death events
                continue;
            }

            if let DamageType::Explosive = ev.damage_type {
                explosive_hit_position = Some(target_transform.translation);
            }

            target_health.current -= ev.damage;
            if target_health.current <= 0 {
                // println!("Target died");
                death_event_writer.send(DeathEvent {
                    entity: ev.target,
                    position: target_transform.translation,
                });
            }
        }
    }

    if let Some(position) = explosive_hit_position {
        damage_sound_event_writer.send(DamageSoundEvent {
            damage_type: DamageType::Explosive,
            position,
        });
    }
}

pub fn handle_death_events(
    mut commands: Commands,
    mut death_event_reader: EventReader<DeathEvent>,
    mut death_sound_event_writer: EventWriter<DeathSoundEvent>,
    mut despawn_event_writer: EventWriter<DespawnEvent>,
    attack_target_query: Query<(Entity, &AttackTarget)>,
) {
    let mut dead_entities: HashSet<Entity> = HashSet::new();
    let mut dead_position: Option<Vec3> = None;
    for death_event in death_event_reader.read() {
        dead_entities.insert(death_event.entity);
        dead_position = Some(death_event.position);
    }

    if !dead_entities.is_empty() {
        // Clean up all AttackTargets in one loop
        for (attacker, target) in attack_target_query.iter() {
            if dead_entities.contains(&target.target) {
                commands.entity(attacker).remove::<AttackTarget>();
            }
        }

        if let Some(position) = dead_position {
            death_sound_event_writer.send(DeathSoundEvent { position });
        }

        despawn_event_writer.send(DespawnEvent {
            entities: dead_entities,
        });
    }
}
