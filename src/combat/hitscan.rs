use bevy::pbr::{NotShadowCaster, NotShadowReceiver};
use bevy::prelude::*;

use crate::cleanup::CleanupBundle;
use crate::graphics::materials::CommonMaterials;
use crate::graphics::Models;
use crate::player::PlayerAlignment;

#[derive(Component)]
pub struct LaserBeam {
    timer: Timer,
}

#[derive(Component)]
struct LaserLight;

#[derive(Event)]
pub struct CreateLaserBeamEvent {
    pub transform: Transform,
    pub attacker_alignment: PlayerAlignment,
    pub light_power: f32,
}

pub fn handle_create_laser_beam_events(
    mut cmds: Commands,
    mut events: EventReader<CreateLaserBeamEvent>,
    models: Res<Models>,
    common_materials: Res<CommonMaterials>,
) {
    for ev in events.read() {
        let light = cmds
            .spawn((
                PointLightBundle {
                    point_light: PointLight {
                        color: ev.attacker_alignment.color(),
                        intensity: 100.0 * ev.light_power,
                        range: 4.0 * ev.light_power,
                        radius: 1.0,
                        // shadows_enabled: true,
                        ..default()
                    },
                    ..default()
                },
                LaserLight,
                Name::new("Laser light"),
            ))
            .id();

        let material = match ev.attacker_alignment {
            PlayerAlignment::Green => common_materials.green_laser_beam.clone(),
            PlayerAlignment::Red => common_materials.red_laser_beam.clone(),
            PlayerAlignment::Blue => common_materials.blue_laser_beam.clone(),
        };

        cmds.spawn((
            MaterialMeshBundle {
                mesh: models.laser_beam.clone(),
                material,
                transform: ev.transform,
                ..default()
            },
            NotShadowCaster,
            NotShadowReceiver,
            CleanupBundle::in_game("LaserBeam"),
            LaserBeam {
                timer: Timer::from_seconds(0.35, TimerMode::Once),
            },
        ))
        .add_child(light);
    }
}

pub fn update_laser_beams(
    mut commands: Commands,
    mut laser_query: Query<(Entity, &mut LaserBeam)>,
    time: Res<Time>,
) {
    for (entity, mut laser) in laser_query.iter_mut() {
        laser.timer.tick(time.delta());
        if laser.timer.just_finished() {
            commands.entity(entity).despawn_recursive();
        }
    }
}
