use bevy::prelude::*;

use events::{
    handle_attack_events, handle_damage_events, handle_death_events, AttackEvent,
    DamageReceivedEvent, DeathEvent,
};
use hitscan::update_laser_beams;
use projectiles::CreateProjectileEvent;

use crate::combat::hitscan::{handle_create_laser_beam_events, CreateLaserBeamEvent};
use crate::combat::projectiles::update_rocket_projectiles;
use crate::core::movement::{Movement, StartMovementEvent};
use crate::core::turn::Turn;
use crate::core::{Collider, GameState, Health, PlayingState, Target};
use crate::player::PlayerTeam;

pub mod events;
pub mod hitscan;
pub mod projectiles;

#[derive(Resource, Default)]
pub struct AutoattackState(pub bool);

pub struct RtsCombatPlugin;

impl Plugin for RtsCombatPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<DeathEvent>()
            .init_resource::<AutoattackState>()
            .add_event::<AttackEvent>()
            .add_event::<DamageReceivedEvent>()
            .add_event::<CreateProjectileEvent>()
            .add_event::<CreateLaserBeamEvent>()
            .add_systems(OnEnter(GameState::InGame), reset_autoattack_state)
            .add_systems(
                Update,
                (
                    autoattack,
                    update_attack_target,
                    attack.in_set(AttackSet),
                    handle_attack_events,
                    handle_damage_events,
                    handle_death_events,
                )
                    .chain()
                    .run_if(in_state(GameState::InGame))
                    .run_if(in_state(PlayingState::Playing)),
            )
            .add_systems(
                Update,
                (
                    update_rocket_projectiles,
                    update_laser_beams,
                    reload,
                    clean_up_attack_targets,
                    handle_create_laser_beam_events,
                )
                    .run_if(in_state(GameState::InGame))
                    .run_if(in_state(PlayingState::Playing)),
            );
    }
}

#[derive(Debug, Hash, PartialEq, Eq, Clone, SystemSet)]
pub struct AttackSet;

#[derive(Component, Reflect)]
pub struct Combat {
    pub damage: i32,
    pub attack_range: f32,
    pub aggro_range: f32,
    pub weapon: Weapon,
    /// How long it takes for our AttackTarget to be out of range before we drop it
    /// Should be very long for melee and short range units
    /// Close to 0 for immobile units or buildings
    pub out_of_range_timer_duration: f32,
    pub attack_timer: AttackTimer,
    pub shooting_point: Vec3,
}

#[derive(Reflect)]
pub enum Weapon {
    LaserEmitter {
        beam_width: f32,
        light_power: f32,
    },
    RocketLauncher {
        rocket_size: f32,
        rocket_speed: f32,
        rocket_turn_speed: f32,
    },
}

impl Weapon {
    pub fn default_laser_emitter() -> Self {
        Weapon::LaserEmitter {
            beam_width: 3.0,
            light_power: 1.0,
        }
    }

    pub fn default_rocket_launcher() -> Self {
        Weapon::RocketLauncher {
            rocket_size: 4.0,
            rocket_speed: 30.0,
            rocket_turn_speed: 8.0,
        }
    }
}

impl Default for Weapon {
    fn default() -> Self {
        Weapon::default_laser_emitter()
    }
}

impl Combat {
    pub fn in_attack_range(&self, distance: f32) -> bool {
        distance <= self.attack_range
    }

    pub fn in_aggro_range(&self, distance: f32) -> bool {
        distance <= self.aggro_range
    }

    pub fn get_shooting_point_with_transform(&self, transform: Transform) -> Vec3 {
        let right = transform.right() * self.shooting_point.x;
        let up = transform.up() * self.shooting_point.y;
        let forward = transform.forward() * self.shooting_point.z;

        transform.translation + right + up + forward
    }
}

#[derive(Reflect, Deref, DerefMut)]
pub struct AttackTimer(Timer);

impl Default for AttackTimer {
    fn default() -> Self {
        AttackTimer::new(1.0)
    }
}

impl AttackTimer {
    pub fn new(duration: f32) -> Self {
        let mut timer = Timer::from_seconds(duration, TimerMode::Once);
        // Make the first shot in the game go off instantly
        timer.set_elapsed(timer.duration());

        AttackTimer(timer)
    }
}

#[derive(Component, Reflect)]
pub struct AttackTarget {
    pub target: Entity,
    pub in_attack_range: bool,
    pub good_attack_angle: bool,
    pub out_of_range_timer: Timer,
}

impl AttackTarget {
    pub fn new(target: Entity, timer_duration: f32) -> Self {
        AttackTarget {
            target,
            in_attack_range: false,
            good_attack_angle: false,
            out_of_range_timer: Timer::from_seconds(timer_duration, TimerMode::Once),
        }
    }
}

impl Default for Combat {
    fn default() -> Self {
        Self {
            damage: 1,
            attack_range: 5.0,
            aggro_range: 25.0,
            weapon: Weapon::default(),
            out_of_range_timer_duration: 2.0,
            attack_timer: AttackTimer::default(),
            shooting_point: Vec3::ZERO,
        }
    }
}

pub enum DamageType {
    Energy,
    Explosive,
}

fn reload(mut query: Query<&mut Combat>, time: Res<Time>) {
    for mut combat in query.iter_mut() {
        combat.attack_timer.tick(time.delta());
    }
}

fn reset_autoattack_state(mut aa_state: ResMut<AutoattackState>) {
    aa_state.0 = true;
}

pub fn autoattack(
    mut commands: Commands,
    mut not_attacking_units: Query<
        (Entity, &Transform, &PlayerTeam, &Combat, Option<&Movement>),
        Without<AttackTarget>,
    >,
    targets: Query<(Entity, &Transform, &PlayerTeam, &Collider), With<Health>>,
    aa_state: Res<AutoattackState>,
) {
    if !aa_state.0 {
        return;
    }

    for (e1, t1, team1, cs1, mov1) in not_attacking_units.iter_mut() {
        if let Some(mov) = mov1 {
            if mov.moving {
                continue;
            }
        }

        let pos = t1.translation;

        let any_in_attack_range = targets.iter().find(|(_, t2, team2, collider2)| {
            let distance_to_hit = pos.distance(t2.translation) - collider2.inner_radius;
            cs1.in_attack_range(distance_to_hit) && team1 != *team2
        });

        if let Some(target) = any_in_attack_range {
            commands
                .entity(e1)
                .insert(AttackTarget::new(target.0, cs1.out_of_range_timer_duration));
            continue;
        }

        let any_in_aggro_range = targets.iter().find(|(_, t2, team2, collider2)| {
            let distance_to_hit = pos.distance(t2.translation) - collider2.inner_radius;
            cs1.in_aggro_range(distance_to_hit) && team1 != *team2
        });

        if let Some(target) = any_in_aggro_range {
            commands
                .entity(e1)
                .insert(AttackTarget::new(target.0, cs1.out_of_range_timer_duration));
        }
    }
}

fn update_attack_target(
    mut commands: Commands,
    target_query: Query<(&Transform, &Collider)>,
    mut attacker_query: Query<(
        Entity,
        &Transform,
        &Combat,
        &mut AttackTarget,
        Option<&Turn>,
    )>,
    time: Res<Time>,
) {
    for (entity, attacker_transform, attacker_combat, mut at, turn) in attacker_query.iter_mut() {
        if let Ok((target_transform, target_collider)) = target_query.get(at.target) {
            let attacker_pos = attacker_transform.translation;
            let target_pos = target_transform.translation;
            let distance_to_hit = attacker_pos.distance(target_pos) - target_collider.inner_radius;
            if attacker_combat.in_aggro_range(distance_to_hit) {
                at.out_of_range_timer.reset();
            } else {
                at.out_of_range_timer.tick(time.delta());
            }

            at.in_attack_range = attacker_combat.in_attack_range(distance_to_hit);

            if at.out_of_range_timer.finished() {
                println!("he ran away :(");
                commands.entity(entity).remove::<AttackTarget>();
            }

            if attacker_pos == target_pos {
                at.good_attack_angle = true;
                continue;
            }

            if let Some(turn) = turn {
                match turn.target {
                    Target::Dynamic(entity) => {
                        at.good_attack_angle = entity == at.target && turn.is_small_enough_angle();
                    }
                    _ => {
                        at.good_attack_angle = false;
                    }
                }
            } else {
                at.good_attack_angle = true;
            }
        }
    }
}

pub fn clean_up_attack_targets(
    mut commands: Commands,
    attack_target_query: Query<(Entity, &AttackTarget)>,
) {
    for (attacker_entity, attack_target) in attack_target_query.iter() {
        if commands.get_entity(attack_target.target).is_none() {
            commands.entity(attacker_entity).remove::<AttackTarget>();
        }
    }
}

pub fn attack(
    mut attack_query: Query<(
        Entity,
        &AttackTarget,
        &Combat,
        Option<&mut Turn>,
        Option<&mut Movement>,
    )>,
    mut attack_event_writer: EventWriter<AttackEvent>,
    mut movement_event: EventWriter<StartMovementEvent>,
) {
    for (attacker, at, combat, turn, mov) in attack_query.iter_mut() {
        if let Some(mut mov) = mov {
            if at.in_attack_range {
                mov.stop();
            } else if !mov.moving {
                movement_event.send(StartMovementEvent {
                    unit_entity: attacker,
                    target: Target::Dynamic(at.target),
                });
            }
        }

        if !at.good_attack_angle {
            if let Some(mut turn) = turn {
                if turn.target != Target::Dynamic(at.target) {
                    turn.target = Target::Dynamic(at.target);
                }
            }
        }

        if at.in_attack_range && at.good_attack_angle && combat.attack_timer.finished() {
            attack_event_writer.send(AttackEvent {
                attacker,
                target: at.target,
            });
        }
    }
}
