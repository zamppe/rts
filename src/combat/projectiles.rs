use bevy::prelude::*;

use crate::cleanup::CleanupBundle;
use crate::combat::events::DamageReceivedEvent;
use crate::combat::DamageType;
use crate::core::movement::{Movement, StartMovementEvent};
use crate::core::turn::Turn;
use crate::core::{Collider, Target};
use crate::graphics::Models;

#[allow(dead_code)]
pub enum ProjectileType {
    Homing,
    Straight,
}

#[derive(Event)]
pub struct CreateProjectileEvent {
    pub proj_type: ProjectileType,
    pub position: Vec3,
    pub target: Entity,
    pub damage: i32,
    pub speed: f32,
    pub turn_speed: f32,
    pub size: f32,
}

#[derive(Component, Reflect)]
pub struct RocketProjectile {
    pub target: Entity,
    pub damage: i32,
}

pub fn handle_create_projectile_events(
    mut commands: Commands,
    mut proj_event_reader: EventReader<CreateProjectileEvent>,
    target_query: Query<&Transform>,
    mut start_moving_event_writer: EventWriter<StartMovementEvent>,
    models: Res<Models>,
) {
    for ev in proj_event_reader.read() {
        if let Ok(target_transform) = target_query.get(ev.target) {
            let attacker_start_point = ev.position;
            let transform = Transform::from_translation(attacker_start_point)
                .with_scale(Vec3::splat(ev.size))
                .looking_at(target_transform.translation, Vec3::Y);

            match ev.proj_type {
                ProjectileType::Homing => {
                    let rocket = commands
                        .spawn((
                            PbrBundle {
                                mesh: models.rocket_projectile.mesh.clone(),
                                material: models.rocket_projectile.mat.clone(),
                                transform,
                                ..default()
                            },
                            RocketProjectile {
                                target: ev.target,
                                damage: ev.damage,
                            },
                            Movement::new(ev.speed),
                            Turn::new(ev.turn_speed).with_no_move_delay(),
                            CleanupBundle::in_game("RocketProjectile"),
                        ))
                        .id();

                    start_moving_event_writer.send(StartMovementEvent {
                        unit_entity: rocket,
                        target: Target::DynamicCenter(ev.target),
                    });
                }
                ProjectileType::Straight => {
                    create_bullet_projectile();
                }
            }
        }
    }
}

pub fn update_rocket_projectiles(
    mut commands: Commands,
    mut damage_event_writer: EventWriter<DamageReceivedEvent>,
    projectile_query: Query<(Entity, &Transform, &RocketProjectile)>,
    target_query: Query<(&Transform, &Collider)>,
) {
    for (proj_entity, proj_transform, proj) in projectile_query.iter() {
        if let Ok((target_transform, target_collider)) = target_query.get(proj.target) {
            let proj_pos = proj_transform.translation;
            let target_contact_point = target_collider.get_center_with_transform(*target_transform);

            // Simplified collision check here
            if proj_pos.distance(target_contact_point) <= target_collider.inner_radius {
                damage_event_writer.send(DamageReceivedEvent {
                    target: proj.target,
                    damage: proj.damage,
                    damage_type: DamageType::Explosive,
                });
                commands.entity(proj_entity).despawn_recursive();
            }
        } else {
            commands.entity(proj_entity).despawn_recursive();
        }
    }
}

pub fn create_bullet_projectile() {}
