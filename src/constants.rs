use bevy::prelude::*;

pub const COLOR_GREEN_ALIGNMENT: Color = Color::rgb(0.5, 1.0, 0.5);
pub const COLOR_RED_ALIGNMENT: Color = Color::rgb(1.0, 0.5, 0.5);
pub const COLOR_BLUE_ALIGNMENT: Color = Color::rgb(0.0, 0.0, 1.0);
pub const BUILDING_COLOR_GREEN_ALIGNMENT: Color = Color::LIME_GREEN;
pub const BUILDING_COLOR_RED_ALIGNMENT: Color = Color::MAROON;
pub const BUILDING_COLOR_BLUE_ALIGNMENT: Color = Color::rgb(0.0, 0.2, 0.9);
pub const BUTTON_COLOR: Color = Color::rgb(0.23, 0.52, 0.125);
pub const BUTTON_COLOR_HOVERED: Color = Color::rgb(0.0, 0.72, 0.0);

pub const COST_BARRACKS: u32 = 400;
pub const COST_GATHERER: u32 = 800;
pub const COST_LASER_TOWER: u32 = 600;
pub const COST_SOLDIER: u32 = 50;
pub const COST_ROCKET_DUDE: u32 = 150;

pub const HEALTH_BARRACKS: i32 = 200;
pub const HEALTH_GATHERER: i32 = 400;
pub const HEALTH_HEADQUARTERS: i32 = 1000;
pub const HEALTH_LASER_TOWER: i32 = 300;
pub const HEALTH_SOLDIER: i32 = 20;
pub const HEALTH_ROCKET_DUDE: i32 = 30;

pub const CONSTRUCTION_TIME_HEADQUARTERS: f32 = 0.5;
pub const CONSTRUCTION_TIME_BARRACKS: f32 = 5.0;
pub const CONSTRUCTION_TIME_GATHERER: f32 = 15.0;
pub const CONSTRUCTION_TIME_LASER_TOWER: f32 = 8.0;

pub const MAX_RESOURCE_AMOUNT: u32 = 50000;
pub const MAX_BUILDING_DISTANCE: f32 = 200.0;

pub const CAMERA_SPEED: f32 = 150.0;
pub const CAMERA_ZOOM_STEP_SCROLL: f32 = 1.4;
pub const MIN_FOV: f32 = 0.2;
pub const MAX_FOV: f32 = 2.00;
pub const MOUSE_PAN_MARGIN: f32 = 0.01;
