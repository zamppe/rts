use bevy::math::Vec3Swizzles;
use bevy::pbr::{NotShadowCaster, NotShadowReceiver};
use bevy::prelude::*;
use bevy::render::view::RenderLayers;
use bevy_health_bar3d::prelude::*;

use crate::cleanup::{CleanupBundle, CleanupInGame};
use crate::combat::{AttackTimer, Combat, Weapon};
use crate::constants::*;
use crate::core::turn::Turn;
use crate::core::GameObjectBundle;
use crate::graphics::building_shadow::BuildingShadow;
use crate::graphics::materials::CommonMaterials;
use crate::graphics::selection_outline::create_selection_outline;
use crate::graphics::Models;
use crate::input::mouse::MouseState;
use crate::player::{PlayerAlignment, PlayerTeam};

#[derive(Bundle)]
struct BuildingBundle {
    pbr_bundle: PbrBundle,
    game_object_bundle: GameObjectBundle,
    building: Building,
    construction_bundle: ConstructionBundle,
    cleanup_bundle: CleanupBundle<CleanupInGame>,
}

#[derive(Component, Debug, Reflect, Clone, Deref, DerefMut, PartialEq)]
pub struct Building(pub BuildingType);

#[derive(Component, Debug, Reflect, Clone, Deref, DerefMut, PartialEq)]
pub struct Headquarters(pub PlayerAlignment);

#[derive(Bundle)]
struct ConstructionBundle {
    construction: Construction,
    construction_bar: BarBundle<Construction>,
}

impl ConstructionBundle {
    fn new(duration: f32, building_size: Vec3) -> Self {
        ConstructionBundle {
            construction: Construction(Timer::from_seconds(duration, TimerMode::Once)),
            construction_bar: BarBundle::<Construction> {
                offset: BarOffset::new(building_size.y + 4.0),
                width: BarWidth::new(building_size.x * 1.5),
                height: BarHeight::Static(0.8),
                border: BarBorder::new(0.15),
                ..default()
            },
        }
    }
}

#[derive(Component, Reflect, Deref, DerefMut)]
pub struct Construction(pub Timer);

impl Construction {
    pub fn new(duration: f32) -> Self {
        Construction(Timer::from_seconds(duration, TimerMode::Once))
    }
}

impl Percentage for Construction {
    fn value(&self) -> f32 {
        self.elapsed().as_secs_f32() / self.duration().as_secs_f32()
    }
}

#[derive(Default, Debug, Eq, PartialEq, Copy, Clone, Hash, Reflect)]
pub enum BuildingType {
    Headquarters,
    #[default]
    Barracks,
    Gatherer,
    LaserTower,
}

impl BuildingType {
    fn construction_time(&self) -> f32 {
        match self {
            BuildingType::Headquarters => CONSTRUCTION_TIME_HEADQUARTERS,
            BuildingType::Barracks => CONSTRUCTION_TIME_BARRACKS,
            BuildingType::Gatherer => CONSTRUCTION_TIME_GATHERER,
            BuildingType::LaserTower => CONSTRUCTION_TIME_LASER_TOWER,
        }
    }

    pub fn cost(&self) -> u32 {
        match self {
            BuildingType::Headquarters => 0,
            BuildingType::Barracks => COST_BARRACKS,
            BuildingType::Gatherer => COST_GATHERER,
            BuildingType::LaserTower => COST_LASER_TOWER,
        }
    }

    fn health(&self) -> i32 {
        match self {
            BuildingType::Headquarters => HEALTH_HEADQUARTERS,
            BuildingType::Barracks => HEALTH_BARRACKS,
            BuildingType::Gatherer => HEALTH_GATHERER,
            BuildingType::LaserTower => HEALTH_LASER_TOWER,
        }
    }

    pub fn name(&self) -> &'static str {
        match self {
            BuildingType::Headquarters => "Headquarters",
            BuildingType::Barracks => "Barracks",
            BuildingType::Gatherer => "Gatherer",
            BuildingType::LaserTower => "Laser Tower",
        }
    }

    pub fn size(&self) -> Vec3 {
        match self {
            BuildingType::Headquarters => Vec3::new(30.0, 50.1, 30.0),
            BuildingType::Barracks => Vec3::new(15.0, 17.8, 15.0),
            BuildingType::Gatherer => Vec3::new(23.8, 12.5, 25.0),
            BuildingType::LaserTower => Vec3::new(10.0, 17.0, 10.0),
        }
    }
}

#[derive(Event)]
pub struct SpawnBuildingEvent {
    pub position: Vec3,
    pub player_team: PlayerTeam,
    pub player_alignment: PlayerAlignment,
    pub building_type: BuildingType,
}

#[derive(Event)]
pub struct PlaceBuildingEvent {
    pub player_alignment: PlayerAlignment,
    pub building_type: BuildingType,
}

pub fn construction_tick(
    mut commands: Commands,
    mut building_query: Query<(
        Entity,
        &Building,
        &mut Construction,
        &Handle<StandardMaterial>,
    )>,
    time: Res<Time>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    for (entity, building, mut construction, material_handle) in building_query.iter_mut() {
        construction.tick(time.delta());
        let percent = construction.percent();

        let material = materials.get_mut(material_handle).unwrap();
        material.base_color.set_a(percent * 0.5 + 0.5);

        if construction.just_finished() {
            let material = materials.get_mut(material_handle).unwrap();
            material.alpha_mode = AlphaMode::Opaque;

            if **building == BuildingType::LaserTower {
                commands.entity(entity).insert(Combat {
                    damage: 20,
                    attack_range: 40.0,
                    aggro_range: 40.0,
                    weapon: Weapon::LaserEmitter {
                        beam_width: 12.0,
                        light_power: 8.0,
                    },
                    out_of_range_timer_duration: 0.1,
                    attack_timer: AttackTimer::new(2.5),
                    shooting_point: Vec3::new(0.0, 16.693, 5.0195),
                });
            }

            if **building == BuildingType::Headquarters {
                let light = commands
                    .spawn(SpotLightBundle {
                        spot_light: SpotLight {
                            range: 100.0,
                            intensity: 30000.0,
                            ..default()
                        },
                        transform: Transform::from_xyz(0.0, 70.0, 0.0)
                            .looking_at(Vec3::ZERO, Vec3::Y),
                        ..default()
                    })
                    .id();

                commands.entity(entity).add_child(light);
            }

            commands
                .entity(entity)
                .remove::<Construction>()
                .remove::<BarBundle<Construction>>()
                .remove::<NotShadowCaster>()
                .remove::<NotShadowReceiver>();
        }
    }
}

pub fn spawn_building(
    mut cmds: Commands,
    mut ev_spawn_building: EventReader<SpawnBuildingEvent>,
    mut meshes: ResMut<Assets<Mesh>>,
    common_materials: Res<CommonMaterials>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    models: Res<Models>,
) {
    for ev in ev_spawn_building.read() {
        let building_size = ev.building_type.size();
        let name = format!(
            "{} | {}",
            ev.building_type.name(),
            ev.player_alignment.name()
        );

        let mesh = match ev.building_type {
            BuildingType::Headquarters => models.headquarters.clone(),
            BuildingType::Barracks => models.barracks.clone(),
            BuildingType::Gatherer => models.gatherer.clone(),
            BuildingType::LaserTower => models.laser_tower.clone(),
        };

        let material = materials.add(StandardMaterial {
            base_color: *ev.player_alignment.building_color().set_a(0.5),
            alpha_mode: AlphaMode::Blend,
            ..default()
        });

        let building = cmds
            .spawn((
                BuildingBundle {
                    pbr_bundle: PbrBundle {
                        mesh,
                        material,
                        transform: Transform::from_translation(ev.position),
                        ..default()
                    },
                    game_object_bundle: GameObjectBundle::building(
                        ev.player_alignment,
                        ev.player_team,
                        ev.building_type.health(),
                        building_size,
                    ),
                    building: Building(ev.building_type),
                    construction_bundle: ConstructionBundle::new(
                        ev.building_type.construction_time(),
                        building_size,
                    ),
                    cleanup_bundle: CleanupBundle::in_game(name),
                },
                RenderLayers::default().with(1),
                NotShadowCaster,
                NotShadowReceiver,
            ))
            .id();

        let selection_outline = create_selection_outline(
            &mut cmds,
            building_size.xz(),
            &mut meshes,
            common_materials.selection.clone(),
        );
        cmds.entity(building).add_child(selection_outline);

        match ev.building_type {
            BuildingType::Headquarters => {
                cmds.entity(building)
                    .insert(Headquarters(ev.player_alignment));
            }
            BuildingType::LaserTower => {
                cmds.entity(building).insert(Turn::new(2.0));
            }
            _ => {}
        };
    }
}

pub fn handle_place_building_events(
    mut cmds: Commands,
    mut place_building_event_reader: EventReader<PlaceBuildingEvent>,
    mut meshes: ResMut<Assets<Mesh>>,
    mouse_state: Res<MouseState>,
    models: Res<Models>,
    common_materials: Res<CommonMaterials>,
) {
    for ev in place_building_event_reader.read() {
        let size = ev.building_type.size().x;

        let mesh = match ev.building_type {
            BuildingType::Headquarters => models.headquarters.clone(),
            BuildingType::Barracks => models.barracks.clone(),
            BuildingType::Gatherer => models.gatherer.clone(),
            BuildingType::LaserTower => models.laser_tower.clone(),
        };

        let material = match ev.player_alignment {
            PlayerAlignment::Green => common_materials.green_building_shadow.clone(),
            PlayerAlignment::Red => common_materials.red_building_shadow.clone(),
            PlayerAlignment::Blue => common_materials.blue_building_shadow.clone(),
        };

        let flying_building = cmds
            .spawn((
                PbrBundle {
                    mesh,
                    material,
                    transform: Transform::from_translation(Vec3::new(0.0, 1.0, 0.0)),
                    ..default()
                },
                NotShadowCaster,
                NotShadowReceiver,
            ))
            .id();

        cmds.spawn((
            PbrBundle {
                mesh: meshes.add(Mesh::from(shape::Plane::from_size(size))),
                material: common_materials.building_shadow_plane.clone(),
                transform: Transform {
                    translation: match mouse_state.world_pos {
                        None => Vec3::ZERO,
                        Some(world_pos) => Vec3::new(world_pos.x, 0.1, world_pos.y),
                    },
                    ..default()
                },
                ..default()
            },
            NotShadowCaster,
            NotShadowReceiver,
            BuildingShadow {
                building_type: ev.building_type,
            },
            CleanupBundle::in_game("BuildingShadow"),
        ))
        .add_child(flying_building);
    }
}
