use bevy::math::Vec3Swizzles;
use bevy::prelude::{Vec2, Vec3};

pub fn collide(a_pos: Vec3, a_size: Vec2, b_pos: Vec3, b_size: Vec2) -> bool {
    let a_min = a_pos.xz() - a_size / 2.0;
    let a_max = a_pos.xz() + a_size / 2.0;

    let b_min = b_pos.xz() - b_size / 2.0;
    let b_max = b_pos.xz() + b_size / 2.0;

    // check to see if the two rectangles are intersecting
    a_min.x < b_max.x && a_max.x > b_min.x && a_min.y < b_max.y && a_max.y > b_min.y
}

pub fn obb_collision(
    center_a: Vec2,
    dimensions_a: Vec2,
    rotation_a: f32,
    center_b: Vec2,
    dimensions_b: Vec2,
    rotation_b: f32,
) -> bool {
    let vertices_a = compute_vertices(center_a, dimensions_a, rotation_a);
    let vertices_b = compute_vertices(center_b, dimensions_b, rotation_b);

    let axes_a = compute_axes(&vertices_a);
    let axes_b = compute_axes(&vertices_b);

    for &axis in axes_a.iter().chain(axes_b.iter()) {
        let projection_a = project(&vertices_a, axis);
        let projection_b = project(&vertices_b, axis);

        if !overlaps(projection_a, projection_b) {
            return false;
        }
    }

    true // All projections overlapped
}

fn compute_vertices(center: Vec2, dimensions: Vec2, rotation: f32) -> [Vec2; 4] {
    let half_dimensions = dimensions / 2.0;
    let cos = rotation.cos();
    let sin = rotation.sin();

    let points = [
        Vec2::new(-half_dimensions.x, -half_dimensions.y),
        Vec2::new(half_dimensions.x, -half_dimensions.y),
        Vec2::new(half_dimensions.x, half_dimensions.y),
        Vec2::new(-half_dimensions.x, half_dimensions.y),
    ];

    let mut result = [Vec2::ZERO; 4];
    for i in 0..4 {
        result[i] = Vec2::new(
            points[i].x * cos + points[i].y * sin + center.x,
            points[i].x * -sin + points[i].y * cos + center.y,
        );
    }

    result
}

fn compute_axes(vertices: &[Vec2; 4]) -> [Vec2; 4] {
    [
        (vertices[1] - vertices[0]).normalize(),
        (vertices[2] - vertices[1]).normalize(),
        (vertices[3] - vertices[2]).normalize(),
        (vertices[0] - vertices[3]).normalize(),
    ]
}

fn project(vertices: &[Vec2; 4], axis: Vec2) -> (f32, f32) {
    let mut min = Vec2::dot(vertices[0], axis);
    let mut max = min;

    for vertex in vertices.iter().skip(1) {
        let projection = Vec2::dot(*vertex, axis);
        if projection < min {
            min = projection;
        } else if projection > max {
            max = projection;
        }
    }

    (min, max)
}

fn overlaps(a: (f32, f32), b: (f32, f32)) -> bool {
    a.1 > b.0 && b.1 > a.0
}
