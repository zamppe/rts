use bevy::math::Vec3Swizzles;
use bevy::prelude::*;
use bevy::utils::HashSet;
use bevy_health_bar3d::prelude::*;

use building::{construction_tick, spawn_building, Construction, SpawnBuildingEvent};
use movement::{apply_movement, handle_move_events, update_movement, StartMovementEvent};
use unit::{handle_spawn_unit_events, SpawnUnitEvent};

use crate::combat::projectiles::handle_create_projectile_events;
use crate::combat::AttackSet;
use crate::core::building::{handle_place_building_events, PlaceBuildingEvent};
use crate::core::movement::update_turns;
use crate::graphics::get_randomized_direction_vec2;
use crate::player::{PlayerAlignment, PlayerTeam};

pub mod building;
pub mod collision;
pub mod movement;
pub mod turn;
pub mod unit;

pub struct RtsCorePlugin;

impl Plugin for RtsCorePlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<PlaceBuildingEvent>()
            .add_event::<SpawnBuildingEvent>()
            .add_state::<GameState>()
            .add_state::<PlayingState>()
            .add_event::<SpawnUnitEvent>()
            .add_event::<StartMovementEvent>()
            .add_plugins(HealthBarPlugin::<Construction>::default())
            .add_plugins(HealthBarPlugin::<Health>::default())
            .insert_resource(
                ColorScheme::<Construction>::new()
                    .foreground_color(ForegroundColor::Static(Color::hsl(0.0, 0.0, 0.9))),
            )
            .add_event::<DespawnEvent>()
            .add_event::<DespawnGameObjectsEvent>()
            .add_systems(OnEnter(GameState::InGame), reset_playing_state)
            .add_systems(OnExit(GameState::InGame), reset_playing_state)
            .add_systems(
                Last,
                (handle_despawn_game_objects_events, handle_despawn_events)
                    .chain()
                    .run_if(in_state(GameState::InGame)),
            )
            .add_systems(
                Update,
                (
                    construction_tick,
                    handle_place_building_events,
                    spawn_building,
                )
                    .run_if(in_state(GameState::InGame))
                    .run_if(in_state(PlayingState::Playing)),
            )
            .add_systems(
                Update,
                (
                    update_movement,
                    update_turns,
                    apply_deferred,
                    apply_movement,
                )
                    .chain()
                    .run_if(in_state(GameState::InGame))
                    .run_if(in_state(PlayingState::Playing))
                    .in_set(MovingTurningSet),
            )
            .add_systems(
                Update,
                (
                    handle_spawn_unit_events,
                    handle_create_projectile_events,
                    apply_deferred,
                    handle_move_events,
                )
                    .chain()
                    .run_if(in_state(GameState::InGame))
                    .run_if(in_state(PlayingState::Playing))
                    .in_set(MoveEventSet),
            );

        app.configure_sets(Update, AttackSet.before(MoveEventSet));
    }
}

#[derive(Default, Debug, Clone, Eq, PartialEq, Hash, States)]
pub enum GameState {
    #[default]
    MainMenu,
    Loading,
    InGame,
    GameOver,
}

#[derive(Default, Debug, Clone, Eq, PartialEq, Hash, States)]
pub enum PlayingState {
    #[default]
    Playing,
    Paused,
}

#[derive(Debug, Hash, PartialEq, Eq, Clone, SystemSet)]
pub struct MoveEventSet;

#[derive(Debug, Hash, PartialEq, Eq, Clone, SystemSet)]
pub struct MovingTurningSet;

#[derive(Bundle)]
pub struct GameObjectBundle {
    game_object: GameObject,
    control: UserControl,
    alignment: PlayerAlignment,
    team: PlayerTeam,
    collider: Collider,
    health: Health,
    health_bar: BarBundle<Health>,
}

impl GameObjectBundle {
    pub fn unit(
        alignment: PlayerAlignment,
        team: PlayerTeam,
        collider: Collider,
        health: i32,
        unit_size: Vec3,
    ) -> Self {
        GameObjectBundle {
            game_object: GameObject::Unit,
            control: UserControl::new(),
            alignment,
            team,
            collider,
            health: Health::new(health),
            health_bar: BarBundle::<Health> {
                offset: BarOffset::new(unit_size.y + 1.0),
                width: BarWidth::new(unit_size.x * 1.2),
                height: BarHeight::Static(0.4),
                border: BarBorder::new(0.1),
                visibility: BarVisibility::Hidden,
                ..default()
            },
        }
    }

    pub fn building(
        alignment: PlayerAlignment,
        team: PlayerTeam,
        health: i32,
        building_size: Vec3,
    ) -> Self {
        GameObjectBundle {
            game_object: GameObject::Building,
            control: UserControl::new(),
            alignment,
            team,
            collider: Collider::new(building_size),
            health: Health::new(health),
            health_bar: BarBundle::<Health> {
                offset: BarOffset::new(building_size.y + 1.0),
                width: BarWidth::new(building_size.x * 1.2),
                height: BarHeight::Static(0.8),
                border: BarBorder::new(0.1),
                visibility: BarVisibility::Hidden,
                ..default()
            },
        }
    }
}

#[derive(Component, Reflect, PartialEq, Eq, Hash)]
pub enum GameObject {
    Unit,
    Building,
}

// most likely need to add "controllable" field
// or maybe other better naming for selecting things we cannot actually control
// (like enemy units)
#[derive(Component, Reflect)]
pub struct UserControl {
    pub selected: bool,
}

impl UserControl {
    pub fn new() -> Self {
        UserControl { selected: false }
    }
}

#[derive(Component, Reflect, Clone)]
pub struct Collider {
    pub hitbox_size: Vec3,
    pub inner_radius: f32,
    pub center: f32,
}

impl Collider {
    pub fn new(hitbox_size: Vec3) -> Self {
        Collider {
            hitbox_size,
            inner_radius: hitbox_size.min_element() / 2.0,
            center: hitbox_size.y / 2.0,
        }
    }

    pub fn with_center(mut self, center: f32) -> Self {
        self.center = center;
        self
    }

    pub fn get_center_with_transform(&self, transform: Transform) -> Vec3 {
        transform.translation + transform.up() * self.center
    }
}

#[derive(Component, Reflect)]
pub struct Health {
    pub current: i32,
    pub max: i32,
}

impl Percentage for Health {
    fn value(&self) -> f32 {
        self.current as f32 / self.max as f32
    }
}

impl Health {
    fn new(value: i32) -> Self {
        Health {
            current: value,
            max: value,
        }
    }
}

impl Default for Health {
    fn default() -> Self {
        Health::new(1)
    }
}

#[derive(Event)]
pub struct DespawnEvent {
    pub entities: HashSet<Entity>,
}

#[derive(Event)]
pub struct DespawnGameObjectsEvent;

#[derive(Reflect, PartialEq, Copy, Clone)]
pub enum Target {
    None,
    Static(Vec3),
    Dynamic(Entity),
    DynamicCenter(Entity),
}

pub fn find_position_around_building(transform: &Transform, building_size: Vec3) -> Vec3 {
    let dir_2d = get_randomized_direction_vec2().normalize();
    let direction_3d = (dir_2d * building_size.xz()).extend(0.0);
    transform.translation + direction_3d.xzy()
}

fn handle_despawn_events(mut commands: Commands, mut despawn_events: EventReader<DespawnEvent>) {
    for ev in despawn_events.read() {
        for entity in ev.entities.iter() {
            commands.entity(*entity).despawn_recursive();
        }
    }
}

fn handle_despawn_game_objects_events(
    mut events: EventReader<DespawnGameObjectsEvent>,
    mut despawn_event_writer: EventWriter<DespawnEvent>,
    game_object_query: Query<Entity, With<GameObject>>,
) {
    for _ev in events.read() {
        let game_objects: HashSet<Entity> = game_object_query.iter().collect();
        despawn_event_writer.send(DespawnEvent {
            entities: game_objects,
        });
    }
}

fn reset_playing_state(mut playing_state: ResMut<NextState<PlayingState>>) {
    playing_state.set(PlayingState::Playing);
}
