use bevy::math::Vec3Swizzles;
use bevy::prelude::*;

use crate::core::turn::{Turn, TurnStatus};
use crate::core::{Collider, Target};

#[derive(Component, Reflect)]
pub struct Movement {
    velocity: Vec3,
    target_pos: Vec3,
    pub moving: bool,
    base_speed: f32,
    will_chase: bool,
    target: Target,
}

impl Movement {
    pub fn new(base_speed: f32) -> Self {
        Movement {
            velocity: Vec3::ZERO,
            target_pos: Vec3::ZERO,
            moving: false,
            base_speed,
            will_chase: true,
            target: Target::None,
        }
    }

    pub fn go_from_a_to_b(&mut self, a_pos: Vec3, b_pos: Vec3) {
        let direction = (b_pos - a_pos).normalize();
        self.target_pos = b_pos;
        self.velocity = direction * self.base_speed;
        self.moving = true;
    }

    pub fn correct_velocity(&mut self, pos: Vec3) {
        let direction = (self.target_pos - pos).normalize();
        self.velocity = direction * self.base_speed;
    }

    pub fn stop(&mut self) {
        self.velocity = Vec3::ZERO;
        self.moving = false;
        self.target_pos = Vec3::ZERO;
        self.target = Target::None;
    }

    pub fn is_close_enough(&self, pos: Vec3) -> bool {
        self.target_pos.distance(pos) < 0.5
    }

    pub fn update_target_pos(&mut self, new_pos: Vec3) {
        self.target_pos = new_pos;
    }

    pub fn move_or_stop(&mut self, transform: &mut Transform, time: &Res<Time>) {
        if self.is_close_enough(transform.translation) {
            transform.translation = self.target_pos;
            self.stop();
        } else {
            self.correct_velocity(transform.translation);
            let change = self.velocity * time.delta_seconds();
            transform.translation += change;
        }
    }

    pub fn move_with_fixed_rotation_or_stop(
        &mut self,
        transform: &mut Transform,
        time: &Res<Time>,
    ) {
        if self.is_close_enough(transform.translation) {
            transform.translation = self.target_pos;
            self.stop();
        } else {
            let direction = transform.forward();
            self.velocity = direction * self.base_speed;
            let change = self.velocity * time.delta_seconds();
            transform.translation += change;
        }
    }
}

#[derive(Event)]
pub struct StartMovementEvent {
    pub unit_entity: Entity,
    pub target: Target,
}

pub fn handle_move_events(
    mut start_moving_event_reader: EventReader<StartMovementEvent>,
    mut query: Query<(&mut Movement, &Transform, Option<&mut Turn>)>,
    target_query: Query<(&Transform, &Collider)>,
) {
    for ev in start_moving_event_reader.read() {
        if let Ok((mut mov, transform, turn)) = query.get_mut(ev.unit_entity) {
            let target_pos = match ev.target {
                Target::None => {
                    continue;
                }
                Target::Static(target_pos) => target_pos,
                Target::Dynamic(entity) => {
                    if let Ok((target_transform, _)) = target_query.get(entity) {
                        target_transform.translation
                    } else {
                        continue;
                    }
                }
                Target::DynamicCenter(entity) => {
                    if let Ok((target_transform, collider)) = target_query.get(entity) {
                        collider.get_center_with_transform(*target_transform)
                    } else {
                        continue;
                    }
                }
            };

            if transform.translation == target_pos {
                continue;
            }

            if let Some(mut turn) = turn {
                turn.target = ev.target;
            }

            mov.go_from_a_to_b(transform.translation, target_pos);
            mov.target = ev.target;
        } else {
            println!("tried doing StartMovementEvent for something ");
            println!("that doesn't have Movement and/or Transform!");
        }
    }
}

pub fn update_movement(
    mut movement_query: Query<&mut Movement>,
    target_query: Query<(&Transform, &Collider)>,
) {
    for mut mov in movement_query.iter_mut() {
        if mov.moving {
            match mov.target {
                Target::Dynamic(entity) => match target_query.get(entity) {
                    Ok((new_target_pos, _)) => {
                        mov.update_target_pos(new_target_pos.translation);
                    }
                    Err(_) => {
                        mov.stop();
                    }
                },
                Target::DynamicCenter(entity) => match target_query.get(entity) {
                    Ok((target_transform, collider)) => {
                        let new_pos = collider.get_center_with_transform(*target_transform);
                        mov.update_target_pos(new_pos);
                    }
                    Err(_) => {
                        mov.stop();
                    }
                },
                _ => (),
            }
        }
    }
}

pub fn apply_movement(
    mut query: Query<(&mut Transform, &mut Movement, Option<&mut Turn>)>,
    time: Res<Time>,
) {
    for (mut transform, mut mov, turn) in query.iter_mut() {
        if mov.moving {
            if let Some(mut turn) = turn {
                match turn.status {
                    // just normal movement if not turning at this moment
                    TurnStatus::Idle => {
                        mov.move_or_stop(&mut transform, &time);
                        if !mov.moving {
                            turn.stop();
                        }
                    }
                    TurnStatus::Turning => {
                        if turn.is_small_enough_angle() {
                            mov.move_with_fixed_rotation_or_stop(&mut transform, &time);
                            if !mov.moving {
                                turn.stop();
                            }
                        }
                    }
                }
            } else {
                // just normal movement if no Turn component
                mov.move_or_stop(&mut transform, &time);
            }
        }
    }
}

pub fn update_turns(
    mut turn_query: Query<(Entity, &mut Turn)>,
    mut transform_query: Query<&mut Transform>,
    collider_query: Query<&Collider>,
    time: Res<Time>,
) {
    for (entity, mut turn) in turn_query.iter_mut() {
        match turn.target {
            Target::None => {}
            Target::Static(target_pos) => {
                let mut transform = transform_query.get_mut(entity).unwrap();
                let turn_result = turn.turn_to_target(*transform, target_pos, time.delta_seconds());

                if let Some(rotation) = turn_result {
                    transform.rotation = rotation;
                }
            }
            Target::Dynamic(target) => match transform_query.get_many_mut([entity, target]) {
                Ok([mut transform, target_transform]) => {
                    let turn_result = turn.turn_to_target(
                        *transform,
                        target_transform.translation,
                        time.delta_seconds(),
                    );

                    if let Some(rotation) = turn_result {
                        transform.rotation = rotation;
                    }
                }
                Err(_) => {
                    turn.stop();
                }
            },
            Target::DynamicCenter(target) => match transform_query.get_many_mut([entity, target]) {
                Ok([mut transform, target_transform]) => {
                    let collider = collider_query.get(target).unwrap();
                    let target_pos = collider.get_center_with_transform(*target_transform);
                    let turn_result =
                        turn.turn_to_target(*transform, target_pos, time.delta_seconds());

                    if let Some(rotation) = turn_result {
                        transform.rotation = rotation;
                    }
                }
                Err(_) => {
                    turn.stop();
                }
            },
        }
    }
}

pub fn build_movement_grid(
    target: Vec2,
    units: &Vec<(Entity, &Transform, &Collider)>,
) -> Vec<(Entity, Vec3, f32)> {
    // Determine angle
    let mut sum = Vec2::new(0.0, 0.0);
    let count = units.len() as f32;
    for (_entity, transform, _) in units {
        sum += (target - transform.translation.xz()).normalize();
    }
    let unit_angle = sum / count;
    let angle = unit_angle.y.atan2(unit_angle.x);
    let affine = bevy::math::Affine2::from_angle(angle);
    // Build grid
    // after switching to 3d, increased this cell size by about 67%
    let movement_grid_cell_size: f32 = 5.0;
    let sqrt = count.sqrt();
    let movement_grid_cols = sqrt as usize;
    let movement_grid_rows = if sqrt.fract() != 0.0 {
        movement_grid_cols + 1
    } else {
        movement_grid_cols
    };

    let mut x: usize = 0;
    let mut y: usize = 0;

    let mut result = Vec::new();

    for (entity, transform, collider) in units {
        let mut move_target = Vec2::new(
            x as f32 * movement_grid_cell_size,
            y as f32 * movement_grid_cell_size * -1.0,
        );

        // Offset all points so that the click point is in the middle of the grid (kinda)
        move_target.x -= (movement_grid_cols as f32) * movement_grid_cell_size / 2.0
            - movement_grid_cell_size / 2.0;
        move_target.y += (movement_grid_rows as f32) * movement_grid_cell_size / 2.0
            - movement_grid_cell_size / 2.0;
        // Rotate each point around origin using affine transformation, then translate to mouse position
        let final_target = affine.transform_point2(move_target) + target;
        x += 1;
        if x >= movement_grid_cols {
            x = 0;
            y += 1;
        }
        result.push((
            *entity,
            final_target.extend(transform.translation.y).xzy(),
            collider.inner_radius * 2.0,
        ));
    }
    result
}
