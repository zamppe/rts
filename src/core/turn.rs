use bevy::prelude::*;

use crate::core::Target;

#[derive(Reflect)]
pub enum TurnStatus {
    Idle,
    Turning,
}

#[derive(Component, Reflect)]
pub struct Turn {
    /// `Idle` or `Turning` during this frame
    pub status: TurnStatus,
    /// Rotation speed in radians per second
    speed: f32,
    pub target: Target,
    /// Min angle to target when the entity can start moving.
    /// `None` means that can move immediately (while turning)
    /// `Some(0.0)` means that it needs to turn fully
    pub min_angle_to_target: Option<f32>,
    pub angle_to_target: f32,
}

impl Default for Turn {
    fn default() -> Self {
        Turn {
            status: TurnStatus::Idle,
            speed: 12.0,
            target: Target::None,
            min_angle_to_target: Some(0.2),
            angle_to_target: -1.0,
        }
    }
}

impl Turn {
    pub fn new(speed: f32) -> Self {
        Turn { speed, ..default() }
    }

    pub fn with_no_move_delay(mut self) -> Self {
        self.min_angle_to_target = None;
        self
    }

    pub fn stop(&mut self) {
        self.status = TurnStatus::Idle;
        self.target = Target::None;
        self.angle_to_target = -1.0;
    }

    pub fn is_small_enough_angle(&self) -> bool {
        if self.min_angle_to_target.is_none() {
            return true;
        }

        0.0 <= self.angle_to_target && self.angle_to_target < self.min_angle_to_target.unwrap()
    }

    pub fn turn_to_target(
        &mut self,
        transform: Transform,
        target_pos: Vec3,
        delta_time: f32,
    ) -> Option<Quat> {
        if transform.translation == target_pos {
            self.stop();
            return None;
        }

        let direction = (target_pos - transform.translation).normalize();
        let q_rel = Quat::from_rotation_arc(transform.forward(), direction);
        let q_resulting = q_rel * transform.rotation;

        let (_, angle) = q_rel.to_axis_angle();
        self.angle_to_target = angle;
        // println!("{:?}", angle.to_degrees());

        let factor = (self.speed * delta_time / angle).min(1.0);
        if factor == 1.0 {
            self.status = TurnStatus::Idle;
        } else {
            self.status = TurnStatus::Turning;
        }

        Some(transform.rotation.slerp(q_resulting, factor))
    }
}
