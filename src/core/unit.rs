use bevy::math::Vec3Swizzles;
use bevy::prelude::*;
use bevy::render::view::RenderLayers;
use rand::distributions::{Distribution, Standard};
use rand::seq::IteratorRandom;
use rand::{thread_rng, Rng};

use crate::cleanup::{CleanupBundle, CleanupInGame};
use crate::combat::{AttackTimer, Combat, Weapon};
use crate::constants::{COST_ROCKET_DUDE, COST_SOLDIER, HEALTH_ROCKET_DUDE, HEALTH_SOLDIER};
use crate::core::building::BuildingType;
use crate::core::movement::{Movement, StartMovementEvent};
use crate::core::turn::Turn;
use crate::core::{Collider, GameObjectBundle, Target};
use crate::graphics::materials::CommonMaterials;
use crate::graphics::selection_outline::create_selection_outline;
use crate::graphics::Models;
use crate::player::{Player, PlayerAlignment, PlayerTeam};

#[derive(Event)]
pub struct SpawnUnitEvent {
    pub unit_type: UnitType,
    pub position: Vec3,
    pub player_alignment: PlayerAlignment,
    pub player_team: PlayerTeam,
    pub is_controlled: bool,
    pub health: i32,
    pub target: Option<Vec3>,
}

#[derive(Copy, Clone, Reflect)]
pub enum UnitType {
    Soldier,
    RocketDude,
    Boss,
}

#[derive(Bundle)]
struct UnitBundle {
    pbr_bundle: PbrBundle,
    game_object_bundle: GameObjectBundle,
    movement: Movement,
    turn: Turn,
    combat: Combat,
    cleanup_bundle: CleanupBundle<CleanupInGame>,
}

impl UnitType {
    pub fn cost(&self) -> u32 {
        match self {
            UnitType::Soldier => COST_SOLDIER,
            UnitType::RocketDude => COST_ROCKET_DUDE,
            UnitType::Boss => 420,
        }
    }

    pub fn health(&self) -> i32 {
        match self {
            UnitType::Soldier => HEALTH_SOLDIER,
            UnitType::RocketDude => HEALTH_ROCKET_DUDE,
            UnitType::Boss => i32::MAX,
        }
    }

    pub fn name(&self) -> &'static str {
        match self {
            UnitType::Soldier => "Soldier",
            UnitType::RocketDude => "Rocket dude",
            UnitType::Boss => "Boss",
        }
    }

    pub fn purchase_requirement(&self) -> BuildingType {
        match self {
            UnitType::Soldier => BuildingType::Barracks,
            UnitType::RocketDude => BuildingType::Barracks,
            UnitType::Boss => BuildingType::Barracks,
        }
    }
}

impl Default for UnitType {
    fn default() -> Self {
        Self::Soldier
    }
}

impl Distribution<UnitType> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> UnitType {
        // 75% chance to spawn a Soldier
        match rng.gen_range(1..=4) {
            1 => UnitType::RocketDude,
            _ => UnitType::Soldier,
        }
    }
}

pub fn handle_spawn_unit_events(
    mut commands: Commands,
    mut ev_spawn_unit: EventReader<SpawnUnitEvent>,
    mut start_moving_event_writer: EventWriter<StartMovementEvent>,
    mut meshes: ResMut<Assets<Mesh>>,
    common_materials: Res<CommonMaterials>,
    models: Res<Models>,
) {
    for unit in ev_spawn_unit.read() {
        let unit_size = match unit.unit_type {
            UnitType::Soldier => Vec3::new(2.5, 3.15, 2.57),
            UnitType::RocketDude => Vec3::new(3.0, 4.94, 3.48),
            UnitType::Boss => Vec3::new(10.0, 16.6, 11.7),
        };

        let health = match unit.unit_type {
            UnitType::Soldier => unit.health,
            UnitType::RocketDude => unit.health * 2,
            UnitType::Boss => unit.health,
        };

        let speed = match unit.unit_type {
            UnitType::Soldier => 22.0,
            UnitType::RocketDude => 15.0,
            UnitType::Boss => 10.0,
        };

        let combat = match unit.unit_type {
            UnitType::Soldier => Combat {
                damage: 3,
                attack_range: 15.0,
                aggro_range: 25.0,
                weapon: Weapon::LaserEmitter {
                    beam_width: 3.0,
                    light_power: 1.0,
                },
                out_of_range_timer_duration: 2.3,
                attack_timer: AttackTimer::new(1.0),
                shooting_point: Vec3::new(0.0, 2.5, 1.4036),
            },
            UnitType::RocketDude => Combat {
                damage: 11,
                attack_range: 25.0,
                aggro_range: 30.0,
                weapon: Weapon::RocketLauncher {
                    rocket_size: 4.0,
                    rocket_speed: 30.0,
                    rocket_turn_speed: 8.0,
                },
                out_of_range_timer_duration: 1.5,
                attack_timer: AttackTimer::new(1.6),
                shooting_point: Vec3::new(1.0406, 4.1809, 2.44),
            },
            UnitType::Boss => Combat {
                damage: 30,
                attack_range: 12.0,
                aggro_range: 30.0,
                weapon: Weapon::RocketLauncher {
                    rocket_size: 8.0,
                    rocket_speed: 20.0,
                    rocket_turn_speed: 4.0,
                },
                out_of_range_timer_duration: 3.0,
                attack_timer: AttackTimer::new(2.0),
                shooting_point: Vec3::new(3.4866, 14.008, 7.5216),
            },
        };

        let turn_speed = match unit.unit_type {
            UnitType::Soldier => 15.0,
            UnitType::RocketDude => 10.0,
            UnitType::Boss => 4.0,
        };

        let material = match unit.player_alignment {
            PlayerAlignment::Green => common_materials.green.clone(),
            PlayerAlignment::Red => common_materials.red.clone(),
            PlayerAlignment::Blue => common_materials.blue.clone(),
        };

        let mesh = match unit.unit_type {
            UnitType::Soldier => models.soldier.clone(),
            UnitType::RocketDude => models.rocket_dude.clone(),
            UnitType::Boss => models.boss.clone(),
        };

        let name = format!(
            "{} | {}",
            unit.unit_type.name(),
            unit.player_alignment.name()
        );

        let collider = match unit.unit_type {
            UnitType::Soldier => Collider::new(unit_size).with_center(2.5),
            _ => Collider::new(unit_size),
        };

        let new_unit = commands
            .spawn((
                UnitBundle {
                    pbr_bundle: PbrBundle {
                        mesh,
                        material,
                        transform: Transform::from_translation(unit.position),
                        ..default()
                    },
                    game_object_bundle: GameObjectBundle::unit(
                        unit.player_alignment,
                        unit.player_team,
                        collider,
                        health,
                        unit_size,
                    ),
                    movement: Movement::new(speed),
                    turn: Turn::new(turn_speed),
                    combat,
                    cleanup_bundle: CleanupBundle::in_game(name),
                },
                RenderLayers::default().with(1),
            ))
            .id();

        let selection_outline = create_selection_outline(
            &mut commands,
            unit_size.xz(),
            &mut meshes,
            common_materials.selection.clone(),
        );
        commands.entity(new_unit).add_child(selection_outline);

        if let Some(target) = unit.target {
            start_moving_event_writer.send(StartMovementEvent {
                unit_entity: new_unit,
                target: Target::Static(target),
            });
        }
    }
}

pub fn spawn_random_unit(
    ev_spawn_unit: &mut EventWriter<SpawnUnitEvent>,
    player_query: &Query<&Player>,
) {
    let mut rng = thread_rng();
    let dist_range = -30.0..30.0;
    let x = rng.gen_range(dist_range.clone());
    let y = rng.gen_range(dist_range.clone());
    let target_x = rng.gen_range(dist_range.clone());
    let target_y = rng.gen_range(dist_range);
    let player = player_query.iter().choose(&mut rng).unwrap();
    let unit_type: UnitType = rng.gen();

    ev_spawn_unit.send(SpawnUnitEvent {
        unit_type,
        position: Vec3::new(x, 0.0, y),
        player_alignment: player.player_alignment,
        player_team: player.player_team,
        is_controlled: true,
        health: 20,
        target: Some(Vec3::new(target_x, 0.0, target_y)),
    });
}

pub fn spawn_boss(ev_spawn_unit: &mut EventWriter<SpawnUnitEvent>) {
    let mut rng = thread_rng();
    let dist_range = -3.0..3.0;
    let x = rng.gen_range(dist_range.clone());
    let y = rng.gen_range(dist_range.clone());
    let target_x = rng.gen_range(dist_range.clone());
    let target_y = rng.gen_range(dist_range);

    ev_spawn_unit.send(SpawnUnitEvent {
        unit_type: UnitType::Boss,
        position: Vec3::new(x, 0.0, y),
        player_alignment: PlayerAlignment::Green,
        player_team: PlayerTeam::A,
        is_controlled: true,
        health: i32::MAX,
        target: Some(Vec3::new(target_x, 0.0, target_y)),
    });
}
