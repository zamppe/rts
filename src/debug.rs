use bevy::prelude::*;
use bevy_inspector_egui::quick::WorldInspectorPlugin;

use crate::ai::Brain;
use crate::combat::{AttackTarget, AttackTimer, Combat};
use crate::core::building::{Building, Construction};
use crate::core::movement::Movement;
use crate::core::turn::Turn;
use crate::core::{Collider, GameObject, Health, UserControl};
use crate::graphics::materials::{LaserBeamMaterial, OutlineMaterial, RangeIndicatorMaterial};
use crate::input::mouse::MouseState;
use crate::player::{Player, PlayerAlignment, PlayerTeam};
use crate::ui::buttons::{DevButton, PanelButton};

pub struct DebugPlugin;

impl Plugin for DebugPlugin {
    fn build(&self, app: &mut App) {
        if cfg!(debug_assertions) {
            app.add_plugins(WorldInspectorPlugin::new())
                .register_type::<Movement>()
                .register_type::<PlayerAlignment>()
                .register_type::<UserControl>()
                .register_type::<PanelButton>()
                .register_type::<DevButton>()
                .register_type::<Collider>()
                .register_type::<Health>()
                .register_type::<Combat>()
                .register_type::<PlayerTeam>()
                .register_type::<Turn>()
                .register_type::<AttackTarget>()
                .register_type::<GameObject>()
                .register_type::<Player>()
                .register_type::<Brain>()
                .register_type::<Building>()
                .register_type::<Construction>()
                .register_type::<MouseState>()
                .register_asset_reflect::<RangeIndicatorMaterial>()
                .register_asset_reflect::<LaserBeamMaterial>()
                .register_asset_reflect::<OutlineMaterial>()
                .register_type::<AttackTimer>();
        }
    }
}
