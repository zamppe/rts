use bevy::prelude::*;

use crate::ai::RtsAiPlugin;
use crate::audio::RtsAudioPlugin;
use crate::camera::RtsCameraPlugin;
use crate::cleanup::CleanupPlugin;
use crate::combat::RtsCombatPlugin;
use crate::core::RtsCorePlugin;
use crate::game_over::GameOverPlugin;
use crate::graphics::GraphicsPlugin;
use crate::input::RtsInputPlugin;
use crate::loading::LoadingPlugin;
use crate::map::MapPlugin;
use crate::menu::MainMenuPlugin;
use crate::player::RtsPlayerPlugin;
use crate::ui::RtsUiPlugin;

pub struct RtsGamePlugin;

impl Plugin for RtsGamePlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins((
            RtsCameraPlugin,
            RtsInputPlugin,
            MapPlugin,
            RtsPlayerPlugin,
            RtsCorePlugin,
            RtsUiPlugin,
            GraphicsPlugin,
            RtsAudioPlugin,
            RtsCombatPlugin,
            RtsAiPlugin,
            MainMenuPlugin,
            GameOverPlugin,
            CleanupPlugin,
            LoadingPlugin,
        ));
    }
}
