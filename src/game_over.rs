use bevy::app::AppExit;
use bevy::prelude::*;
use bevy::text::TextLayoutInfo;
use rand::{thread_rng, Rng};

use crate::cleanup::CleanupBundle;
use crate::constants::{BUTTON_COLOR, BUTTON_COLOR_HOVERED};
use crate::core::GameState;
use crate::ui::UiAssets;

pub struct GameOverPlugin;

impl Plugin for GameOverPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            OnEnter(GameState::GameOver),
            (
                spawn_f,
                spawn_game_over_camera,
                spawn_game_over_text,
                spawn_quit_button,
                spawn_background,
            ),
        )
        .add_systems(
            Update,
            (press_f, update_f, handle_quit_button).run_if(in_state(GameState::GameOver)),
        );
    }
}

#[derive(Component)]
struct F;

#[derive(Component, Deref, DerefMut)]
struct Velocity(Vec2);

#[derive(Component)]
struct QuitButton;

const F_MIN_SPEED: f32 = 200.0;
const F_MAX_SPEED: f32 = 900.0;

const F_MIN_AMOUNT: u32 = 20;
const F_MAX_AMOUNT: u32 = 32;

const F_MIN_SIZE: f32 = 80.0;
const F_MAX_SIZE: f32 = 320.0;

const LINES: [&str; 23] = [
    "Well, that was embarrassing!",
    "Did we mention this is hard?",
    "So close, yet so far... Not really",
    "Award for most creative defeat: you!",
    "Expert in demolishing own base!",
    "You're really good at this... Said no one ever",
    "Congratulations on the new record for fastest defeat!",
    "We've seen better days. Much better",
    "Don't quit your day job, Commander!",
    "Let's pretend that was just a practice round",
    "Headquarters decided to take an unplanned vacation",
    "It's not losing, it's learning... Right?",
    "On the bright side, you can only go up from here!",
    "Next time, try not to press the big red button!",
    "That strategy was... Interesting?",
    "Maybe next time send in the clowns instead?",
    "Your base called, it misses its walls!",
    "Next time: more strategy, less panicking!",
    "A for effort, F for execution!",
    "Remember when we had a base? Good times...",
    "Maybe we shouldn't have skipped the tutorial...",
    "Our psychic predicted victory... We should fire them.",
    "Looks like we brought a knife to a gunfight. Again.",
];

impl Velocity {
    fn generate() -> Self {
        let mut rng = thread_rng();
        let dist_range = -1.0..1.0;
        let x = rng.gen_range(dist_range.clone());
        let y = rng.gen_range(dist_range.clone());
        let speed = rng.gen_range(F_MIN_SPEED..F_MAX_SPEED);
        let velocity = Vec2::new(x, y).normalize() * speed;

        Velocity(velocity)
    }
}

fn spawn_f(mut commands: Commands) {
    let mut rng = thread_rng();
    let amount = rng.gen_range(F_MIN_AMOUNT..=F_MAX_AMOUNT);

    for _ in 0..amount {
        commands.spawn((
            Text2dBundle {
                text: Text::from_section(
                    "F",
                    TextStyle {
                        font_size: rng.gen_range(F_MIN_SIZE..F_MAX_SIZE),
                        color: Color::BLACK,
                        ..default()
                    },
                )
                .with_alignment(TextAlignment::Center),
                ..default()
            },
            CleanupBundle::game_over("F"),
            F,
            Velocity::generate(),
        ));
    }
}

fn spawn_game_over_camera(mut commands: Commands) {
    commands.spawn((
        Camera2dBundle::default(),
        CleanupBundle::game_over("Game over camera"),
    ));
}

fn pick_random_line() -> &'static str {
    let mut rng = thread_rng();
    let index = rng.gen_range(0..LINES.len());
    LINES[index]
}

fn spawn_game_over_text(mut commands: Commands, ui_assets: Res<UiAssets>) {
    let text = commands
        .spawn((
            TextBundle {
                text: Text::from_section(
                    pick_random_line(),
                    TextStyle {
                        font: ui_assets.font.clone(),
                        font_size: 48.0,
                        color: Color::BLACK,
                    },
                ),
                ..default()
            },
            Name::new("Game over text"),
        ))
        .id();

    let text_node = commands
        .spawn((
            NodeBundle {
                background_color: Color::rgba(0.2, 0.59, 1.0, 0.9).into(),
                style: Style {
                    padding: UiRect {
                        left: Val::Px(30.0),
                        right: Val::Px(30.0),
                        top: Val::Px(20.0),
                        bottom: Val::Px(20.0),
                    },
                    ..default()
                },
                ..default()
            },
            Name::new("Game over text node"),
        ))
        .add_child(text)
        .id();

    let stats_text = commands
        .spawn((
            TextBundle {
                text: Text::from_section(
                    "Stats",
                    TextStyle {
                        font: ui_assets.font.clone(),
                        font_size: 48.0,
                        color: Color::BLACK,
                    },
                ),
                ..default()
            },
            Name::new("Game over stats text"),
        ))
        .id();

    let stats_node = commands
        .spawn((
            NodeBundle {
                background_color: Color::rgba(0.2, 0.59, 1.0, 0.9).into(),
                style: Style {
                    width: Val::Px(900.0),
                    height: Val::Px(750.0),
                    padding: UiRect {
                        left: Val::Px(30.0),
                        right: Val::Px(30.0),
                        top: Val::Px(20.0),
                        bottom: Val::Px(20.0),
                    },
                    align_items: AlignItems::Center,
                    flex_direction: FlexDirection::Column,
                    ..default()
                },
                ..default()
            },
            Name::new("Game over stats node"),
        ))
        .add_child(stats_text)
        .id();

    let panel = commands
        .spawn((
            NodeBundle {
                style: Style {
                    align_items: AlignItems::Center,
                    flex_direction: FlexDirection::Column,
                    row_gap: Val::Px(40.0),
                    ..default()
                },
                ..default()
            },
            Name::new("Game over panel"),
        ))
        .add_child(text_node)
        .add_child(stats_node)
        .id();

    commands
        .spawn((
            NodeBundle {
                style: Style {
                    position_type: PositionType::Absolute,
                    margin: UiRect::new(Val::Auto, Val::Auto, Val::Vh(10.0), Val::Auto),
                    ..default()
                },
                ..default()
            },
            CleanupBundle::game_over("Game over UI container"),
        ))
        .add_child(panel);
}

fn spawn_quit_button(mut commands: Commands, ui_assets: Res<UiAssets>) {
    let quit_button = commands
        .spawn((
            ButtonBundle {
                style: Style {
                    width: Val::Px(180.0),
                    height: Val::Px(54.0),
                    justify_content: JustifyContent::Center,
                    align_items: AlignItems::Center,
                    ..default()
                },
                background_color: BUTTON_COLOR.into(),
                ..default()
            },
            Name::new("Quit button"),
            QuitButton,
        ))
        .with_children(|parent| {
            parent.spawn((
                TextBundle {
                    text: Text::from_section(
                        "Quit game",
                        TextStyle {
                            font: ui_assets.font.clone(),
                            font_size: 30.0,
                            color: Color::WHITE,
                        },
                    ),
                    ..default()
                },
                Name::new("Quit button text"),
            ));
        })
        .id();

    commands
        .spawn((
            NodeBundle {
                style: Style {
                    position_type: PositionType::Absolute,
                    margin: UiRect::new(Val::Auto, Val::Percent(2.0), Val::Auto, Val::Percent(2.0)),
                    ..default()
                },
                ..default()
            },
            CleanupBundle::game_over("Quit button container"),
        ))
        .add_child(quit_button);
}

fn spawn_background(mut commands: Commands) {
    commands.spawn((
        SpriteBundle {
            sprite: Sprite {
                color: Color::hex("#87C1FF").unwrap(),
                custom_size: Some(Vec2::new(10_000.0, 10_000.0)),
                ..default()
            },
            transform: Transform::from_xyz(0.0, 0.0, -1.0),
            ..default()
        },
        CleanupBundle::game_over("Game over background"),
    ));
}

fn press_f(keyboard: Res<Input<KeyCode>>, mut next_state: ResMut<NextState<GameState>>) {
    if keyboard.just_pressed(KeyCode::F) {
        next_state.set(GameState::MainMenu);
    }
}

fn update_f(
    mut f_query: Query<(&mut Transform, &mut Velocity, &TextLayoutInfo), With<F>>,
    time: Res<Time>,
    window_query: Query<&Window>,
) {
    let window = window_query.get_single().unwrap();
    let half_win_width = window.width() / 2.0;
    let half_win_height = window.height() / 2.0;

    for (mut transform, mut velocity, text_layout_info) in &mut f_query {
        if text_layout_info.glyphs.is_empty() {
            return;
        }

        let half_f_width = text_layout_info.glyphs[0].size.x / 2.0;
        let half_f_height = text_layout_info.glyphs[0].size.y / 2.0;
        let x = half_win_width - half_f_width;
        let y = half_win_height - half_f_height;

        if transform.translation.x.abs() > x {
            transform.translation.x = x.copysign(transform.translation.x);
            velocity.x = -velocity.x;
        }

        if transform.translation.y.abs() > y {
            transform.translation.y = y.copysign(transform.translation.y);
            velocity.y = -velocity.y;
        }

        transform.translation.x += velocity.x * time.delta_seconds();
        transform.translation.y += velocity.y * time.delta_seconds();
    }
}

fn handle_quit_button(
    mut interaction_query: Query<
        (&Interaction, &mut BackgroundColor),
        (Changed<Interaction>, With<QuitButton>),
    >,
    mut exit: EventWriter<AppExit>,
) {
    if let Ok((interaction, mut color)) = interaction_query.get_single_mut() {
        match interaction {
            Interaction::Pressed => {
                exit.send(AppExit);
            }
            Interaction::Hovered => {
                color.0 = BUTTON_COLOR_HOVERED;
            }
            Interaction::None => {
                color.0 = BUTTON_COLOR;
            }
        }
    }
}
