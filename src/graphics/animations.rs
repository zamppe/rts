use std::time::Duration;

use bevy::prelude::*;
use bevy_tweening::{lens::*, *};

#[derive(Debug, Copy, Clone, PartialEq)]
struct CameraFovLens {
    pub start: f32,
    pub end: f32,
}

impl Lens<Projection> for CameraFovLens {
    fn lerp(&mut self, target: &mut Projection, ratio: f32) {
        if let Projection::Perspective(ref mut proj) = &mut *target {
            let value = self.start + (self.end - self.start) * ratio;
            proj.fov = value;
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
struct CameraTransformLens {
    pub start: Transform,
    pub end: Transform,
}

impl Lens<Transform> for CameraTransformLens {
    fn lerp(&mut self, target: &mut Transform, ratio: f32) {
        let translation_value =
            self.start.translation + (self.end.translation - self.start.translation) * ratio;
        target.translation = translation_value;
        target.rotation = self.start.rotation.slerp(self.end.rotation, ratio);
    }
}

pub fn camera_fov_animator(start: f32, end: f32, duration: u64) -> Animator<Projection> {
    let tween = Tween::new(
        EaseFunction::QuadraticOut,
        Duration::from_millis(duration),
        CameraFovLens { start, end },
    );

    Animator::new(tween)
}

pub fn camera_transform_animator(
    start: Transform,
    end: Transform,
    duration: u64,
) -> Animator<Transform> {
    let tween = Tween::new(
        EaseFunction::QuadraticOut,
        Duration::from_millis(duration),
        CameraTransformLens { start, end },
    );

    Animator::new(tween)
}
