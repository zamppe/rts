use bevy::math::Affine2;
use bevy::pbr::{NotShadowCaster, NotShadowReceiver};
use bevy::prelude::*;

use crate::camera::MainCamera;
use crate::cleanup::CleanupBundle;
use crate::graphics::materials::CommonMaterials;
use crate::input::mouse::{MouseMode, MouseState};

pub const BOX_SELECTION_MIN_DRAG_DISTANCE: f32 = 1.0;

#[derive(Component)]
pub struct BoxSelection {
    pub start_point: Vec2,
    pub position: Vec3,
    pub size: Vec2,
    pub rotation: f32,
}

pub fn create_box_selection(
    mut commands: Commands,
    mouse_state: Res<MouseState>,
    common_materials: Res<CommonMaterials>,
    mut meshes: ResMut<Assets<Mesh>>,
    camera_query: Query<&Transform, With<MainCamera>>,
) {
    let Some(world_pos) = mouse_state.click_start_point else {
        return;
    };
    let position = Vec3::new(world_pos.x, 0.1, world_pos.y);
    let size = Vec2::ZERO;
    let camera_transform = camera_query.single();
    let (camera_y_angle, _, _) = camera_transform.rotation.to_euler(EulerRot::YXZ);

    commands.spawn((
        MaterialMeshBundle {
            mesh: meshes.add(Mesh::from(shape::Cube::new(1.0))),
            material: common_materials.box_selection.clone(),
            transform: Transform {
                translation: position,
                rotation: Quat::from_rotation_y(camera_y_angle),
                ..default()
            },
            ..default()
        },
        NotShadowReceiver,
        NotShadowCaster,
        BoxSelection {
            start_point: world_pos,
            position,
            size,
            rotation: camera_y_angle,
        },
        CleanupBundle::in_game("BoxSelection"),
    ));
}

pub fn update_box_selection(
    mouse_state: Res<MouseState>,
    mut mouse_mode_state: ResMut<NextState<MouseMode>>,
    mut box_selection_query: Query<(&mut Transform, &mut BoxSelection), Without<Camera3d>>,
) {
    // If left mouse button was released, switch to normal mouse mode
    if !mouse_state.is_lmb_pressed {
        mouse_mode_state.set(MouseMode::Normal);
        return;
    }

    // Update graphics for box selection
    if let Ok((mut box_transform, mut box_selection)) = box_selection_query.get_single_mut() {
        if let Some(world_pos_2d) = mouse_state.world_pos {
            // Calculate the midpoint (center) of the current selection box
            let midpoint = (world_pos_2d + box_selection.start_point) / 2.0;

            // Translate to origin
            let translated_world_pos = world_pos_2d - midpoint;
            let translated_start_point = box_selection.start_point - midpoint;

            // Rotation
            let rotation = Affine2::from_angle(box_selection.rotation);
            let rotated_world_pos = rotation.transform_point2(translated_world_pos);
            let rotated_start_point = rotation.transform_point2(translated_start_point);

            // Translate back
            let final_world_pos = rotated_world_pos + midpoint;
            let final_start_point = rotated_start_point + midpoint;

            // Compute the box selection as usual
            let delta = final_world_pos - final_start_point;
            let new_pos = delta / 2.0;

            box_transform.scale = Vec3::new(delta.abs().x, 0.0, delta.abs().y);
            box_transform.translation.x = final_start_point.x + new_pos.x;
            box_transform.translation.z = final_start_point.y + new_pos.y;

            box_selection.position = box_transform.translation;
            box_selection.size = delta.abs();
        }
    }
}

pub fn despawn_box_selection(
    mut commands: Commands,
    box_selection_entity_query: Query<Entity, With<BoxSelection>>,
) {
    if let Ok(box_selection) = box_selection_entity_query.get_single() {
        commands.entity(box_selection).despawn_recursive();
    }
}
