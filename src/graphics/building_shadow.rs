use bevy::prelude::*;

use crate::core::building::BuildingType;
use crate::input::mouse::MouseState;

#[derive(Component)]
pub struct BuildingShadow {
    pub building_type: BuildingType,
}

pub fn despawn_building_shadow(
    mut commands: Commands,
    building_shadow_query: Query<Entity, With<BuildingShadow>>,
) {
    if let Ok(entity) = building_shadow_query.get_single() {
        commands.entity(entity).despawn_recursive();
    }
}

pub fn update_building_shadow(
    mouse_state: Res<MouseState>,
    mut building_shadow_query: Query<&mut Transform, With<BuildingShadow>>,
) {
    if let Some(world_pos) = mouse_state.world_pos {
        if let Ok(mut transform) = building_shadow_query.get_single_mut() {
            transform.translation = Vec3::new(world_pos.x, 0.1, world_pos.y);
        };
    }
}
