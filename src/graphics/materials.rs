use bevy::pbr::{MaterialPipeline, MaterialPipelineKey};
use bevy::prelude::*;
use bevy::render::mesh::MeshVertexBufferLayout;
use bevy::render::render_resource::{
    AsBindGroup, RenderPipelineDescriptor, ShaderRef, SpecializedMeshPipelineError,
};

use crate::constants::{COLOR_BLUE_ALIGNMENT, COLOR_GREEN_ALIGNMENT, COLOR_RED_ALIGNMENT};

#[derive(Resource, Reflect)]
pub struct CommonMaterials {
    pub green: Handle<StandardMaterial>,
    pub red: Handle<StandardMaterial>,
    pub blue: Handle<StandardMaterial>,
    pub selection: Handle<StandardMaterial>,
    pub movement_indicator: Handle<StandardMaterial>,
    pub green_building_shadow: Handle<StandardMaterial>,
    pub red_building_shadow: Handle<StandardMaterial>,
    pub blue_building_shadow: Handle<StandardMaterial>,
    pub building_shadow_plane: Handle<StandardMaterial>,
    pub skybox: Handle<StandardMaterial>,
    pub box_selection: Handle<OutlineMaterial>,
    pub attack_range: Handle<RangeIndicatorMaterial>,
    pub aggro_range: Handle<RangeIndicatorMaterial>,
    pub green_laser_beam: Handle<LaserBeamMaterial>,
    pub red_laser_beam: Handle<LaserBeamMaterial>,
    pub blue_laser_beam: Handle<LaserBeamMaterial>,
    pub camera_projection: Handle<OutlineMaterial>,
}

#[derive(Asset, AsBindGroup, Reflect, Debug, Clone, Default)]
#[reflect(Default, Debug)]
pub struct RangeIndicatorMaterial {
    #[uniform(0)]
    color: Color,
    alpha_mode: AlphaMode,
}

impl Material for RangeIndicatorMaterial {
    fn fragment_shader() -> ShaderRef {
        "shaders/range_indicator.wgsl".into()
    }

    fn alpha_mode(&self) -> AlphaMode {
        self.alpha_mode
    }
}

#[derive(Asset, AsBindGroup, Reflect, Debug, Clone, Default)]
#[reflect(Default, Debug)]
pub struct LaserBeamMaterial {
    #[uniform(0)]
    color: Color,
    alpha_mode: AlphaMode,
}

impl Material for LaserBeamMaterial {
    fn vertex_shader() -> ShaderRef {
        "shaders/laser_beam.wgsl".into()
    }

    fn fragment_shader() -> ShaderRef {
        "shaders/laser_beam.wgsl".into()
    }

    fn alpha_mode(&self) -> AlphaMode {
        self.alpha_mode
    }

    fn specialize(
        _pipeline: &MaterialPipeline<Self>,
        descriptor: &mut RenderPipelineDescriptor,
        _layout: &MeshVertexBufferLayout,
        _key: MaterialPipelineKey<Self>,
    ) -> Result<(), SpecializedMeshPipelineError> {
        descriptor.primitive.cull_mode = None;
        Ok(())
    }
}

#[derive(Asset, AsBindGroup, Reflect, Debug, Clone)]
#[reflect(Default, Debug)]
pub struct OutlineMaterial {
    #[uniform(0)]
    color: Color,
    #[uniform(0)]
    thickness: f32,
    #[uniform(0)]
    main_alpha: f32,
    #[uniform(0)]
    outline_alpha: f32,
    alpha_mode: AlphaMode,
}

impl Default for OutlineMaterial {
    fn default() -> Self {
        OutlineMaterial {
            color: Color::WHITE,
            thickness: 5.0,
            main_alpha: 0.2,
            outline_alpha: 1.0,
            alpha_mode: AlphaMode::Blend,
        }
    }
}

impl Material for OutlineMaterial {
    fn fragment_shader() -> ShaderRef {
        "shaders/outline.wgsl".into()
    }

    fn alpha_mode(&self) -> AlphaMode {
        self.alpha_mode
    }
}

pub fn prepare_materials(
    mut commands: Commands,
    mut materials: ResMut<Assets<StandardMaterial>>,
    mut range_materials: ResMut<Assets<RangeIndicatorMaterial>>,
    mut laser_materials: ResMut<Assets<LaserBeamMaterial>>,
    mut outline_materials: ResMut<Assets<OutlineMaterial>>,
) {
    commands.insert_resource(CommonMaterials {
        green: materials.add(COLOR_GREEN_ALIGNMENT.into()),
        red: materials.add(COLOR_RED_ALIGNMENT.into()),
        blue: materials.add(COLOR_BLUE_ALIGNMENT.into()),
        selection: materials.add(StandardMaterial {
            base_color: Color::rgba(0.0, 1.0, 0.0, 0.3),
            alpha_mode: AlphaMode::Add,
            unlit: true,
            ..default()
        }),
        box_selection: outline_materials.add(OutlineMaterial {
            color: Color::rgba(0.0, 1.0, 1.0, 1.0),
            ..default()
        }),
        attack_range: range_materials.add(RangeIndicatorMaterial {
            color: Color::rgba(0.0, 1.0, 1.0, 0.4),
            alpha_mode: AlphaMode::Blend,
        }),
        aggro_range: range_materials.add(RangeIndicatorMaterial {
            color: Color::rgba(1.0, 0.65, 0.0, 0.4),
            alpha_mode: AlphaMode::Blend,
        }),
        green_laser_beam: laser_materials.add(LaserBeamMaterial {
            color: COLOR_GREEN_ALIGNMENT,
            alpha_mode: AlphaMode::Blend,
        }),
        red_laser_beam: laser_materials.add(LaserBeamMaterial {
            color: COLOR_RED_ALIGNMENT,
            alpha_mode: AlphaMode::Blend,
        }),
        blue_laser_beam: laser_materials.add(LaserBeamMaterial {
            color: COLOR_BLUE_ALIGNMENT,
            alpha_mode: AlphaMode::Blend,
        }),
        movement_indicator: materials.add(StandardMaterial {
            base_color: Color::DARK_GREEN,
            unlit: true,
            alpha_mode: AlphaMode::Blend,
            ..default()
        }),
        green_building_shadow: materials.add(StandardMaterial {
            base_color: COLOR_GREEN_ALIGNMENT,
            alpha_mode: AlphaMode::Add,
            ..default()
        }),
        red_building_shadow: materials.add(StandardMaterial {
            base_color: COLOR_GREEN_ALIGNMENT,
            alpha_mode: AlphaMode::Add,
            ..default()
        }),
        blue_building_shadow: materials.add(StandardMaterial {
            base_color: COLOR_GREEN_ALIGNMENT,
            alpha_mode: AlphaMode::Add,
            ..default()
        }),
        building_shadow_plane: materials.add(StandardMaterial {
            base_color: Color::GREEN,
            alpha_mode: AlphaMode::Add,
            ..default()
        }),
        skybox: materials.add(StandardMaterial {
            base_color: Color::hex("888888").unwrap(),
            unlit: true,
            cull_mode: None,
            ..default()
        }),
        camera_projection: outline_materials.add(OutlineMaterial {
            color: Color::WHITE,
            main_alpha: 0.0,
            ..default()
        }),
    });
}
