use bevy::math::vec2;
use bevy::prelude::*;
use bevy_health_bar3d::prelude::WithBar;
use bevy_tweening::TweeningPlugin;
use rand::{thread_rng, Rng};

use box_selection::{create_box_selection, despawn_box_selection, update_box_selection};
use building_shadow::{despawn_building_shadow, update_building_shadow};
use selection_outline::SelectionOutline;

use crate::core::{GameState, Health, PlayingState, UserControl};
use crate::graphics::materials::{
    prepare_materials, LaserBeamMaterial, OutlineMaterial, RangeIndicatorMaterial,
};
use crate::graphics::movement_indicator::{
    handle_movement_indicator_events, start_playing_animations, update_movement_indicator,
    CreateMovementIndicatorEvent,
};
use crate::input::mouse::MouseMode;

pub mod animations;
pub mod box_selection;
pub mod building_shadow;
pub mod materials;
pub mod movement_indicator;
pub mod range_indicator;
pub mod selection_outline;
pub mod turn_indicator;

pub struct GraphicsPlugin;

impl Plugin for GraphicsPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(Msaa::Sample4)
            .add_event::<CreateMovementIndicatorEvent>()
            .add_plugins((
                TweeningPlugin,
                MaterialPlugin::<RangeIndicatorMaterial>::default(),
                MaterialPlugin::<LaserBeamMaterial>::default(),
                MaterialPlugin::<OutlineMaterial>::default(),
            ))
            .add_systems(
                PreStartup,
                (
                    prepare_materials,
                    load_animations,
                    load_models,
                    load_model_materials,
                ),
            )
            .add_systems(
                Update,
                (
                    start_playing_animations,
                    handle_movement_indicator_events
                        .run_if(on_event::<CreateMovementIndicatorEvent>()),
                )
                    .run_if(in_state(GameState::InGame))
                    .run_if(in_state(PlayingState::Playing)),
            )
            .add_systems(
                Update,
                (
                    update_selection_outline_and_health_bar,
                    update_movement_indicator,
                )
                    .run_if(in_state(GameState::InGame)),
            )
            .add_systems(
                OnEnter(MouseMode::BoxSelection),
                create_box_selection.run_if(in_state(GameState::InGame)),
            )
            .add_systems(
                OnExit(MouseMode::BoxSelection),
                despawn_box_selection.run_if(in_state(GameState::InGame)),
            )
            .add_systems(
                Update,
                update_box_selection
                    .run_if(in_state(MouseMode::BoxSelection))
                    .run_if(in_state(GameState::InGame)),
            )
            .add_systems(
                Update,
                update_building_shadow
                    .run_if(in_state(MouseMode::PlacingBuilding))
                    .run_if(in_state(GameState::InGame)),
            )
            .add_systems(
                OnExit(MouseMode::PlacingBuilding),
                despawn_building_shadow.run_if(in_state(GameState::InGame)),
            );
    }
}

#[derive(Resource)]
pub struct Models {
    pub ground: ModelBundle,
    pub turn_indicator: ModelBundle,
    pub movement_indicator: Handle<Scene>,
    pub laser_beam: Handle<Mesh>,
    pub rocket_projectile: ModelBundle,
    pub soldier: Handle<Mesh>,
    pub rocket_dude: Handle<Mesh>,
    pub boss: Handle<Mesh>,
    pub headquarters: Handle<Mesh>,
    pub barracks: Handle<Mesh>,
    pub gatherer: Handle<Mesh>,
    pub laser_tower: Handle<Mesh>,
    pub lamp: ModelBundle,
}

pub struct ModelBundle {
    pub mesh: Handle<Mesh>,
    pub mat: Handle<StandardMaterial>,
}

impl ModelBundle {
    pub fn new(asset_server: &Res<AssetServer>, path: &'static str) -> Self {
        ModelBundle {
            mesh: asset_server.load(path.to_owned() + "#Mesh0/Primitive0"),
            mat: asset_server.load(path.to_owned() + "#Material0"),
        }
    }
}

#[derive(Resource, Reflect)]
pub struct ModelMaterials {
    pub ground: Handle<StandardMaterial>,
    pub turn_indicator: Handle<StandardMaterial>,
    pub rocket_projectile: Handle<StandardMaterial>,
    pub movement_indicator: Handle<StandardMaterial>,
}

#[derive(Resource)]
pub struct Animations(Vec<Handle<AnimationClip>>);

#[allow(dead_code)]
pub fn get_randomized_color(base_color: Color, leeway: f32) -> Color {
    let mut rng = thread_rng();
    let mut new_color: Vec<f32> = Vec::new();
    for color_component in base_color.as_rgba_f32().iter() {
        let color_min = (color_component - leeway).clamp(0.0, 1.0);
        let color_max = (color_component + leeway).clamp(0.0, 1.0);
        let new_cc = rng.gen_range(color_min..color_max);
        new_color.push(new_cc);
    }

    Color::rgb(new_color[0], new_color[1], new_color[2])
}

pub fn get_randomized_direction_vec2() -> Vec2 {
    let mut rng = thread_rng();
    let x = rng.gen_range(-1.0..1.0);
    let y = rng.gen_range(-1.0..1.0);

    vec2(x, y)
}

fn load_animations(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands.insert_resource(Animations(vec![
        asset_server.load("models/movement_indicator.glb#Animation0")
    ]));
}

fn load_models(mut commands: Commands, ass: Res<AssetServer>) {
    commands.insert_resource(Models {
        ground: ModelBundle::new(&ass, "models/ground.glb"),
        turn_indicator: ModelBundle::new(&ass, "models/turn_indicator.glb"),
        movement_indicator: ass.load("models/movement_indicator.glb#Scene0"),
        laser_beam: ass.load("models/laser_beam.glb#Mesh0/Primitive0"),
        rocket_projectile: ModelBundle::new(&ass, "models/rocket_projectile.glb"),
        soldier: ass.load("models/soldier.glb#Mesh0/Primitive0"),
        rocket_dude: ass.load("models/rocket_dude.glb#Mesh0/Primitive0"),
        boss: ass.load("models/boss.glb#Mesh0/Primitive0"),
        headquarters: ass.load("models/headquarters.glb#Mesh0/Primitive0"),
        barracks: ass.load("models/barracks.glb#Mesh0/Primitive0"),
        gatherer: ass.load("models/gatherer.glb#Mesh0/Primitive0"),
        laser_tower: ass.load("models/laser_tower.glb#Mesh0/Primitive0"),
        lamp: ModelBundle::new(&ass, "models/lamp.glb"),
    });
}

fn load_model_materials(mut commands: Commands, ass: Res<AssetServer>) {
    commands.insert_resource(ModelMaterials {
        ground: ass.load("models/ground.glb#Material0"),
        turn_indicator: ass.load("models/turn_indicator.glb#Material0"),
        movement_indicator: ass.load("models/movement_indicator.glb#Material0"),
        rocket_projectile: ass.load("models/rocket_projectile.glb#Material0"),
    });
}

fn update_selection_outline_and_health_bar(
    parent_user_control_query: Query<
        (&Children, &UserControl, &WithBar<Health>),
        Changed<UserControl>,
    >,
    selection_query: Query<Entity, With<SelectionOutline>>,
    mut vis_query: Query<&mut Visibility>,
) {
    for (children, user_control, bar) in parent_user_control_query.iter() {
        for child in children.iter() {
            if let Ok(selection_outline) = selection_query.get(*child) {
                let health_bar = bar.get();

                if let Ok([mut selection_vis, mut bar_vis]) =
                    vis_query.get_many_mut([selection_outline, health_bar])
                {
                    match user_control.selected {
                        true => {
                            *selection_vis = Visibility::Visible;
                            *bar_vis = Visibility::Visible;
                        }
                        false => {
                            *selection_vis = Visibility::Hidden;
                            *bar_vis = Visibility::Hidden;
                        }
                    };
                }
            }
        }
    }
}
