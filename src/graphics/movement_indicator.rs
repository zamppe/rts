use std::f32::consts::FRAC_PI_2;

use bevy::pbr::{NotShadowCaster, NotShadowReceiver};
use bevy::prelude::*;
use bevy::render::view::RenderLayers;
use bevy_scene_hook::{HookedSceneBundle, SceneHook};

use crate::cleanup::CleanupBundle;
use crate::graphics::materials::CommonMaterials;
use crate::graphics::{Animations, Models};

#[derive(Component)]
pub struct MovementIndicator {
    duration: f32,
    top_parent: Entity,
    minimap_indicator: Entity,
}

#[derive(Event)]
pub struct CreateMovementIndicatorEvent {
    pub position: Vec2,
    pub size: f32,
}

pub fn handle_movement_indicator_events(
    mut cmds: Commands,
    mut events: EventReader<CreateMovementIndicatorEvent>,
    models: Res<Models>,
    common_materials: Res<CommonMaterials>,
    mut meshes: ResMut<Assets<Mesh>>,
) {
    for ev in events.read() {
        let material = common_materials.movement_indicator.clone();

        let minimap_indicator = cmds
            .spawn((
                MaterialMeshBundle {
                    mesh: meshes.add(Mesh::from(shape::Circle::new(1.0))),
                    material: common_materials.movement_indicator.clone(),
                    transform: Transform {
                        translation: Vec3::new(0.0, 50.0, 0.0),
                        rotation: Quat::from_rotation_x(-FRAC_PI_2),
                        ..default()
                    },
                    ..default()
                },
                RenderLayers::layer(1),
                NotShadowReceiver,
                NotShadowCaster,
                Name::new("Minimap movement indicator"),
            ))
            .id();

        let mut ecmds = cmds.spawn_empty();
        let entity_id = ecmds.id();

        ecmds
            .insert((
                HookedSceneBundle {
                    scene: SceneBundle {
                        scene: models.movement_indicator.clone(),
                        transform: Transform {
                            translation: Vec3::new(ev.position.x, 0.0, ev.position.y),
                            scale: Vec3::splat(ev.size),
                            ..default()
                        },
                        ..default()
                    },
                    hook: SceneHook::new(move |entity, ecmds| {
                        match entity.get::<Name>().map(|t| t.as_str()) {
                            // 0.75 is the duration of full animation
                            Some("Arrow") => ecmds.insert(MovementIndicator {
                                duration: 0.75 * 1.0,
                                top_parent: entity_id,
                                minimap_indicator,
                            }),
                            Some("Arrow mesh") => ecmds
                                .remove::<Handle<StandardMaterial>>()
                                .insert(material.clone()),
                            _ => ecmds,
                        };
                    }),
                },
                CleanupBundle::in_game("MovementIndicator"),
            ))
            .add_child(minimap_indicator);
    }
}

pub fn start_playing_animations(
    animations: Res<Animations>,
    mut players: Query<&mut AnimationPlayer, Added<AnimationPlayer>>,
) {
    for mut player in &mut players {
        player.play(animations.0[0].clone_weak());
    }
}

pub fn update_movement_indicator(
    mut commands: Commands,
    player_query: Query<(&AnimationPlayer, &MovementIndicator)>,
    minimap_indicator_query: Query<&Handle<StandardMaterial>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    for (player, movement_indicator) in player_query.iter() {
        if player.elapsed() > movement_indicator.duration {
            commands
                .entity(movement_indicator.top_parent)
                .despawn_recursive();
        } else {
            let fraction = (player.elapsed() / movement_indicator.duration).clamp(0.0, 1.0);
            let material_handle = minimap_indicator_query
                .get(movement_indicator.minimap_indicator)
                .unwrap();
            let material = materials.get_mut(material_handle).unwrap();
            material.base_color.set_a(1.0 - fraction);
        }
    }
}
