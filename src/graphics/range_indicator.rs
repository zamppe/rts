use std::f32::consts::PI;

use bevy::pbr::{NotShadowCaster, NotShadowReceiver};
use bevy::prelude::*;

use crate::graphics::materials::RangeIndicatorMaterial;

#[derive(Component)]
pub struct RangeIndicator;

pub fn create_range_indicator(
    commands: &mut Commands,
    radius: f32,
    meshes: &mut ResMut<Assets<Mesh>>,
    material: Handle<RangeIndicatorMaterial>,
) -> Entity {
    let mesh = meshes.add(Mesh::from(shape::Circle::new(radius)));
    commands
        .spawn((
            MaterialMeshBundle {
                mesh,
                material,
                transform: Transform {
                    translation: Vec3::new(0.0, 0.1, 0.0),
                    rotation: Quat::from_rotation_x(PI / -2.0),
                    ..default()
                },
                ..default()
            },
            NotShadowReceiver,
            NotShadowCaster,
            RangeIndicator,
            Name::new("RangeIndicator"),
        ))
        .id()
}
