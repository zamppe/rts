use std::f32::consts::PI;

use bevy::pbr::{NotShadowCaster, NotShadowReceiver};
use bevy::prelude::*;
use bevy::render::view::RenderLayers;

#[derive(Component)]
pub struct SelectionOutline;

pub fn create_selection_outline(
    commands: &mut Commands,
    object_size: Vec2,
    meshes: &mut ResMut<Assets<Mesh>>,
    material: Handle<StandardMaterial>,
) -> Entity {
    let rel_margin = 0.85;
    let abs_margin = 0.01;
    let radius = object_size.length() / 2.0 * rel_margin + abs_margin;
    let mesh = meshes.add(Mesh::from(shape::Circle::new(radius)));

    commands
        .spawn((
            PbrBundle {
                mesh,
                material,
                transform: Transform {
                    translation: Vec3::new(0.0, 0.1, 0.0),
                    rotation: Quat::from_rotation_x(PI / -2.0),
                    ..default()
                },
                visibility: Visibility::Hidden,
                ..default()
            },
            NotShadowReceiver,
            NotShadowCaster,
            SelectionOutline,
            RenderLayers::default().with(1),
            Name::new("SelectionOutline"),
        ))
        .id()
}
