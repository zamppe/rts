use bevy::pbr::{NotShadowCaster, NotShadowReceiver};
use bevy::prelude::*;

use crate::graphics::Models;

#[derive(Component)]
pub struct TurnIndicator;

#[allow(dead_code)]
pub fn create_turn_indicator(
    cmds: &mut Commands,
    object_size: Vec2,
    models: &Res<Models>,
) -> Entity {
    let scale = Vec3::splat(object_size.x * 0.8);

    cmds.spawn((
        PbrBundle {
            mesh: models.turn_indicator.mesh.clone(),
            material: models.turn_indicator.mat.clone(),
            transform: Transform {
                translation: Vec3::new(0.0, object_size.y * 0.8, 0.0),
                scale,
                ..default()
            },
            ..default()
        },
        NotShadowReceiver,
        NotShadowCaster,
        TurnIndicator,
        Name::new("TurnIndicator"),
    ))
    .id()
}
