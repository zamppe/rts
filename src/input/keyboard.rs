use bevy::prelude::*;
use bevy::utils::{EntityHashSet, HashSet};
use bevy_health_bar3d::prelude::WithBar;

use crate::combat::{AttackTarget, AutoattackState, Combat};
use crate::core::movement::Movement;
use crate::core::turn::Turn;
use crate::core::{DespawnEvent, GameState, Health, PlayingState, UserControl};
use crate::graphics::materials::CommonMaterials;
use crate::graphics::range_indicator::{create_range_indicator, RangeIndicator};
use crate::input::mouse::MouseMode;
use crate::ui::popups::CreateTextPopupEvent;

pub fn press_esc(
    keyboard: Res<Input<KeyCode>>,
    mut object_query: Query<&mut UserControl>,
    mouse_mode_state: Res<State<MouseMode>>,
    mut next_state: ResMut<NextState<MouseMode>>,
) {
    // Press Esc to:
    // - Deselect all objects
    // - Exit PlacingBuilding mode
    if keyboard.just_pressed(KeyCode::Escape) {
        for mut user_control in object_query.iter_mut() {
            user_control.selected = false;
        }

        if mouse_mode_state.get() == &MouseMode::PlacingBuilding {
            next_state.set(MouseMode::Normal);
        }
    }
}

pub fn press_delete(
    keyboard: Res<Input<KeyCode>>,
    object_query: Query<(Entity, &UserControl)>,
    mut despawn_event_writer: EventWriter<DespawnEvent>,
) {
    // Press Delete to delete selected objects
    if keyboard.just_pressed(KeyCode::Delete) {
        let entities: HashSet<Entity> = object_query
            .iter()
            .filter_map(|(entity, user_control)| {
                if user_control.selected {
                    Some(entity)
                } else {
                    None
                }
            })
            .collect();
        despawn_event_writer.send(DespawnEvent { entities });
    }
}

pub fn press_s(
    mut commands: Commands,
    keyboard: Res<Input<KeyCode>>,
    unit_query: Query<(Entity, &UserControl)>,
    mut movement_query: Query<&mut Movement>,
    mut turn_query: Query<&mut Turn>,
) {
    // Press S to stop unit/building moving and attacking and turning
    if keyboard.just_pressed(KeyCode::S) {
        for (entity, user_control) in unit_query.iter() {
            if user_control.selected {
                commands.entity(entity).remove::<AttackTarget>();
                if let Ok(mut mov) = movement_query.get_mut(entity) {
                    mov.stop();
                }
                if let Ok(mut turn) = turn_query.get_mut(entity) {
                    turn.stop();
                }
            }
        }
    }
}

pub fn switch_autoattack_state(
    mut aa_state: ResMut<AutoattackState>,
    keyboard: Res<Input<KeyCode>>,
    mut ev_text_popup: EventWriter<CreateTextPopupEvent>,
) {
    if keyboard.just_pressed(KeyCode::F4) {
        aa_state.0 = !aa_state.0;
        let on_off_str = match aa_state.0 {
            true => "ON",
            false => "OFF",
        };
        ev_text_popup.send(CreateTextPopupEvent {
            text: format!("Autoattack is now {}", on_off_str),
            is_warning: true,
            ..default()
        });
    }
}

pub fn press_alt(
    mut commands: Commands,
    keyboard: Res<Input<KeyCode>>,
    combat_query: Query<(Entity, &Combat, &UserControl)>,
    user_control_query: Query<(Entity, &UserControl)>,
    range_indicator_query: Query<Entity, With<RangeIndicator>>,
    bar_query: Query<(Entity, &WithBar<Health>)>,
    mut vis_query: Query<&mut Visibility>,
    mut meshes: ResMut<Assets<Mesh>>,
    common_materials: Res<CommonMaterials>,
) {
    if keyboard.just_pressed(KeyCode::AltLeft) {
        for (entity, cs, user_control) in combat_query.iter() {
            if user_control.selected {
                let attack_range = create_range_indicator(
                    &mut commands,
                    cs.attack_range,
                    &mut meshes,
                    common_materials.attack_range.clone(),
                );
                let aggro_range = create_range_indicator(
                    &mut commands,
                    cs.aggro_range,
                    &mut meshes,
                    common_materials.aggro_range.clone(),
                );
                commands
                    .entity(entity)
                    .push_children(&[attack_range, aggro_range]);
            }
        }

        for (_, bar) in &bar_query {
            let health_bar = bar.get();
            if let Ok(mut bar_visibility) = vis_query.get_mut(health_bar) {
                *bar_visibility = Visibility::Visible;
            }
        }
    }

    if keyboard.just_released(KeyCode::AltLeft) {
        for range_indicator in range_indicator_query.iter() {
            commands.entity(range_indicator).despawn_recursive();
        }

        let selected_entities: EntityHashSet<Entity> = user_control_query
            .iter()
            .filter_map(|(entity, user_control)| {
                if user_control.selected {
                    Some(entity)
                } else {
                    None
                }
            })
            .collect();

        for (entity, bar) in &bar_query {
            if selected_entities.contains(&entity) {
                continue;
            }

            let health_bar = bar.get();
            if let Ok(mut bar_visibility) = vis_query.get_mut(health_bar) {
                *bar_visibility = Visibility::Hidden;
            }
        }
    }
}

pub fn press_f10(keyboard: Res<Input<KeyCode>>, mut next_state: ResMut<NextState<GameState>>) {
    if keyboard.just_pressed(KeyCode::F10) {
        next_state.set(GameState::MainMenu);
    }
}

pub fn press_p(
    keyboard: Res<Input<KeyCode>>,
    current_state: Res<State<PlayingState>>,
    mut next_state: ResMut<NextState<PlayingState>>,
) {
    if keyboard.just_pressed(KeyCode::P) {
        match current_state.get() {
            PlayingState::Playing => next_state.set(PlayingState::Paused),
            PlayingState::Paused => next_state.set(PlayingState::Playing),
        }
    }
}
