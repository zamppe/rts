use bevy::prelude::*;

use keyboard::{
    press_alt, press_delete, press_esc, press_f10, press_p, press_s, switch_autoattack_state,
};
use mouse::{
    check_if_need_start_box_selection, process_lmb_click_in_normal_mouse_mode,
    process_mouse_input_placing_state, process_rmb_click_in_normal_mouse_mode,
    select_with_box_selection, update_mouse_state, MouseMode, MousePanState, MouseState,
};

use crate::combat::autoattack;
use crate::combat::events::handle_death_events;
use crate::core::{GameState, PlayingState};
use crate::input::mouse::send_minimap_click_event;

pub mod keyboard;
pub mod mouse;

pub struct RtsInputPlugin;

impl Plugin for RtsInputPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<MouseState>()
            .add_state::<MouseMode>()
            .init_resource::<MousePanState>()
            .add_systems(OnEnter(GameState::InGame), reset_mouse_states)
            .add_systems(
                Update,
                update_mouse_state.run_if(in_state(GameState::InGame)),
            )
            .add_systems(
                Update,
                (send_minimap_click_event.run_if(resource_changed::<MouseState>()),),
            )
            .add_systems(
                Update,
                (
                    // this might still be fragile :(
                    process_rmb_click_in_normal_mouse_mode.after(autoattack),
                )
                    .run_if(in_state(MouseMode::Normal))
                    .run_if(in_state(GameState::InGame))
                    .run_if(in_state(PlayingState::Playing)),
            )
            .add_systems(
                Update,
                (
                    check_if_need_start_box_selection,
                    process_lmb_click_in_normal_mouse_mode
                        .before(handle_death_events)
                        .before(process_mouse_input_placing_state),
                )
                    .run_if(in_state(MouseMode::Normal))
                    .run_if(in_state(GameState::InGame)),
            )
            .add_systems(
                Update,
                select_with_box_selection
                    .run_if(in_state(MouseMode::BoxSelection))
                    .run_if(in_state(GameState::InGame)),
            )
            .add_systems(
                Update,
                process_mouse_input_placing_state
                    .run_if(in_state(MouseMode::PlacingBuilding))
                    .run_if(in_state(GameState::InGame))
                    .run_if(in_state(PlayingState::Playing)),
            )
            .add_systems(
                // moved these to PostUpdate because they're messing with component insertion
                // (in particular, `press_s`)
                // and that fixed a bug where pressing S did not stop turning
                // because AttackTarget was still present and added a turn target
                // within the same frame
                PostUpdate,
                (
                    press_esc,
                    press_delete,
                    switch_autoattack_state,
                    press_alt,
                    press_s,
                    press_f10,
                    press_p,
                )
                    .run_if(in_state(GameState::InGame)),
            );
    }
}

fn reset_mouse_states(
    mut mouse_state: ResMut<MouseState>,
    mut mouse_pan_state: ResMut<MousePanState>,
    mut next_state: ResMut<NextState<MouseMode>>,
) {
    *mouse_state = MouseState::default();
    *mouse_pan_state = MousePanState::default();
    next_state.set(MouseMode::Normal);
}
