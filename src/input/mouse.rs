use std::f32::consts::FRAC_PI_2;

use bevy::math::Vec3Swizzles;
use bevy::prelude::*;
use bevy::window::CursorGrabMode;
use bevy_mod_raycast::prelude::*;

use crate::audio::ErrorSoundEvent;
use crate::camera::MainCamera;
use crate::combat::{AttackTarget, Combat};
use crate::constants::{MAX_BUILDING_DISTANCE, MOUSE_PAN_MARGIN};
use crate::core::building::{Building, SpawnBuildingEvent};
use crate::core::collision::{collide, obb_collision};
use crate::core::movement::{build_movement_grid, Movement, StartMovementEvent};
use crate::core::{Collider, GameObject, Health, Target, UserControl};
use crate::graphics::box_selection::{BoxSelection, BOX_SELECTION_MIN_DRAG_DISTANCE};
use crate::graphics::building_shadow::BuildingShadow;
use crate::graphics::movement_indicator::CreateMovementIndicatorEvent;
use crate::player::{
    is_same_team, ClientPlayer, PaymentEvent, Player, PlayerAlignment, PlayerTeam,
};
use crate::ui::minimap::{MinimapCamera, MinimapClickEvent, BORDER_WIDTH, MINIMAP_CAMERA_OFFSET};
use crate::ui::popups::CreateTextPopupEvent;
use crate::ui::UI_PANEL_WIDTH;

#[derive(Default, Debug, Hash, PartialEq, Eq, Clone, Copy, States)]
pub enum MouseMode {
    #[default]
    Normal,
    BoxSelection,
    PlacingBuilding,
}

#[derive(Default, Copy, Clone, PartialEq)]
pub enum MousePanHorizontalDirection {
    #[default]
    None,
    Left,
    Right,
}

#[derive(Default, Copy, Clone, PartialEq)]
pub enum MousePanVerticalDirection {
    #[default]
    None,
    Top,
    Bottom,
}

#[derive(Resource, Default, Copy, Clone, PartialEq)]
pub struct MousePanState {
    pub horizontal: MousePanHorizontalDirection,
    pub vertical: MousePanVerticalDirection,
}

#[derive(Resource, Reflect, Copy, Clone, PartialEq)]
#[reflect(Resource)]
pub struct MouseState {
    /// Cursor position in world coordinates, `None` if outside the window
    pub world_pos: Option<Vec2>,
    /// Where cursor points on the minimap, in world coords. `None` if outside the minimap
    pub minimap_world_pos: Option<Vec2>,
    /// Is cursor currently inside the right panel UI area
    pub is_inside_ui: bool,
    /// Is cursor currently inside the minimap area
    pub is_inside_minimap: bool,
    /// Is left mouse button pressed currently
    pub is_lmb_pressed: bool,
    /// Is left mouse button pressed currently inside the minimap area
    pub is_lmb_pressed_inside_minimap: bool,
    /// Cursor position at the moment of clicking LMB
    pub click_start_point: Option<Vec2>,
    /// Distance from `click_start_point` to current position, if LMB is held
    pub drag_distance: f32,
    /// Whether we're in flying camera mode or not
    pub is_flycam_on: bool,
}

impl Default for MouseState {
    fn default() -> Self {
        MouseState {
            world_pos: Some(Vec2::ZERO),
            minimap_world_pos: Some(Vec2::ZERO),
            is_inside_ui: false,
            is_inside_minimap: false,
            is_lmb_pressed: false,
            is_lmb_pressed_inside_minimap: false,
            click_start_point: Some(Vec2::ZERO),
            drag_distance: 0.0,
            is_flycam_on: false,
        }
    }
}

pub fn select_with_box_selection(
    mut object_query: Query<(&Transform, &Collider, &mut UserControl)>,
    box_selection_query: Query<&BoxSelection>,
) {
    if let Ok(box_selection) = box_selection_query.get_single() {
        for (obj_transform, obj_collider, mut obj_uc) in object_query.iter_mut() {
            let (obj_angle, _, _) = obj_transform.rotation.to_euler(EulerRot::YXZ);
            obj_uc.selected = obb_collision(
                box_selection.position.xz(),
                box_selection.size,
                box_selection.rotation,
                obj_transform.translation.xz(),
                obj_collider.hitbox_size.xz(),
                obj_angle,
            );
        }
    }
}

pub fn update_mouse_state(
    mouse: Res<Input<MouseButton>>,
    window_query: Query<&Window>,
    mut mouse_state: ResMut<MouseState>,
    mut mouse_pan_state: ResMut<MousePanState>,
    camera_query: Query<(&Camera, &GlobalTransform), With<MainCamera>>,
    minimap_camera_query: Query<&Projection, With<MinimapCamera>>,
) {
    let window = window_query.single();
    let mut new_state = *mouse_state;
    let mut new_pan_state = *mouse_pan_state;

    let Some(cursor_pos) = window.cursor_position() else {
        // Are these two changes enough?
        new_state.world_pos = None;
        new_state.is_lmb_pressed = false;
        *mouse_pan_state = MousePanState::default();

        // Avoid change detection
        if new_state != *mouse_state {
            *mouse_state = new_state;
        }

        if new_pan_state != *mouse_pan_state {
            *mouse_pan_state = new_pan_state;
        }

        return;
    };

    let (camera, camera_transform) = camera_query.single();
    let world_pos = camera
        .viewport_to_world(camera_transform, cursor_pos)
        .and_then(|ray| {
            ray.intersect_plane(Vec3::ZERO, Vec3::Y)
                .map(|distance| ray.get_point(distance).xz())
        });
    let minimap_width = window.height() * 0.3;

    // Update MouseState
    new_state.world_pos = world_pos;
    new_state.is_flycam_on = window.cursor.grab_mode == CursorGrabMode::Confined;
    new_state.is_inside_ui = cursor_pos.x >= (window.width() - UI_PANEL_WIDTH);

    new_state.is_inside_minimap = cursor_pos.x > BORDER_WIDTH
        && cursor_pos.x < BORDER_WIDTH + minimap_width
        && cursor_pos.y > window.height() - BORDER_WIDTH - minimap_width
        && cursor_pos.y < window.height() - BORDER_WIDTH;

    if new_state.is_inside_minimap {
        let minimap_projection = minimap_camera_query.single();
        if let Projection::Orthographic(ref proj) = &minimap_projection {
            let multi = proj.scale * FRAC_PI_2;
            let center_x = BORDER_WIDTH + minimap_width / 2.0;
            let center_y = window.height() - BORDER_WIDTH - minimap_width / 2.0;
            let result_pos = Vec2::new(cursor_pos.x - center_x, cursor_pos.y - center_y) * multi;

            new_state.minimap_world_pos = Some(result_pos + MINIMAP_CAMERA_OFFSET);
        }
    } else {
        new_state.minimap_world_pos = None;
    }

    new_state.is_lmb_pressed = mouse.pressed(MouseButton::Left);

    if !mouse_state.is_lmb_pressed
        && new_state.is_lmb_pressed
        && !new_state.is_inside_minimap
        && !new_state.is_inside_ui
    {
        new_state.click_start_point = new_state.world_pos;
    }

    if !new_state.is_lmb_pressed {
        new_state.click_start_point = None;
    }

    if let Some(start_pos) = new_state.click_start_point {
        if let Some(world_pos) = new_state.world_pos {
            new_state.drag_distance = start_pos.distance(world_pos);
        }
    } else {
        new_state.drag_distance = 0.0;
    }

    new_state.is_lmb_pressed_inside_minimap = mouse.pressed(MouseButton::Left)
        && new_state.is_inside_minimap
        && new_state.click_start_point.is_none();

    // Update MousePanState
    let x_percent = cursor_pos.x / window.width();
    let y_percent = cursor_pos.y / window.height();
    if x_percent < MOUSE_PAN_MARGIN {
        new_pan_state.horizontal = MousePanHorizontalDirection::Left;
    } else if x_percent > (1.0 - MOUSE_PAN_MARGIN) {
        new_pan_state.horizontal = MousePanHorizontalDirection::Right;
    } else {
        new_pan_state.horizontal = MousePanHorizontalDirection::None;
    }
    if y_percent < MOUSE_PAN_MARGIN {
        new_pan_state.vertical = MousePanVerticalDirection::Top;
    } else if y_percent > (1.0 - MOUSE_PAN_MARGIN) {
        new_pan_state.vertical = MousePanVerticalDirection::Bottom;
    } else {
        new_pan_state.vertical = MousePanVerticalDirection::None;
    }

    // Avoid change detection
    if new_state != *mouse_state {
        *mouse_state = new_state;
    }

    if new_pan_state != *mouse_pan_state {
        *mouse_pan_state = new_pan_state;
    }
}

pub fn process_mouse_input_placing_state(
    mouse: Res<Input<MouseButton>>,
    keyboard: Res<Input<KeyCode>>,
    mouse_state: Res<MouseState>,
    mut mouse_mode_state: ResMut<NextState<MouseMode>>,
    mut ev_spawn_building: EventWriter<SpawnBuildingEvent>,
    mut ev_payment: EventWriter<PaymentEvent>,
    mut ev_text_popup: EventWriter<CreateTextPopupEvent>,
    mut error_sound_event_writer: EventWriter<ErrorSoundEvent>,
    building_shadow_query: Query<(&Transform, &BuildingShadow)>,
    mut building_placement_collision_query: Query<
        (&PlayerAlignment, &Transform, &Collider),
        With<Building>,
    >,
    client_player_query: Query<(Entity, &Player), With<ClientPlayer>>,
) {
    if mouse.any_just_pressed([MouseButton::Left, MouseButton::Right]) {
        // Ignore clicks inside UI area
        if mouse_state.is_inside_ui || mouse_state.is_inside_minimap {
            return;
        }

        // Exit PlacingBuilding mode by right-clicking
        if mouse.just_pressed(MouseButton::Right) {
            mouse_mode_state.set(MouseMode::Normal);
            return;
        }

        let world_pos = match mouse_state.world_pos {
            Some(pos) => pos,
            None => return,
        };

        let (player_entity, client_player) = client_player_query.single();
        let (shadow_transform, shadow) = building_shadow_query.single();

        // Validation - check that there are enough resources to construct the building
        if client_player.resource.amount < shadow.building_type.cost() {
            ev_text_popup.send(CreateTextPopupEvent {
                text: "Not enough resources".to_string(),
                is_warning: true,
                ..default()
            });
            error_sound_event_writer.send(ErrorSoundEvent);
            return;
        }

        let any_collision = building_placement_collision_query.iter_mut().any(
            |(_player_alignment, building_transform, collider)| {
                collide(
                    building_transform.translation,
                    collider.hitbox_size.xz(),
                    shadow_transform.translation,
                    shadow.building_type.size().xz(),
                )
            },
        );

        // Validation - check that the building to be placed is not overlapping anything
        if any_collision {
            ev_text_popup.send(CreateTextPopupEvent {
                text: "Area is already occupied".to_string(),
                is_warning: true,
                ..default()
            });
            error_sound_event_writer.send(ErrorSoundEvent);
            return;
        }
        let close_enough_to_some_building = building_placement_collision_query.iter_mut().any(
            |(player_alignment, building_transform, collider)| {
                client_player.player_alignment == *player_alignment
                    && collide(
                        building_transform.translation,
                        collider.hitbox_size.xz() + MAX_BUILDING_DISTANCE,
                        shadow_transform.translation,
                        shadow.building_type.size().xz(),
                    )
            },
        );

        // Validation - check that the building to be placed is near enough friendly buildings
        if !close_enough_to_some_building {
            ev_text_popup.send(CreateTextPopupEvent {
                text: "Too far away from friendly buildings".to_string(),
                is_warning: true,
                ..default()
            });
            error_sound_event_writer.send(ErrorSoundEvent);
            return;
        }

        // All validation have passed
        ev_spawn_building.send(SpawnBuildingEvent {
            position: world_pos.extend(0.0).xzy(),
            player_team: client_player.player_team,
            player_alignment: client_player.player_alignment,
            building_type: shadow.building_type,
        });

        ev_payment.send(PaymentEvent {
            player_entity,
            cost: shadow.building_type.cost(),
        });

        if !keyboard.any_pressed([KeyCode::ShiftLeft, KeyCode::ShiftRight]) {
            mouse_mode_state.set(MouseMode::Normal);
        }
    }
}

pub fn process_lmb_click_in_normal_mouse_mode(
    mouse: Res<Input<MouseButton>>,
    mouse_state: Res<MouseState>,
    camera_query: Query<&Transform, With<MainCamera>>,
    target_query: Query<&GameObject>,
    mut user_control_query: Query<&mut UserControl>,
    mut raycast: Raycast,
) {
    if mouse.just_pressed(MouseButton::Left)
        && !mouse_state.is_inside_ui
        && !mouse_state.is_inside_minimap
    {
        if let Some(world_pos) = mouse_state.world_pos {
            clear_all_selection(&mut user_control_query);

            let camera_transform = camera_query.single();
            if let Some(entity) =
                pick_object_with_ray(world_pos, camera_transform, &mut raycast, &target_query)
            {
                if let Ok(mut obj_user_control) = user_control_query.get_mut(entity) {
                    obj_user_control.selected = true;
                }
            }
        }
    }
}

fn pick_object_with_ray(
    world_pos: Vec2,
    camera_transform: &Transform,
    raycast: &mut Raycast,
    target_query: &Query<&GameObject>,
) -> Option<Entity> {
    let click_pos = world_pos.extend(0.0).xzy();
    let dir = (click_pos - camera_transform.translation).normalize();

    let filter = |entity| target_query.contains(entity);
    let settings = RaycastSettings::default().with_filter(&filter);
    let result = raycast.cast_ray(Ray3d::new(camera_transform.translation, dir), &settings);
    if !result.is_empty() {
        return Some(result[0].0);
    }

    None
}

// Need to put this somewhere else probably
fn clear_all_selection(user_control_query: &mut Query<&mut UserControl>) {
    for mut user_control in user_control_query.iter_mut() {
        user_control.selected = false;
    }
}

pub fn process_rmb_click_in_normal_mouse_mode(
    mut commands: Commands,
    mouse: Res<Input<MouseButton>>,
    mouse_state: Res<MouseState>,
    keyboard_state: Res<Input<KeyCode>>,
    mut start_moving_event_writer: EventWriter<StartMovementEvent>,
    mut error_sound_event_writer: EventWriter<ErrorSoundEvent>,
    mut movement_indicator_event: EventWriter<CreateMovementIndicatorEvent>,
    mut user_control_query: Query<(
        Entity,
        &UserControl,
        &Transform,
        &Collider,
        Option<&Combat>,
        Option<&mut Movement>,
    )>,
    health_query: Query<&Health>,
    team_query: Query<&PlayerTeam>,
    camera_query: Query<&Transform, With<MainCamera>>,
    target_query: Query<&GameObject>,
    mut raycast: Raycast,
) {
    if mouse.just_pressed(MouseButton::Right) && !mouse_state.is_inside_ui {
        let world_pos = match mouse_state.world_pos {
            Some(pos) => pos,
            None => return,
        };

        let camera_transform = camera_query.single();
        let optional_target_entity =
            pick_object_with_ray(world_pos, camera_transform, &mut raycast, &target_query);
        let mut good_order = false;
        // If there is a target, and all the attack conditions are met, we will attack.
        // Attack conditions:
        // - our selected entity has Combat (= is able to deal damage)
        // - the target is not our selected entity
        // - the target has Health (= is able to receive damage)
        // - the target is not of the same Faction
        // If some of the attack conditions are not met, we do nothing.
        if let Some(target_entity) = optional_target_entity {
            for (obj_entity, obj_user_control, _, _, combat, movement) in
                user_control_query.iter_mut()
            {
                // Only use entities that are selected and not ourselves
                if !obj_user_control.selected || obj_entity == target_entity {
                    continue;
                }

                let is_valid_target_team = keyboard_state.pressed(KeyCode::ControlLeft)
                    || !is_same_team(obj_entity, target_entity, &team_query);

                // Follow
                if !is_valid_target_team && movement.is_some() {
                    start_moving_event_writer.send(StartMovementEvent {
                        unit_entity: obj_entity,
                        target: Target::Dynamic(target_entity),
                    });
                    good_order = true;
                    continue;
                }

                // Attack
                // Force attack anything if holding LControl
                if let Some(combat) = combat {
                    if health_query.get(target_entity).is_ok() && is_valid_target_team {
                        if let Some(mut mov) = movement {
                            mov.stop();
                        }

                        commands.entity(obj_entity).insert(AttackTarget::new(
                            target_entity,
                            combat.out_of_range_timer_duration,
                        ));
                        good_order = true;
                    }
                }
            }
        // If there was nothing attackable where we clicked, then we move units if any selected
        } else {
            let units: Vec<(Entity, &Transform, &Collider)> = user_control_query
                .iter()
                .filter_map(|(ent, user_control, transform, collider, _, movement)| {
                    if user_control.selected && movement.is_some() {
                        Some((ent, transform, collider))
                    } else {
                        None
                    }
                })
                .collect();

            let movement_grid = if mouse_state.is_inside_minimap {
                let position = mouse_state.minimap_world_pos.unwrap();
                build_movement_grid(position, &units)
            } else {
                build_movement_grid(world_pos, &units)
            };
            for (obj_entity, target, size) in movement_grid.iter() {
                commands.entity(*obj_entity).remove::<AttackTarget>();
                start_moving_event_writer.send(StartMovementEvent {
                    unit_entity: *obj_entity,
                    target: Target::Static(*target),
                });
                // maybe get rid of Collider altogether since there is Aabb component?
                movement_indicator_event.send(CreateMovementIndicatorEvent {
                    position: target.xz(),
                    size: *size,
                });
                good_order = true;
            }
        }

        if !good_order {
            error_sound_event_writer.send(ErrorSoundEvent);
        }
    }
}

pub fn check_if_need_start_box_selection(
    mouse_state: Res<MouseState>,
    mut mouse_mode_state: ResMut<NextState<MouseMode>>,
) {
    if mouse_state.drag_distance > BOX_SELECTION_MIN_DRAG_DISTANCE {
        mouse_mode_state.set(MouseMode::BoxSelection);
    }
}

pub fn send_minimap_click_event(
    mouse_state: Res<MouseState>,
    mut minimap_click_event: EventWriter<MinimapClickEvent>,
) {
    if mouse_state.is_lmb_pressed_inside_minimap {
        let world_coords = mouse_state.minimap_world_pos.unwrap();
        minimap_click_event.send(MinimapClickEvent::new(world_coords));
    }
}
