use std::f32::consts::PI;

use bevy::pbr::CascadeShadowConfigBuilder;
use bevy::pbr::{NotShadowCaster, NotShadowReceiver};
use bevy::prelude::*;
use bevy::render::view::RenderLayers;
use bevy::time::Stopwatch;
use bevy_flycam::FlyCam;
use bevy_health_bar3d::prelude::*;
use bevy_pipelines_ready::{PipelinesReady, PipelinesReadyPlugin};
use bevy_scene_hook::{HookedSceneBundle, SceneHook};

use crate::camera::get_default_camera_transform;
use crate::camera::MainCamera;
use crate::cleanup::CleanupBundle;
use crate::core::building::Construction;
use crate::core::{GameState, Health};
use crate::graphics::materials::{
    CommonMaterials, LaserBeamMaterial, OutlineMaterial, RangeIndicatorMaterial,
};
use crate::graphics::{ModelMaterials, Models};

pub struct LoadingPlugin;

impl Plugin for LoadingPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins(PipelinesReadyPlugin)
            .add_systems(
                OnEnter(GameState::Loading),
                (
                    start_loading_timers,
                    spawn_camera,
                    spawn_loading_screen,
                    spawn_light,
                    spawn_lamps,
                    spawn_dummy_entities_with_common_materials,
                    spawn_dummy_entities_with_gltf_materials,
                    spawn_movement_indicator,
                    spawn_health_bar,
                    spawn_constructing_building,
                    spawn_rocket_dude,
                    spawn_f,
                    spawn_2d_camera,
                    spawn_minimap_camera,
                    spawn_selection_outline,
                )
                    .chain(),
            )
            .add_systems(
                Update,
                (transition, transition_on_timer).run_if(in_state(GameState::Loading)),
            )
            .add_systems(
                Update,
                track_pipelines.run_if(resource_changed::<PipelinesReady>()),
            );
    }
}

const EXPECTED_PIPELINES: usize = 31;

#[derive(Component)]
pub struct Sun;

#[derive(Component)]
struct LoadingProgressBar;

#[derive(Resource, Deref, DerefMut)]
struct LoadingTimer(Timer);

#[derive(Resource, Deref, DerefMut)]
struct LoadingStopwatch(Stopwatch);

fn start_loading_timers(mut commands: Commands) {
    commands.insert_resource(LoadingTimer(Timer::from_seconds(1.5, TimerMode::Once)));
    commands.insert_resource(LoadingStopwatch(Stopwatch::new()));
}

fn spawn_loading_screen(mut commands: Commands) {
    let progress_bar = commands
        .spawn((
            ImageBundle {
                style: Style {
                    position_type: PositionType::Absolute,
                    bottom: Val::Percent(0.0),
                    width: Val::Percent(0.0),
                    height: Val::Percent(3.0),
                    ..default()
                },
                background_color: Color::GREEN.into(),
                ..default()
            },
            LoadingProgressBar,
            CleanupBundle::loading("Loading progress bar"),
        ))
        .id();

    commands
        .spawn((
            NodeBundle {
                style: Style {
                    position_type: PositionType::Absolute,
                    width: Val::Percent(100.0),
                    height: Val::Percent(100.0),
                    margin: UiRect::all(Val::Auto),
                    ..default()
                },
                background_color: Color::rgba(0.0, 0.0, 0.05, 0.97).into(),
                ..default()
            },
            CleanupBundle::loading("Loading screen container"),
        ))
        .add_child(progress_bar);
}

fn spawn_camera(mut commands: Commands) {
    commands.spawn((
        Camera3dBundle {
            transform: get_default_camera_transform(),
            ..default()
        },
        FogSettings {
            color: Color::rgba_linear(0.5, 0.5, 0.5, 1.0),
            directional_light_color: Color::rgba(1.0, 0.95, 0.75, 1.0),
            directional_light_exponent: 100.0,
            falloff: FogFalloff::Linear {
                start: 260.0,
                end: 700.0,
            },
        },
        FlyCam,
        MainCamera,
        CleanupBundle::in_game("Camera"),
    ));
}

fn spawn_light(mut commands: Commands) {
    // ambient light
    commands.insert_resource(AmbientLight {
        color: Color::WHITE,
        brightness: 0.04,
    });

    // directional light
    commands.spawn((
        DirectionalLightBundle {
            cascade_shadow_config: CascadeShadowConfigBuilder {
                minimum_distance: 50.0,
                first_cascade_far_bound: 100.0,
                maximum_distance: 700.0,
                ..default()
            }
            .into(),
            directional_light: DirectionalLight {
                color: Color::rgb(0.98, 0.95, 0.82),
                illuminance: 8000.0,
                shadows_enabled: true,
                ..default()
            },
            transform: Transform::from_rotation(Quat::from_euler(
                EulerRot::ZYX,
                0.0,
                1.0,
                -PI / 4.,
            )),
            ..default()
        },
        Sun,
        CleanupBundle::in_game("Sun"),
    ));
}

fn spawn_lamps(mut commands: Commands, models: Res<Models>) {
    let x_z_rot = [
        (35.0, 35.0, 0.25),
        (-35.0, -35.0, -0.75),
        (35.0, -35.0, 0.75),
        (-35.0, 35.0, -0.25),
    ];

    for (x, z, rot) in x_z_rot.iter() {
        let light = commands
            .spawn(SpotLightBundle {
                spot_light: SpotLight {
                    color: Color::rgb(0.98, 0.95, 0.82),
                    range: 40.0,
                    intensity: 10000.0,
                    shadows_enabled: false,
                    ..default()
                },
                transform: Transform::from_xyz(0.0, 12.2, -4.0582)
                    .looking_at(Vec3::new(0.0, 0.0, -12.0), Vec3::Y),
                ..default()
            })
            .id();

        commands
            .spawn((
                PbrBundle {
                    mesh: models.lamp.mesh.clone(),
                    material: models.lamp.mat.clone(),
                    transform: Transform {
                        translation: Vec3::new(*x, 0.0, *z),
                        rotation: Quat::from_rotation_y(PI * *rot),
                        scale: Vec3::splat(1.5),
                    },
                    ..default()
                },
                CleanupBundle::in_game("Lamp"),
            ))
            .add_child(light);
    }
}

fn transition(
    ready: Res<PipelinesReady>,
    mut next_state: ResMut<NextState<GameState>>,
    loading_stopwatch: Res<LoadingStopwatch>,
    mut progress_bar_query: Query<&mut Style, With<LoadingProgressBar>>,
) {
    if let Ok(mut progress_bar) = progress_bar_query.get_single_mut() {
        progress_bar.width = Val::Percent(ready.get() as f32 / EXPECTED_PIPELINES as f32 * 100.0);
    }

    if ready.get() >= EXPECTED_PIPELINES {
        info!("Loading took {}s", loading_stopwatch.elapsed_secs());
        next_state.set(GameState::InGame);
    }
}

fn transition_on_timer(
    mut next_state: ResMut<NextState<GameState>>,
    mut loading_timer: ResMut<LoadingTimer>,
    mut loading_stopwatch: ResMut<LoadingStopwatch>,
    time: Res<Time>,
) {
    loading_stopwatch.tick(time.delta());
    if loading_timer.tick(time.delta()).just_finished() {
        info!("Timed out waiting for pipelines to load!");
        next_state.set(GameState::InGame);
    }
}

fn spawn_dummy_entities_with_common_materials(
    mut cmds: Commands,
    common_materials: Res<CommonMaterials>,
    mut meshes: ResMut<Assets<Mesh>>,
) {
    for field in common_materials.iter_fields() {
        if let Some(material) = field.downcast_ref::<Handle<StandardMaterial>>() {
            spawn_entity_with_material(&mut cmds, &mut meshes, material.clone());
        }

        if let Some(material) = field.downcast_ref::<Handle<OutlineMaterial>>() {
            spawn_entity_with_material(&mut cmds, &mut meshes, material.clone());
        }

        if let Some(material) = field.downcast_ref::<Handle<RangeIndicatorMaterial>>() {
            spawn_entity_with_material(&mut cmds, &mut meshes, material.clone());
        }

        if let Some(material) = field.downcast_ref::<Handle<LaserBeamMaterial>>() {
            spawn_entity_with_material(&mut cmds, &mut meshes, material.clone());
        }
    }
}

fn spawn_entity_with_material<M: Material>(
    cmds: &mut Commands,
    meshes: &mut ResMut<Assets<Mesh>>,
    material: Handle<M>,
) {
    cmds.spawn((
        MaterialMeshBundle {
            mesh: meshes.add(Mesh::from(shape::Plane::default())),
            material,
            ..default()
        },
        CleanupBundle::loading("Dummy entity with material"),
    ));
}

fn spawn_dummy_entities_with_gltf_materials(
    mut cmds: Commands,
    model_materials: Res<ModelMaterials>,
    mut meshes: ResMut<Assets<Mesh>>,
) {
    for field in model_materials.iter_fields() {
        if let Some(material) = field.downcast_ref::<Handle<StandardMaterial>>() {
            spawn_entity_with_material(&mut cmds, &mut meshes, material.clone());
        }
    }
}

fn spawn_movement_indicator(
    mut cmds: Commands,
    models: Res<Models>,
    common_materials: Res<CommonMaterials>,
) {
    let material = common_materials.movement_indicator.clone();
    let mut ecmds = cmds.spawn_empty();
    ecmds.insert((
        HookedSceneBundle {
            scene: SceneBundle {
                scene: models.movement_indicator.clone(),
                ..default()
            },
            hook: SceneHook::new(move |entity, ecmds| {
                match entity.get::<Name>().map(|t| t.as_str()) {
                    Some("Arrow mesh") => ecmds
                        .remove::<Handle<StandardMaterial>>()
                        .insert(material.clone()),
                    _ => ecmds,
                };
            }),
        },
        CleanupBundle::loading("Dummy movement indicator"),
    ));
}

fn spawn_health_bar(mut cmds: Commands) {
    cmds.spawn((
        SpatialBundle::default(),
        BarBundle::<Health> { ..default() },
        Health::default(),
        CleanupBundle::loading("Dummy health bar"),
    ));

    cmds.spawn((
        SpatialBundle::default(),
        BarBundle::<Construction> {
            border: BarBorder::new(0.15),
            ..default()
        },
        Construction::new(0.1),
        CleanupBundle::loading("Dummy health bar with border"),
    ));
}

fn track_pipelines(ready: Res<PipelinesReady>) {
    info!("Pipelines Ready: {}/{}", ready.get(), EXPECTED_PIPELINES);
}

fn spawn_constructing_building(
    mut cmds: Commands,
    mut materials: ResMut<Assets<StandardMaterial>>,
    models: Res<Models>,
) {
    cmds.spawn((
        PbrBundle {
            mesh: models.gatherer.clone(),
            material: materials.add(StandardMaterial {
                base_color: Color::rgba(0.5, 1.0, 0.5, 0.5),
                alpha_mode: AlphaMode::Blend,
                ..default()
            }),
            ..default()
        },
        RenderLayers::default().with(1),
        NotShadowCaster,
        NotShadowReceiver,
        CleanupBundle::loading("Constructing building"),
    ));
}

fn spawn_rocket_dude(
    mut commands: Commands,
    common_materials: Res<CommonMaterials>,
    models: Res<Models>,
) {
    commands.spawn((
        PbrBundle {
            mesh: models.rocket_dude.clone(),
            material: common_materials.red.clone(),
            ..default()
        },
        Health::default(),
        BarBundle::<Health> {
            offset: BarOffset::new(1.0),
            width: BarWidth::new(1.2),
            height: BarHeight::Static(0.4),
            border: BarBorder::new(0.1),
            visibility: BarVisibility::Hidden,
            ..default()
        },
        RenderLayers::default().with(1),
        CleanupBundle::loading("Dummy rocket dude"),
    ));
}

fn spawn_f(mut commands: Commands) {
    commands.spawn((
        Text2dBundle {
            text: Text::from_section(
                "F",
                TextStyle {
                    font_size: 100.0,
                    color: Color::BLACK,
                    ..default()
                },
            )
            .with_alignment(TextAlignment::Center),
            ..default()
        },
        CleanupBundle::loading("F"),
    ));
}

fn spawn_2d_camera(mut commands: Commands) {
    commands.spawn((
        Camera2dBundle {
            camera: Camera {
                order: 1,
                ..default()
            },
            ..default()
        },
        CleanupBundle::loading("2d camera"),
    ));
}

fn spawn_minimap_camera(mut commands: Commands) {
    commands.spawn((
        Camera3dBundle {
            camera: Camera {
                order: -1,
                ..default()
            },
            projection: Projection::Orthographic(Default::default()),
            transform: Transform::from_translation(Vec3::new(200.0, 650.0, 100.0))
                .with_rotation(Quat::from_rotation_x(-PI / 2.0)),
            ..default()
        },
        UiCameraConfig { show_ui: false },
        RenderLayers::layer(1),
        CleanupBundle::loading("Minimap camera"),
    ));
}

fn spawn_selection_outline(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    common_materials: Res<CommonMaterials>,
) {
    commands.spawn((
        PbrBundle {
            mesh: meshes.add(Mesh::from(shape::Circle::default())),
            material: common_materials.selection.clone(),
            ..default()
        },
        NotShadowReceiver,
        NotShadowCaster,
        RenderLayers::default().with(1),
        CleanupBundle::loading("Dummy selection outline"),
    ));
}

#[allow(dead_code)]
fn spawn_ground(mut commands: Commands, models: Res<Models>) {
    commands.spawn((
        PbrBundle {
            mesh: models.ground.mesh.clone(),
            material: models.ground.mat.clone(),
            ..default()
        },
        CleanupBundle::loading("Dummy ground"),
    ));
}
