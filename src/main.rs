#![allow(clippy::too_many_arguments)]
#![allow(clippy::type_complexity)]

use bevy::log::LogPlugin;
use bevy::prelude::*;
use bevy::window::{Cursor, CursorGrabMode, WindowMode};
use bevy_scene_hook::HookPlugin;

use crate::debug::DebugPlugin;
use crate::game::RtsGamePlugin;

mod ai;
mod audio;
mod camera;
mod cleanup;
mod combat;
mod constants;
mod core;
mod debug;
mod game;
mod game_over;
mod graphics;
mod input;
mod loading;
mod map;
mod menu;
mod player;
mod ui;

fn main() {
    App::new()
        .insert_resource(ClearColor(Color::hex("#87C1FF").unwrap()))
        .add_plugins(
            DefaultPlugins
                .set(WindowPlugin {
                    primary_window: Some(Window {
                        cursor: Cursor {
                            grab_mode: CursorGrabMode::Confined,
                            ..default()
                        },
                        mode: WindowMode::BorderlessFullscreen,
                        title: "RTS".to_string(),
                        resizable: false,
                        ..default()
                    }),
                    ..default()
                })
                .set(LogPlugin {
                    filter: "wgpu=error,symphonia=warn,naga=warn,bevy_inspector_egui=error"
                        .to_string(),
                    ..default()
                }),
        )
        .add_plugins((RtsGamePlugin, DebugPlugin, HookPlugin))
        .run();
}
