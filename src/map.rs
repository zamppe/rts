use bevy::pbr::NotShadowCaster;
use bevy::pbr::{DirectionalLightShadowMap, NotShadowReceiver};
use bevy::prelude::*;
use bevy::render::view::RenderLayers;

use crate::camera::MainCamera;
use crate::cleanup::CleanupBundle;
use crate::core::GameState;
use crate::graphics::materials::CommonMaterials;
use crate::graphics::Models;
use crate::loading::Sun;
use crate::ui::PausedText;

pub struct MapPlugin;

impl Plugin for MapPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(DirectionalLightShadowMap { size: 4096 })
            .init_resource::<TimeOfDay>()
            .add_systems(
                OnEnter(GameState::InGame),
                (spawn_ground, spawn_sky, reset_time_of_day),
            )
            .add_systems(
                Update,
                change_time_of_day
                    .run_if(resource_changed::<TimeOfDay>())
                    .run_if(in_state(GameState::InGame)),
            );
    }
}

#[derive(Resource, Default)]
pub enum TimeOfDay {
    #[default]
    Day,
    Night,
}

fn spawn_ground(mut commands: Commands, models: Res<Models>) {
    commands.spawn((
        PbrBundle {
            mesh: models.ground.mesh.clone(),
            material: models.ground.mat.clone(),
            ..default()
        },
        CleanupBundle::in_game("Ground"),
    ));

    commands.spawn((
        PbrBundle {
            mesh: models.ground.mesh.clone(),
            material: models.ground.mat.clone(),
            ..default()
        },
        RenderLayers::layer(1),
        NotShadowReceiver,
        NotShadowCaster,
        CleanupBundle::in_game("Minimap ground"),
    ));
}

fn spawn_sky(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    common_materials: Res<CommonMaterials>,
) {
    // Sky
    commands.spawn((
        PbrBundle {
            mesh: meshes.add(Mesh::from(shape::Box::new(10.0, 10.0, 10.0))),
            material: common_materials.skybox.clone(),
            transform: Transform::from_scale(Vec3::splat(500.0)),
            ..default()
        },
        NotShadowCaster,
        CleanupBundle::in_game("Skybox"),
    ));
}

fn reset_time_of_day(mut time_of_day: ResMut<TimeOfDay>) {
    *time_of_day = TimeOfDay::Day;
}

fn change_time_of_day(
    time_of_day: Res<TimeOfDay>,
    mut sun_query: Query<&mut DirectionalLight, With<Sun>>,
    mut clear: ResMut<ClearColor>,
    mut fog_query: Query<&mut FogSettings, With<MainCamera>>,
    mut paused_text_query: Query<&mut Text, With<PausedText>>,
) {
    let illuminance = match *time_of_day {
        TimeOfDay::Day => 8000.0,
        TimeOfDay::Night => 100.0,
    };

    match *time_of_day {
        TimeOfDay::Day => {
            clear.0 = Color::hex("#87C1FF").unwrap();
        }
        TimeOfDay::Night => {
            clear.0 = Color::DARK_GRAY;
        }
    }

    if let Ok(mut sun) = sun_query.get_single_mut() {
        sun.illuminance = illuminance;
    }

    let mut fog = fog_query.single_mut();
    if let FogFalloff::Linear {
        ref mut start,
        ref mut end,
    } = &mut fog.falloff
    {
        match *time_of_day {
            TimeOfDay::Day => {
                *start = 260.0;
                *end = 700.0;
            }
            TimeOfDay::Night => {
                *start = 220.0;
                *end = 500.0;
            }
        };
    }

    fog.color = match *time_of_day {
        TimeOfDay::Day => Color::rgba_linear(0.5, 0.5, 0.5, 1.0),
        TimeOfDay::Night => Color::rgba_linear(0.05, 0.05, 0.05, 1.0),
    };

    if let Ok(mut paused_text) = paused_text_query.get_single_mut() {
        paused_text.sections[0].style.color = match *time_of_day {
            TimeOfDay::Day => Color::BLACK,
            TimeOfDay::Night => Color::rgb(0.8, 0.8, 0.8),
        }
    }
}
