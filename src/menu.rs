use bevy::app::AppExit;
use bevy::prelude::*;

use crate::cleanup::CleanupBundle;
use crate::constants::{BUTTON_COLOR, BUTTON_COLOR_HOVERED};
use crate::core::GameState;
use crate::ui::UiAssets;

pub struct MainMenuPlugin;

#[derive(Component)]
struct PlayButton;

#[derive(Component)]
struct QuitButton;

#[derive(Component)]
struct ButtonEnabled;

const GAME_TITLE: &str = "Nameless RTS";

impl Plugin for MainMenuPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            OnEnter(GameState::MainMenu),
            (spawn_menu, spawn_menu_camera, spawn_title, spawn_background),
        )
        .add_systems(
            Update,
            (handle_play_button,).run_if(in_state(GameState::MainMenu)),
        );
    }
}

fn spawn_menu_button(commands: &mut Commands, ui_assets: &UiAssets, text: &'static str) -> Entity {
    commands
        .spawn((
            ButtonBundle {
                style: Style {
                    width: Val::Px(400.0),
                    height: Val::Px(90.0),
                    justify_content: JustifyContent::Center,
                    align_items: AlignItems::Center,
                    ..default()
                },
                background_color: BUTTON_COLOR.into(),
                ..default()
            },
            Name::new("Menu button"),
        ))
        .with_children(|parent| {
            parent.spawn((
                TextBundle {
                    text: Text::from_section(
                        text,
                        TextStyle {
                            font: ui_assets.font.clone(),
                            font_size: 64.0,
                            color: Color::WHITE,
                        },
                    ),
                    ..default()
                },
                Name::new("Menu button text"),
            ));
        })
        .id()
}

fn spawn_menu(mut commands: Commands, ui_assets: Res<UiAssets>) {
    let play_button = spawn_menu_button(&mut commands, &ui_assets, "Play");
    commands
        .entity(play_button)
        .insert((PlayButton, ButtonEnabled));

    let settings_button = spawn_menu_button(&mut commands, &ui_assets, "Settings");

    let quit_button = spawn_menu_button(&mut commands, &ui_assets, "Quit");
    commands
        .entity(quit_button)
        .insert((QuitButton, ButtonEnabled));

    commands
        .spawn((
            NodeBundle {
                style: Style {
                    position_type: PositionType::Absolute,
                    margin: UiRect::all(Val::Auto),
                    align_items: AlignItems::Center,
                    flex_direction: FlexDirection::Column,
                    row_gap: Val::Px(15.0),
                    ..default()
                },
                ..default()
            },
            CleanupBundle::main_menu("Menu container"),
        ))
        .add_child(play_button)
        .add_child(settings_button)
        .add_child(quit_button);
}

fn spawn_menu_camera(mut commands: Commands) {
    commands.spawn((
        Camera2dBundle::default(),
        CleanupBundle::main_menu("Menu camera"),
    ));
}

fn spawn_title(mut commands: Commands, ui_assets: Res<UiAssets>) {
    let text = commands
        .spawn((
            TextBundle {
                text: Text::from_section(
                    GAME_TITLE,
                    TextStyle {
                        font: ui_assets.font.clone(),
                        font_size: 80.0,
                        color: Color::BLACK,
                    },
                ),
                ..default()
            },
            Name::new("Title text"),
        ))
        .id();

    commands
        .spawn((
            NodeBundle {
                style: Style {
                    position_type: PositionType::Absolute,
                    margin: UiRect::new(Val::Auto, Val::Auto, Val::Vh(5.0), Val::Auto),
                    ..default()
                },
                ..default()
            },
            CleanupBundle::main_menu("Menu game title"),
        ))
        .add_child(text);
}

fn spawn_background(mut commands: Commands) {
    commands.spawn((
        SpriteBundle {
            sprite: Sprite {
                color: Color::hex("#87C1FF").unwrap(),
                custom_size: Some(Vec2::new(10_000.0, 10_000.0)),
                ..default()
            },
            transform: Transform::from_xyz(0.0, 0.0, -1.0),
            ..default()
        },
        CleanupBundle::main_menu("Menu background"),
    ));
}

fn handle_play_button(
    mut interaction_query: Query<
        (
            &Interaction,
            &mut BackgroundColor,
            Has<PlayButton>,
            Has<QuitButton>,
        ),
        (Changed<Interaction>, With<ButtonEnabled>),
    >,
    mut game_state: ResMut<NextState<GameState>>,
    mut exit: EventWriter<AppExit>,
) {
    for (interaction, mut color, is_play_button, is_quit_button) in &mut interaction_query {
        match interaction {
            Interaction::Pressed => {
                if is_play_button {
                    game_state.set(GameState::Loading)
                }

                if is_quit_button {
                    exit.send(AppExit);
                }
            }
            Interaction::Hovered => {
                color.0 = BUTTON_COLOR_HOVERED;
            }
            Interaction::None => {
                color.0 = BUTTON_COLOR;
            }
        }
    }
}
