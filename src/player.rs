use bevy::prelude::*;
use bevy::utils::HashSet;
use rand::distributions::{Distribution, Standard};
use rand::Rng;

use crate::ai::Brain;
use crate::cleanup::CleanupBundle;
use crate::constants::*;
use crate::core::building::{BuildingType, Headquarters, SpawnBuildingEvent};
use crate::core::{GameState, PlayingState};
use crate::ui::popups::CreateTextPopupEvent;

pub struct RtsPlayerPlugin;

impl Plugin for RtsPlayerPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(OnEnter(GameState::InGame), setup_players)
            .add_event::<PaymentEvent>()
            .add_event::<GameOverEvent>()
            .add_systems(
                Update,
                (check_game_over_condition, handle_game_over_events)
                    .chain()
                    .run_if(in_state(GameState::InGame)),
            )
            .add_systems(
                Update,
                (increase_player_resources, handle_payments)
                    .run_if(in_state(GameState::InGame))
                    .run_if(in_state(PlayingState::Playing)),
            );
    }
}

#[derive(Component, Clone, Copy, Eq, PartialEq, Reflect)]
pub enum PlayerTeam {
    A,
    B,
}

#[derive(Component, Reflect, Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum PlayerAlignment {
    Green,
    Red,
    Blue,
}

#[derive(Reflect, Default)]
pub struct PlayerResource {
    pub amount: u32,
    pub growth_timer: Timer,
}

#[derive(Component, Reflect)]
pub struct Player {
    pub player_team: PlayerTeam,
    pub player_alignment: PlayerAlignment,
    pub resource: PlayerResource,
    pub spawn_point: Vec3,
}

#[derive(Component)]
pub struct ClientPlayer;

#[derive(Component)]
pub struct HumanPlayer;

#[derive(Component)]
pub struct AiPlayer;

impl PlayerAlignment {
    pub fn name(&self) -> &'static str {
        match self {
            PlayerAlignment::Green => "Green",
            PlayerAlignment::Red => "Red",
            PlayerAlignment::Blue => "Blue",
        }
    }

    pub fn color(&self) -> Color {
        match self {
            PlayerAlignment::Green => COLOR_GREEN_ALIGNMENT,
            PlayerAlignment::Red => COLOR_RED_ALIGNMENT,
            PlayerAlignment::Blue => COLOR_BLUE_ALIGNMENT,
        }
    }

    pub fn building_color(&self) -> Color {
        match self {
            PlayerAlignment::Green => BUILDING_COLOR_GREEN_ALIGNMENT,
            PlayerAlignment::Red => BUILDING_COLOR_RED_ALIGNMENT,
            PlayerAlignment::Blue => BUILDING_COLOR_BLUE_ALIGNMENT,
        }
    }
}

impl Distribution<PlayerAlignment> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> PlayerAlignment {
        match rng.gen_range(1..=3) {
            1 => PlayerAlignment::Green,
            2 => PlayerAlignment::Red,
            3 => PlayerAlignment::Blue,
            _ => PlayerAlignment::Green,
        }
    }
}

#[derive(Event)]
pub struct PaymentEvent {
    pub player_entity: Entity,
    pub cost: u32,
}

#[derive(Event)]
pub struct GameOverEvent {
    player: Entity,
}

fn increase_player_resources(mut player_query: Query<&mut Player>, time: Res<Time>) {
    for mut player in player_query.iter_mut() {
        if player.resource.amount < MAX_RESOURCE_AMOUNT {
            player.resource.growth_timer.tick(time.delta());
            if player.resource.growth_timer.just_finished() {
                player.resource.amount += 10;
            }
        } else if player.resource.amount > MAX_RESOURCE_AMOUNT {
            player.resource.amount = MAX_RESOURCE_AMOUNT;
        }
    }
}

fn spawn_client_player(
    commands: &mut Commands,
    building_event_writer: &mut EventWriter<SpawnBuildingEvent>,
    player_alignment: PlayerAlignment,
    player_team: PlayerTeam,
    spawn_point: Vec3,
) {
    commands.spawn((
        Player {
            player_team,
            player_alignment,
            resource: PlayerResource {
                amount: 1000,
                growth_timer: Timer::from_seconds(0.2, TimerMode::Repeating),
            },
            spawn_point,
        },
        HumanPlayer,
        ClientPlayer,
        CleanupBundle::in_game(format!("Player | {}", player_alignment.name())),
    ));

    building_event_writer.send(SpawnBuildingEvent {
        position: spawn_point,
        player_team,
        player_alignment,
        building_type: BuildingType::Headquarters,
    });
}

fn spawn_ai_player(
    commands: &mut Commands,
    building_event_writer: &mut EventWriter<SpawnBuildingEvent>,
    player_alignment: PlayerAlignment,
    player_team: PlayerTeam,
    spawn_point: Vec3,
) {
    commands.spawn((
        Player {
            player_team,
            player_alignment,
            resource: PlayerResource {
                amount: 1000,
                growth_timer: Timer::from_seconds(0.2, TimerMode::Repeating),
            },
            spawn_point,
        },
        AiPlayer,
        Brain::default(),
        CleanupBundle::in_game(format!("Computer | {}", player_alignment.name())),
    ));

    building_event_writer.send(SpawnBuildingEvent {
        position: spawn_point,
        player_team,
        player_alignment,
        building_type: BuildingType::Headquarters,
    });
}

pub fn setup_players(
    mut commands: Commands,
    mut building_event_writer: EventWriter<SpawnBuildingEvent>,
) {
    spawn_client_player(
        &mut commands,
        &mut building_event_writer,
        PlayerAlignment::Green,
        PlayerTeam::A,
        Vec3::new(250.0, 0.0, 250.0),
    );

    spawn_ai_player(
        &mut commands,
        &mut building_event_writer,
        PlayerAlignment::Red,
        PlayerTeam::B,
        Vec3::new(250.0, 0.0, 5.0),
    );

    spawn_ai_player(
        &mut commands,
        &mut building_event_writer,
        PlayerAlignment::Blue,
        PlayerTeam::B,
        Vec3::new(400.0, 0.0, 100.0),
    );
}

fn handle_payments(
    mut event_reader: EventReader<PaymentEvent>,
    mut player_query: Query<&mut Player>,
) {
    for ev in event_reader.read() {
        let mut player = player_query.get_mut(ev.player_entity).unwrap();
        player.resource.amount -= ev.cost;
    }
}

pub fn is_same_team(entity_a: Entity, entity_b: Entity, team_query: &Query<&PlayerTeam>) -> bool {
    if let Ok(team_a) = team_query.get(entity_a) {
        if let Ok(team_b) = team_query.get(entity_b) {
            return team_a == team_b;
        }
    }

    false
}

#[allow(dead_code)]
pub fn is_same_alignment(
    entity_a: Entity,
    entity_b: Entity,
    alignment_query: &Query<&PlayerAlignment>,
) -> bool {
    if let Ok(alignment_a) = alignment_query.get(entity_a) {
        if let Ok(alignment_b) = alignment_query.get(entity_b) {
            return alignment_a == alignment_b;
        }
    }

    false
}

fn check_game_over_condition(
    player_query: Query<(Entity, &Player)>,
    hq_query: Query<&Headquarters>,
    mut game_over_event: EventWriter<GameOverEvent>,
) {
    let player_count = player_query.iter().count();
    let hq_count = hq_query.iter().count();

    if player_count == hq_count || (player_count > 1 && hq_count == 0) {
        return;
    }

    let players: HashSet<_> = player_query
        .iter()
        .map(|(_, x)| x.player_alignment)
        .collect();
    let hqs: HashSet<_> = hq_query.iter().map(|x| x.0).collect();
    let players_without_hq: HashSet<_> = players.difference(&hqs).collect();

    for (entity, player) in &player_query {
        if players_without_hq.contains(&player.player_alignment) {
            game_over_event.send(GameOverEvent { player: entity });
        }
    }
}

fn handle_game_over_events(
    mut commands: Commands,
    mut events: EventReader<GameOverEvent>,
    player_query: Query<(&Player, Has<ClientPlayer>)>,
    mut next_game_state: ResMut<NextState<GameState>>,
    mut text_popup_event: EventWriter<CreateTextPopupEvent>,
) {
    for ev in events.read() {
        if let Ok((player, is_client)) = player_query.get(ev.player) {
            let line = format!("Player {:?} is out!", player.player_alignment);
            text_popup_event.send(CreateTextPopupEvent {
                text: line,
                duration: 5.0,
                is_warning: true,
                font_size: 50.0,
            });

            commands.entity(ev.player).despawn_recursive();

            if is_client {
                next_game_state.set(GameState::GameOver)
            }
        }
    }
}
