use bevy::app::AppExit;
use bevy::prelude::*;

use crate::audio::ErrorSoundEvent;
use crate::constants::{BUTTON_COLOR, BUTTON_COLOR_HOVERED};
use crate::core::building::{Building, BuildingType, Construction, PlaceBuildingEvent};
use crate::core::unit::{spawn_boss, spawn_random_unit, SpawnUnitEvent, UnitType};
use crate::core::{find_position_around_building, DespawnGameObjectsEvent, GameState, UserControl};
use crate::input::mouse::{MouseMode, MouseState};
use crate::map::TimeOfDay;
use crate::player::{ClientPlayer, PaymentEvent, Player, PlayerAlignment};
use crate::ui::popups::CreateTextPopupEvent;
use crate::ui::tooltips::{HideTooltipEvent, ShowTooltipEvent};
use crate::ui::UiAssets;

#[derive(Component, Reflect)]
pub struct PanelButton {
    pub btn_type: ButtonType,
    #[reflect(ignore)]
    pub text: &'static str,
    #[reflect(ignore)]
    pub popup_text: &'static str,
    pub tooltip_text: String,
}

#[derive(Component, Reflect)]
pub struct DevButton {
    pub btn_type: DevButtonType,
    #[reflect(ignore)]
    pub text: &'static str,
    #[reflect(ignore)]
    pub popup_text: &'static str,
    pub tooltip_text: String,
}

#[derive(Reflect)]
pub enum ButtonType {
    Building(BuildingType),
    Unit(UnitType),
    QuitToMenu,
    QuitGame,
}

#[derive(Reflect)]
pub enum DevButtonType {
    SpawnUnit,
    SpawnBoss,
    SpawnManyUnits,
    SelectAll,
    DespawnAll,
    ToggleDayNight,
}

#[derive(Component)]
pub struct TopButton;

#[derive(Component)]
pub struct BottomButton;

pub fn use_buttons(
    mouse_mode_state: Res<State<MouseMode>>,
    mouse_state: Res<MouseState>,
    mut next_state: ResMut<NextState<MouseMode>>,
    mut ev_text_popup: EventWriter<CreateTextPopupEvent>,
    mut ev_spawn_unit: EventWriter<SpawnUnitEvent>,
    mut ev_show_tooltip: EventWriter<ShowTooltipEvent>,
    mut ev_hide_tooltip: EventWriter<HideTooltipEvent>,
    mut ev_payment: EventWriter<PaymentEvent>,
    mut error_sound_event_writer: EventWriter<ErrorSoundEvent>,
    mut place_building_event_writer: EventWriter<PlaceBuildingEvent>,
    mut interaction_query: Query<
        (&Interaction, &PanelButton, &mut BackgroundColor),
        (Changed<Interaction>, With<TopButton>, Without<BottomButton>),
    >,
    barracks_query: Query<(&Building, &Transform, &PlayerAlignment), Without<Construction>>,
    client_player_query: Query<(Entity, &Player), With<ClientPlayer>>,
) {
    if mouse_state.is_flycam_on {
        return;
    }

    for (interaction, btn, mut ui_color) in interaction_query.iter_mut() {
        match *interaction {
            Interaction::Pressed => match btn.btn_type {
                ButtonType::Building(building_type) => {
                    if mouse_mode_state.get() == &MouseMode::PlacingBuilding {
                        ev_text_popup.send(CreateTextPopupEvent {
                            text: "Already placing a building".to_string(),
                            is_warning: true,
                            ..default()
                        });
                        error_sound_event_writer.send(ErrorSoundEvent);
                        return;
                    }

                    let (_player_entity, client_player) = client_player_query.single();
                    if client_player.resource.amount < building_type.cost() {
                        ev_text_popup.send(CreateTextPopupEvent {
                            text: "Not enough resources".to_string(),
                            is_warning: true,
                            ..default()
                        });
                        error_sound_event_writer.send(ErrorSoundEvent);
                        return;
                    }

                    ev_text_popup.send(CreateTextPopupEvent {
                        text: btn.popup_text.to_string(),
                        ..default()
                    });

                    next_state.set(MouseMode::PlacingBuilding);
                    place_building_event_writer.send(PlaceBuildingEvent {
                        player_alignment: client_player.player_alignment,
                        building_type,
                    });
                }
                ButtonType::Unit(unit_type) => {
                    let (player_entity, client_player) = client_player_query.single();
                    if client_player.resource.amount < unit_type.cost() {
                        ev_text_popup.send(CreateTextPopupEvent {
                            text: "Not enough resources".to_string(),
                            is_warning: true,
                            ..default()
                        });
                        error_sound_event_writer.send(ErrorSoundEvent);
                        return;
                    }

                    let maybe_barracks =
                        barracks_query
                            .iter()
                            .find(|(building, _transform, player_alignment)| {
                                *player_alignment == &client_player.player_alignment
                                    && ***building == BuildingType::Barracks
                            });

                    match maybe_barracks {
                        None => {
                            ev_text_popup.send(CreateTextPopupEvent {
                                text: "No barracks found. Build one first!".to_string(),
                                is_warning: true,
                                ..default()
                            });
                            error_sound_event_writer.send(ErrorSoundEvent);
                        }
                        Some((_building, transform, _player_alignment)) => {
                            let position = find_position_around_building(
                                transform,
                                unit_type.purchase_requirement().size(),
                            );

                            ev_spawn_unit.send(SpawnUnitEvent {
                                unit_type,
                                position,
                                player_alignment: client_player.player_alignment,
                                player_team: client_player.player_team,
                                is_controlled: true,
                                health: unit_type.health(),
                                target: Some(position),
                            });

                            ev_payment.send(PaymentEvent {
                                player_entity,
                                cost: unit_type.cost(),
                            });
                        }
                    }
                }
                _ => {}
            },
            Interaction::Hovered => {
                ui_color.0 = BUTTON_COLOR_HOVERED;
                ev_show_tooltip.send(ShowTooltipEvent {
                    text: btn.tooltip_text.clone(),
                })
            }
            Interaction::None => {
                ui_color.0 = BUTTON_COLOR;
                ev_hide_tooltip.send(HideTooltipEvent);
            }
        }
    }
}

pub fn use_bottom_buttons(
    mouse_state: Res<MouseState>,
    mut next_game_state: ResMut<NextState<GameState>>,
    mut ev_show_tooltip: EventWriter<ShowTooltipEvent>,
    mut ev_hide_tooltip: EventWriter<HideTooltipEvent>,
    mut interaction_query: Query<
        (&Interaction, &PanelButton, &mut BackgroundColor),
        (Changed<Interaction>, With<BottomButton>, Without<TopButton>),
    >,
    mut exit: EventWriter<AppExit>,
) {
    if mouse_state.is_flycam_on {
        return;
    }

    for (interaction, btn, mut ui_color) in interaction_query.iter_mut() {
        match *interaction {
            Interaction::Pressed => match btn.btn_type {
                ButtonType::QuitToMenu => {
                    next_game_state.set(GameState::MainMenu);
                }
                ButtonType::QuitGame => {
                    exit.send(AppExit);
                }
                _ => {}
            },
            Interaction::Hovered => {
                ui_color.0 = BUTTON_COLOR_HOVERED;
                ev_show_tooltip.send(ShowTooltipEvent {
                    text: btn.tooltip_text.clone(),
                })
            }
            Interaction::None => {
                ui_color.0 = BUTTON_COLOR;
                ev_hide_tooltip.send(HideTooltipEvent);
            }
        }
    }
}

pub fn use_dev_buttons(
    mouse_state: Res<MouseState>,
    mut ev_text_popup: EventWriter<CreateTextPopupEvent>,
    mut ev_spawn_unit: EventWriter<SpawnUnitEvent>,
    mut ev_show_tooltip: EventWriter<ShowTooltipEvent>,
    mut ev_hide_tooltip: EventWriter<HideTooltipEvent>,
    mut ev_despawn: EventWriter<DespawnGameObjectsEvent>,
    mut user_control_query: Query<&mut UserControl>,
    mut interaction_query: Query<
        (&Interaction, &DevButton, &mut BackgroundColor),
        Changed<Interaction>,
    >,
    player_query: Query<&Player>,
    mut time_of_day: ResMut<TimeOfDay>,
) {
    if mouse_state.is_flycam_on {
        return;
    }

    for (interaction, btn, mut ui_color) in interaction_query.iter_mut() {
        match *interaction {
            Interaction::Pressed => {
                ev_text_popup.send(CreateTextPopupEvent {
                    text: btn.popup_text.to_string(),
                    ..default()
                });

                match btn.btn_type {
                    DevButtonType::SpawnUnit => {
                        spawn_random_unit(&mut ev_spawn_unit, &player_query)
                    }
                    DevButtonType::SpawnBoss => spawn_boss(&mut ev_spawn_unit),
                    DevButtonType::SpawnManyUnits => {
                        for _ in 0..100 {
                            spawn_random_unit(&mut ev_spawn_unit, &player_query);
                        }
                    }
                    DevButtonType::DespawnAll => {
                        ev_despawn.send(DespawnGameObjectsEvent);
                    }
                    DevButtonType::SelectAll => {
                        for mut user_control in user_control_query.iter_mut() {
                            user_control.selected = true;
                        }
                    }
                    DevButtonType::ToggleDayNight => {
                        *time_of_day = match *time_of_day {
                            TimeOfDay::Day => TimeOfDay::Night,
                            TimeOfDay::Night => TimeOfDay::Day,
                        }
                    }
                }
            }
            Interaction::Hovered => {
                ui_color.0 = BUTTON_COLOR_HOVERED;
                ev_show_tooltip.send(ShowTooltipEvent {
                    text: btn.tooltip_text.clone(),
                })
            }
            Interaction::None => {
                ui_color.0 = BUTTON_COLOR;
                ev_hide_tooltip.send(HideTooltipEvent);
            }
        }
    }
}

fn spawn_button(
    commands: &mut Commands,
    ui_assets: &UiAssets,
    text: &'static str,
    text_color: Color,
) -> Entity {
    let button_text = commands
        .spawn(TextBundle {
            text: Text::from_section(
                text,
                TextStyle {
                    font: ui_assets.font.clone(),
                    font_size: 20.0,
                    color: text_color,
                },
            ),
            ..default()
        })
        .id();

    commands
        .spawn((
            ButtonBundle {
                style: Style {
                    width: Val::Percent(100.0),
                    height: Val::Px(25.0),
                    justify_content: JustifyContent::Center,
                    align_items: AlignItems::Center,
                    ..default()
                },
                background_color: BUTTON_COLOR.into(),
                ..default()
            },
            Name::new(text),
        ))
        .add_child(button_text)
        .id()
}

pub fn create_ui_button(
    commands: &mut Commands,
    ui_assets: &UiAssets,
    btn_type: ButtonType,
) -> Entity {
    // need review of this approach.
    // could either hardcode static strings here,
    // or use String instead everywhere, to be able to format these (for some reason)
    let text = match btn_type {
        ButtonType::Building(building_type) => match building_type {
            BuildingType::Headquarters => "Build Headquarters",
            BuildingType::Barracks => "Build Barracks",
            BuildingType::Gatherer => "Build Gatherer",
            BuildingType::LaserTower => "Build Laser Tower",
        },
        ButtonType::Unit(unit_type) => match unit_type {
            UnitType::Soldier => "Buy Soldier",
            UnitType::RocketDude => "Buy Rocket dude",
            UnitType::Boss => "Buy Boss",
        },
        ButtonType::QuitToMenu => "Quit to menu",
        ButtonType::QuitGame => "Quit game",
    };

    let popup_text = match btn_type {
        ButtonType::Building(building_type) => match building_type {
            BuildingType::Headquarters => "Constructing Headquarters",
            BuildingType::Barracks => "Constructing Barracks",
            BuildingType::Gatherer => "Constructing Gatherer",
            BuildingType::LaserTower => "Constructing Laser Tower",
        },
        ButtonType::Unit(unit_type) => match unit_type {
            UnitType::Soldier => "Buying Soldier",
            UnitType::RocketDude => "Buying Rocket dude",
            UnitType::Boss => "Buying Boss",
        },
        ButtonType::QuitToMenu => "Quitting to menu",
        ButtonType::QuitGame => "Bye!",
    };

    let tooltip_text = match btn_type {
        ButtonType::Building(building_type) => {
            let header = format!(
                "Build {}\nCost: {}\n\n",
                building_type.name(),
                building_type.cost()
            );
            match building_type {
                BuildingType::Headquarters => format!("{}The main building.", header),
                BuildingType::Barracks => format!("{}Trains Soldiers.", header),
                BuildingType::Gatherer => format!("{}Collects resource over time.", header),
                BuildingType::LaserTower => format!("{}Eliminates hostiles.", header),
            }
        }
        ButtonType::Unit(unit_type) => {
            let header = format!("Buy {}\nCost: {}\n\n", unit_type.name(), unit_type.cost());
            match unit_type {
                UnitType::Soldier => format!("{}Unit with basic fighting capabilities", header),
                UnitType::Boss => format!("{}FAT GREEN BOSS", header),
                UnitType::RocketDude => format!(
                    "{}Unit that shoots powerful rocket projectiles at enemies",
                    header
                ),
            }
        }
        ButtonType::QuitToMenu => "Quit to menu".to_string(),
        ButtonType::QuitGame => "Quit game".to_string(),
    };

    let button = spawn_button(commands, ui_assets, text, Color::rgb(1.0, 1.0, 1.0));

    match btn_type {
        ButtonType::QuitToMenu | ButtonType::QuitGame => {
            commands.entity(button).insert(BottomButton)
        }
        _ => commands.entity(button).insert(TopButton),
    };

    commands
        .entity(button)
        .insert(PanelButton {
            btn_type,
            text,
            popup_text,
            tooltip_text,
        })
        .id()
}

pub fn create_dev_button(
    commands: &mut Commands,
    ui_assets: &UiAssets,
    btn_type: DevButtonType,
) -> Entity {
    let text = match btn_type {
        DevButtonType::SpawnUnit => "Spawn Unit",
        DevButtonType::SpawnBoss => "Spawn Boss",
        DevButtonType::SpawnManyUnits => "Spawn 100 Units",
        DevButtonType::DespawnAll => "Despawn All",
        DevButtonType::SelectAll => "Select All",
        DevButtonType::ToggleDayNight => "Toggle Day/Night",
    };

    let popup_text = match btn_type {
        DevButtonType::SpawnUnit => "Spawning a random unit",
        DevButtonType::SpawnBoss => "Spawning a fat green boss",
        DevButtonType::SpawnManyUnits => "Spawning 100 units",
        DevButtonType::DespawnAll => "Despawning everything",
        DevButtonType::SelectAll => "Selecting all selectable objects",
        DevButtonType::ToggleDayNight => "Toggling day/night",
    };

    let tooltip_text = match btn_type {
        DevButtonType::SpawnUnit => "Spawn a unit of a random alignment.".to_string(),
        DevButtonType::SpawnBoss => "Spawn a fat green boss.".to_string(),
        DevButtonType::SpawnManyUnits => {
            "Spawn 100 units, alignment is chosen randomly.".to_string()
        }
        DevButtonType::DespawnAll => "Despawn all objects in the game.".to_string(),
        DevButtonType::SelectAll => {
            "Select all selectable objects (units and buildings).".to_string()
        }
        DevButtonType::ToggleDayNight => "Toggle time of day".to_string(),
    };

    let button = spawn_button(commands, ui_assets, text, Color::YELLOW);
    commands
        .entity(button)
        .insert(DevButton {
            btn_type,
            text,
            popup_text,
            tooltip_text,
        })
        .id()
}
