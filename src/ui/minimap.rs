use std::f32::consts::PI;

use bevy::pbr::{NotShadowCaster, NotShadowReceiver};
use bevy::render::mesh::{Indices, PrimitiveTopology, VertexAttributeValues};
use bevy::{
    prelude::*,
    render::{
        camera::RenderTarget,
        render_resource::{
            Extent3d, TextureDescriptor, TextureDimension, TextureFormat, TextureUsages,
        },
        view::RenderLayers,
    },
};

use crate::camera::MainCamera;
use crate::cleanup::CleanupBundle;
use crate::core::GameState;
use crate::graphics::animations::camera_transform_animator;
use crate::graphics::materials::CommonMaterials;
use crate::ui::UI_PANEL_WIDTH;

pub struct MinimapPlugin;

impl Plugin for MinimapPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<MinimapClickEvent>()
            .add_systems(
                OnEnter(GameState::InGame),
                (spawn_minimap, spawn_camera_projection),
            )
            .add_systems(
                Update,
                (
                    update_camera_projection,
                    zoom_minimap_camera,
                    handle_minimap_click_events,
                )
                    .run_if(in_state(GameState::InGame)),
            );
    }
}

pub const BORDER_WIDTH: f32 = 10.0;
const ZOOM_STEP: f32 = 2.5;
pub const MINIMAP_CAMERA_OFFSET: Vec2 = Vec2 { x: 200.0, y: 100.0 };
const MINIMAP_CAMERA_HEIGHT: f32 = 650.0;

#[derive(Component)]
struct CameraProjection;

#[derive(Component)]
pub struct MinimapCamera;

#[derive(Event)]
pub struct MinimapClickEvent {
    world_pos: Vec2,
}

impl MinimapClickEvent {
    pub fn new(world_pos: Vec2) -> Self {
        MinimapClickEvent { world_pos }
    }
}

fn spawn_minimap(mut commands: Commands, mut images: ResMut<Assets<Image>>) {
    let size = Extent3d {
        width: 512,
        height: 512,
        ..default()
    };

    // This is the texture that will be rendered to.
    let mut image = Image {
        texture_descriptor: TextureDescriptor {
            label: None,
            size,
            dimension: TextureDimension::D2,
            format: TextureFormat::Bgra8UnormSrgb,
            mip_level_count: 1,
            sample_count: 1,
            usage: TextureUsages::TEXTURE_BINDING
                | TextureUsages::COPY_DST
                | TextureUsages::RENDER_ATTACHMENT,
            view_formats: &[],
        },
        ..default()
    };

    // fill image.data with zeroes
    image.resize(size);
    let image_handle = images.add(image);

    commands.spawn((
        Camera3dBundle {
            camera: Camera {
                target: RenderTarget::Image(image_handle.clone()),
                ..default()
            },
            projection: Projection::Orthographic(Default::default()),
            transform: Transform::from_xyz(
                MINIMAP_CAMERA_OFFSET.x,
                MINIMAP_CAMERA_HEIGHT,
                MINIMAP_CAMERA_OFFSET.y,
            )
            .with_rotation(Quat::from_rotation_x(-PI / 2.0)),
            ..default()
        },
        MinimapCamera,
        UiCameraConfig { show_ui: false },
        RenderLayers::layer(1),
        CleanupBundle::in_game("Minimap camera"),
    ));

    let ui_image = commands
        .spawn((
            ImageBundle {
                image: UiImage::new(image_handle),
                style: Style {
                    width: Val::VMin(30.0),
                    height: Val::VMin(30.0),
                    ..default()
                },
                ..default()
            },
            Name::new("Minimap image"),
        ))
        .id();

    let node = commands
        .spawn((
            NodeBundle {
                background_color: Color::TEAL.into(),
                style: Style {
                    padding: UiRect::all(Val::Px(BORDER_WIDTH)),
                    ..default()
                },
                ..default()
            },
            Name::new("Minimap node"),
        ))
        .add_child(ui_image)
        .id();

    commands
        .spawn((
            NodeBundle {
                style: Style {
                    position_type: PositionType::Absolute,
                    margin: UiRect {
                        left: Val::Px(0.0),
                        right: Val::Auto,
                        top: Val::Auto,
                        bottom: Val::Px(0.0),
                    },
                    ..default()
                },
                ..default()
            },
            CleanupBundle::in_game("Minimap UI"),
        ))
        .add_child(node);
}

#[derive(Debug, Copy, Clone)]
struct Trapezoid {
    top_left_x: f32,
    bottom_left_x: f32,
    bottom_right_x: f32,
    top_right_x: f32,

    top_left_z: f32,
    bottom_left_z: f32,
    bottom_right_z: f32,
    top_right_z: f32,
}

impl Trapezoid {
    pub fn from_corners(
        top_left: Vec2,
        bottom_left: Vec2,
        bottom_right: Vec2,
        top_right: Vec2,
    ) -> Trapezoid {
        Trapezoid {
            top_left_x: top_left.x,
            bottom_left_x: bottom_left.x,
            bottom_right_x: bottom_right.x,
            top_right_x: top_right.x,
            top_left_z: top_left.y,
            bottom_left_z: bottom_left.y,
            bottom_right_z: bottom_right.y,
            top_right_z: top_right.y,
        }
    }
}

#[rustfmt::skip]
impl From<Trapezoid> for Mesh {
    fn from(sp: Trapezoid) -> Self {
        let vertices = &[
            ([sp.top_left_x, 0.0, sp.top_left_z], [0., 1.0, 0.], [0., 0.]),
            ([sp.bottom_left_x, 0.0, sp.bottom_left_z], [0., 1.0, 0.], [0., 1.0]),
            ([sp.bottom_right_x, 0.0, sp.bottom_right_z], [0., 1.0, 0.], [1.0, 1.0]),
            ([sp.top_right_x, 0.0, sp.top_right_z], [0., 1.0, 0.], [1.0, 0.]),
        ];

        let positions: Vec<_> = vertices.iter().map(|(p, _, _)| *p).collect();
        let normals: Vec<_> = vertices.iter().map(|(_, n, _)| *n).collect();
        let uvs: Vec<_> = vertices.iter().map(|(_, _, uv)| *uv).collect();

        let indices = Indices::U32(vec![0, 1, 2, 2, 3, 0]);

        Mesh::new(PrimitiveTopology::TriangleList)
            .with_inserted_attribute(Mesh::ATTRIBUTE_POSITION, positions)
            .with_inserted_attribute(Mesh::ATTRIBUTE_NORMAL, normals)
            .with_inserted_attribute(Mesh::ATTRIBUTE_UV_0, uvs)
            .with_indices(Some(indices))
    }
}

fn spawn_camera_projection(
    window_query: Query<&Window>,
    mut commands: Commands,
    camera_query: Query<(&Camera, &GlobalTransform), With<MainCamera>>,
    mut meshes: ResMut<Assets<Mesh>>,
    common_materials: Res<CommonMaterials>,
) {
    let window = window_query.single();
    let (camera, camera_transform) = camera_query.single();

    let corners = [
        Vec2::new(0.0, 0.0),
        Vec2::new(0.0, window.height()),
        Vec2::new(window.width() - UI_PANEL_WIDTH, window.height()),
        Vec2::new(window.width() - UI_PANEL_WIDTH, 0.0),
    ];

    let mut positions: Vec<Vec2> = Vec::new();
    for corner in corners.iter() {
        let world_pos = camera
            .viewport_to_world(camera_transform, *corner)
            .and_then(|ray| {
                ray.intersect_plane(Vec3::ZERO, Vec3::Y)
                    .map(|distance| ray.get_point(distance).xz())
            });

        positions.push(world_pos.unwrap());
    }

    let shape = Trapezoid::from_corners(positions[0], positions[1], positions[2], positions[3]);

    commands.spawn((
        MaterialMeshBundle {
            mesh: meshes.add(Mesh::from(shape)),
            material: common_materials.camera_projection.clone(),
            transform: Transform::from_xyz(0.0, 100.0, 0.0),
            ..default()
        },
        CameraProjection,
        RenderLayers::layer(1),
        NotShadowCaster,
        NotShadowReceiver,
        CleanupBundle::in_game("Minimap camera projection"),
    ));
}

fn update_camera_projection(
    camera_query: Query<
        (&Camera, &GlobalTransform),
        (
            Or<(Changed<GlobalTransform>, Changed<Projection>)>,
            With<MainCamera>,
        ),
    >,
    window_query: Query<&Window>,
    mesh_query: Query<&Handle<Mesh>, (With<CameraProjection>, Without<MainCamera>)>,
    mut meshes: ResMut<Assets<Mesh>>,
) {
    if let Ok((camera, global_transform)) = camera_query.get_single() {
        let window = window_query.single();
        let mesh_handle = mesh_query.single();
        let mesh = meshes.get_mut(mesh_handle).unwrap();

        let corners = [
            Vec2::new(0.0, 0.0),
            Vec2::new(0.0, window.height()),
            Vec2::new(window.width() - UI_PANEL_WIDTH, window.height()),
            Vec2::new(window.width() - UI_PANEL_WIDTH, 0.0),
        ];

        let mut p: Vec<Vec2> = Vec::new();
        for corner in corners.iter() {
            let world_pos = camera
                .viewport_to_world(global_transform, *corner)
                .and_then(|ray| {
                    ray.intersect_plane(Vec3::ZERO, Vec3::Y)
                        .map(|distance| ray.get_point(distance).xz())
                });

            if let Some(pos) = world_pos {
                p.push(pos);
            } else {
                return;
            }
        }

        let pos_attribute = mesh.attribute_mut(Mesh::ATTRIBUTE_POSITION).unwrap();
        if let VertexAttributeValues::Float32x3(ref mut pos_attribute) = pos_attribute {
            *pos_attribute = vec![
                [p[0].x, 0.0, p[0].y],
                [p[1].x, 0.0, p[1].y],
                [p[2].x, 0.0, p[2].y],
                [p[3].x, 0.0, p[3].y],
            ];
        }
    }
}

fn zoom_minimap_camera(
    mut camera_query: Query<&mut Projection, With<MinimapCamera>>,
    keyboard: Res<Input<KeyCode>>,
    time: Res<Time>,
) {
    if !keyboard.any_pressed([KeyCode::NumpadAdd, KeyCode::NumpadSubtract, KeyCode::Home]) {
        return;
    }

    let Projection::Orthographic(proj) = camera_query.single_mut().into_inner() else {
        return;
    };

    if keyboard.pressed(KeyCode::NumpadAdd) {
        proj.scale /= ZOOM_STEP.powf(time.delta_seconds());
    }

    if keyboard.pressed(KeyCode::NumpadSubtract) {
        proj.scale *= ZOOM_STEP.powf(time.delta_seconds());
    }

    if keyboard.pressed(KeyCode::Home) {
        proj.scale = 1.0;
    }

    proj.scale = proj.scale.clamp(0.3, 4.3);
}

fn handle_minimap_click_events(
    mut commands: Commands,
    mut events: EventReader<MinimapClickEvent>,
    camera_query: Query<(Entity, &Transform), With<MainCamera>>,
) {
    for ev in events.read() {
        let (camera, camera_transform) = camera_query.single();

        let back = camera_transform.back();
        // I guess this only works for the default camera rotation (pitch specifically)
        let offset = Vec2::new(back.x, back.z).normalize() * 80.0;

        let mut new_transform = *camera_transform;
        new_transform.translation.x = ev.world_pos.x + offset.x;
        new_transform.translation.z = ev.world_pos.y + offset.y;

        let animator = camera_transform_animator(*camera_transform, new_transform, 80);
        commands.entity(camera).insert(animator);
    }
}
