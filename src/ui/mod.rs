use bevy::prelude::*;

use buttons::{create_ui_button, use_buttons, ButtonType};
use popups::{handle_text_popup_event, update_text_popups, CreateTextPopupEvent};
use tooltips::{
    handle_hide_tooltip_event, handle_show_tooltip_event, HideTooltipEvent, ShowTooltipEvent,
};

use crate::cleanup::CleanupBundle;
use crate::core::building::BuildingType;
use crate::core::unit::UnitType;
use crate::core::{GameObject, GameState, PlayingState};
use crate::map::TimeOfDay;
use crate::player::{ClientPlayer, Player};
use crate::ui::buttons::{create_dev_button, use_bottom_buttons, use_dev_buttons, DevButtonType};
use crate::ui::minimap::MinimapPlugin;
use crate::ui::tooltips::create_tooltip;

pub mod buttons;
pub mod minimap;
pub mod popups;
mod tooltips;

pub struct RtsUiPlugin;

pub const UI_PANEL_WIDTH: f32 = 270.0;
pub const UI_PANEL_COLOR: Color = Color::rgba(0.0, 0.0, 0.0, 1.0);
pub const UI_SUBPANEL_COLOR: Color = Color::rgba(0.15, 0.15, 0.15, 1.0);

#[derive(Resource)]
pub struct UiAssets {
    pub font: Handle<Font>,
    pub mono_font: Handle<Font>,
}

#[derive(Component, Reflect)]
pub struct PlayerResourceText;

#[derive(Component)]
pub struct UnitCountText;

#[derive(Component)]
struct PausedScreen;

#[derive(Component)]
pub struct PausedText;

impl Plugin for RtsUiPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<CreateTextPopupEvent>()
            .add_event::<ShowTooltipEvent>()
            .add_plugins(MinimapPlugin)
            .add_event::<HideTooltipEvent>()
            .add_systems(PreStartup, load_ui_assets)
            .add_systems(OnEnter(GameState::InGame), setup_ui)
            .add_systems(OnEnter(PlayingState::Paused), create_paused_screen)
            .add_systems(
                Update,
                (
                    use_buttons.run_if(in_state(PlayingState::Playing)),
                    use_dev_buttons,
                    use_bottom_buttons,
                    handle_text_popup_event,
                    handle_show_tooltip_event,
                    handle_hide_tooltip_event,
                    update_text_popups,
                    update_unit_count_text,
                    update_client_resource_text,
                )
                    .run_if(in_state(GameState::InGame)),
            );
    }
}

fn load_ui_assets(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands.insert_resource(UiAssets {
        font: asset_server.load("fonts/QuattrocentoSans-Regular.ttf"),
        mono_font: asset_server.load("fonts/digital-7 (mono).ttf"),
    });
}

fn setup_ui(mut commands: Commands, ui_assets: Res<UiAssets>) {
    let text_style = TextStyle {
        font: ui_assets.mono_font.clone(),
        font_size: 30.0,
        color: Color::rgb(0.9, 0.9, 0.9),
    };

    let text_alignment = TextAlignment::Left;

    let player_resource_text = commands
        .spawn(TextBundle {
            text: Text::from_section("1000", text_style).with_alignment(text_alignment),
            style: Style {
                position_type: PositionType::Relative,
                align_self: AlignSelf::Center,
                // bevy_ui is buggy with this font, which is why margin is needed here
                margin: UiRect {
                    bottom: Val::Px(30.0),
                    ..default()
                },
                ..default()
            },
            ..default()
        })
        .insert(Name::new("Player resource"))
        .insert(PlayerResourceText)
        .id();

    let text_style = TextStyle {
        font: ui_assets.mono_font.clone(),
        font_size: 16.0,
        color: Color::rgb(0.2, 0.9, 0.2),
    };

    let unit_count_text = commands
        .spawn(TextBundle {
            text: Text::from_section("Total units: 0", text_style).with_alignment(text_alignment),
            style: Style {
                position_type: PositionType::Relative,
                align_self: AlignSelf::Center,
                margin: UiRect {
                    top: Val::Px(15.0),
                    ..default()
                },
                ..default()
            },
            ..default()
        })
        .insert(Name::new("Unit Count Text"))
        .insert(UnitCountText)
        .id();

    let barracks_button = create_ui_button(
        &mut commands,
        &ui_assets,
        ButtonType::Building(BuildingType::Barracks),
    );

    let gatherer_button = create_ui_button(
        &mut commands,
        &ui_assets,
        ButtonType::Building(BuildingType::Gatherer),
    );

    let laser_tower_button = create_ui_button(
        &mut commands,
        &ui_assets,
        ButtonType::Building(BuildingType::LaserTower),
    );

    let buy_soldier_button = create_ui_button(
        &mut commands,
        &ui_assets,
        ButtonType::Unit(UnitType::Soldier),
    );

    let buy_rocket_dude_button = create_ui_button(
        &mut commands,
        &ui_assets,
        ButtonType::Unit(UnitType::RocketDude),
    );

    let quit_to_menu_button = create_ui_button(&mut commands, &ui_assets, ButtonType::QuitToMenu);

    let quit_game_button = create_ui_button(&mut commands, &ui_assets, ButtonType::QuitGame);

    let spawn_unit_button = create_dev_button(&mut commands, &ui_assets, DevButtonType::SpawnUnit);
    let spawn_boss_button = create_dev_button(&mut commands, &ui_assets, DevButtonType::SpawnBoss);
    let spawn_many_units_button =
        create_dev_button(&mut commands, &ui_assets, DevButtonType::SpawnManyUnits);
    let despawn_all_button =
        create_dev_button(&mut commands, &ui_assets, DevButtonType::DespawnAll);
    let select_all_button = create_dev_button(&mut commands, &ui_assets, DevButtonType::SelectAll);
    let toggle_day_night_btn =
        create_dev_button(&mut commands, &ui_assets, DevButtonType::ToggleDayNight);

    let tooltip = create_tooltip(&mut commands, &ui_assets);

    // top subpanel of the right panel
    let top_subpanel = commands
        .spawn((
            NodeBundle {
                style: Style {
                    flex_direction: FlexDirection::Column,
                    justify_content: JustifyContent::FlexStart,
                    padding: UiRect::px(15.0, 15.0, 30.0, 30.0),
                    row_gap: Val::Px(10.0),
                    ..default()
                },
                background_color: UI_SUBPANEL_COLOR.into(),
                ..default()
            },
            Name::new("UI top subpanel of right panel"),
        ))
        .push_children(&[
            player_resource_text,
            barracks_button,
            gatherer_button,
            laser_tower_button,
            buy_soldier_button,
            buy_rocket_dude_button,
            spawn_unit_button,
            spawn_boss_button,
            spawn_many_units_button,
            select_all_button,
            despawn_all_button,
            toggle_day_night_btn,
            unit_count_text,
        ])
        .id();

    // bottom subpanel of the right panel
    let bottom_subpanel = commands
        .spawn((
            NodeBundle {
                style: Style {
                    flex_direction: FlexDirection::Column,
                    justify_content: JustifyContent::FlexStart,
                    padding: UiRect::px(15.0, 15.0, 30.0, 30.0),
                    row_gap: Val::Px(10.0),
                    ..default()
                },
                background_color: UI_SUBPANEL_COLOR.into(),
                ..default()
            },
            Name::new("UI bottom subpanel of right panel"),
        ))
        .push_children(&[quit_to_menu_button, quit_game_button])
        .id();

    // right panel
    let right_panel = commands
        .spawn((
            NodeBundle {
                style: Style {
                    flex_direction: FlexDirection::Column,
                    justify_content: JustifyContent::SpaceBetween,
                    padding: UiRect::all(Val::Px(10.0)),
                    width: Val::Px(UI_PANEL_WIDTH),
                    height: Val::Percent(100.0),
                    ..default()
                },
                background_color: UI_PANEL_COLOR.into(),
                ..default()
            },
            Name::new("UI right panel"),
        ))
        .add_child(top_subpanel)
        .add_child(bottom_subpanel)
        .id();

    // root node
    commands
        .spawn((
            NodeBundle {
                style: Style {
                    width: Val::Percent(100.0),
                    height: Val::Percent(100.0),
                    flex_direction: FlexDirection::RowReverse,
                    justify_content: JustifyContent::FlexStart,
                    align_items: AlignItems::FlexEnd,
                    ..default()
                },
                ..default()
            },
            CleanupBundle::in_game("UI root node"),
        ))
        .add_child(right_panel)
        .add_child(tooltip);
}

fn update_client_resource_text(
    client_player_query: Query<&Player, With<ClientPlayer>>,
    mut player_resource_text_query: Query<&mut Text, With<PlayerResourceText>>,
) {
    if let Ok(client_player) = client_player_query.get_single() {
        if client_player.resource.growth_timer.just_finished() {
            let mut player_resource_text = player_resource_text_query.single_mut();
            player_resource_text.sections[0].value = format!("{}", client_player.resource.amount);
        }
    }
}

fn update_unit_count_text(
    unit_query: Query<&GameObject>,
    mut unit_count_text_query: Query<&mut Text, With<UnitCountText>>,
) {
    let mut unit_count = 0;
    for game_object in unit_query.iter() {
        if game_object == &GameObject::Unit {
            unit_count += 1;
        }
    }

    let mut unit_count_text = unit_count_text_query.single_mut();
    unit_count_text.sections[0].value = format!("Total units:{}", unit_count);
}

fn create_paused_screen(mut commands: Commands, time_of_day: Res<TimeOfDay>) {
    let color = match *time_of_day {
        TimeOfDay::Day => Color::BLACK,
        TimeOfDay::Night => Color::rgb(0.8, 0.8, 0.8),
    };

    let text = commands
        .spawn((
            TextBundle {
                text: Text::from_section(
                    "PAUSED",
                    TextStyle {
                        font_size: 130.0,
                        color,
                        ..default()
                    },
                ),
                ..default()
            },
            Name::new("Paused screen text"),
            PausedText,
        ))
        .id();

    commands
        .spawn((
            NodeBundle {
                style: Style {
                    position_type: PositionType::Absolute,
                    margin: UiRect::all(Val::Auto),
                    ..default()
                },
                ..default()
            },
            CleanupBundle::paused("Paused screen container"),
            PausedScreen,
        ))
        .add_child(text);
}
