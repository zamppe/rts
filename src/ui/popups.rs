use bevy::prelude::*;

use crate::cleanup::CleanupBundle;
use crate::ui::UiAssets;

#[derive(Component)]
pub struct TextPopup {
    timer: Timer,
    when_start_fading: f32,
}

#[derive(Event)]
pub struct CreateTextPopupEvent {
    pub text: String,
    pub duration: f32,
    pub is_warning: bool,
    pub font_size: f32,
}

impl Default for CreateTextPopupEvent {
    fn default() -> Self {
        CreateTextPopupEvent {
            text: "Text popup".to_string(),
            duration: 2.0,
            is_warning: false,
            font_size: 30.0,
        }
    }
}

pub fn update_text_popups(
    mut commands: Commands,
    mut query: Query<(&Parent, &mut Text, &mut TextPopup)>,
    time: Res<Time>,
) {
    for (parent_entity, mut text, mut popup) in query.iter_mut() {
        popup.timer.tick(time.delta());
        let percent_left = popup.timer.percent_left();
        if percent_left < popup.when_start_fading {
            text.sections[0]
                .style
                .color
                .set_a(percent_left / popup.when_start_fading);
        }

        if popup.timer.just_finished() {
            commands.entity(parent_entity.get()).despawn_recursive();
        }
    }
}

pub fn handle_text_popup_event(
    mut commands: Commands,
    mut ev_text_popup: EventReader<CreateTextPopupEvent>,
    ui_assets: Res<UiAssets>,
    popup_query: Query<&Parent, With<TextPopup>>,
) {
    for event in ev_text_popup.read() {
        // Destroy old popup
        for popup_parent_node in popup_query.iter() {
            commands.entity(popup_parent_node.get()).despawn_recursive();
        }

        // Create new one
        create_text_popup(
            &mut commands,
            &ui_assets,
            event.text.as_str(),
            event.duration,
            event.is_warning,
            event.font_size,
        );
    }
}

pub fn create_text_popup(
    commands: &mut Commands,
    ui_assets: &UiAssets,
    text: &str,
    duration: f32,
    is_warning: bool,
    font_size: f32,
) {
    let text_style = TextStyle {
        font: ui_assets.font.clone(),
        font_size,
        color: match is_warning {
            true => Color::RED,
            false => Color::rgb(0.9, 0.9, 0.9),
        },
    };

    let text_alignment = TextAlignment::Left;

    let node_style = Style {
        position_type: PositionType::Absolute,
        flex_direction: FlexDirection::Column,
        justify_content: JustifyContent::FlexEnd,
        width: Val::Percent(100.0),
        height: Val::Percent(100.0),
        ..default()
    };

    let style = Style {
        position_type: PositionType::Relative,
        align_self: AlignSelf::Center,
        margin: UiRect {
            left: Val::Auto,
            right: Val::Auto,
            top: Val::Percent(1.0),
            bottom: Val::Auto,
        },
        ..default()
    };

    commands
        .spawn((
            NodeBundle {
                style: node_style,
                ..default()
            },
            CleanupBundle::in_game("TextPopup node"),
        ))
        .with_children(|parent| {
            parent.spawn((
                TextBundle {
                    text: Text::from_section(text, text_style).with_alignment(text_alignment),
                    style,
                    ..default()
                },
                Name::new("TextPopup"),
                TextPopup {
                    timer: Timer::from_seconds(duration, TimerMode::Once),
                    when_start_fading: 0.3,
                },
            ));
        });
}
