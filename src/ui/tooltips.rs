use bevy::prelude::*;

use crate::ui::UiAssets;

#[derive(Component)]
pub struct Tooltip;

#[derive(Event)]
pub struct ShowTooltipEvent {
    pub text: String,
}

#[derive(Event)]
pub struct HideTooltipEvent;

pub fn handle_show_tooltip_event(
    mut ev_show_tooltip: EventReader<ShowTooltipEvent>,
    mut text_query: Query<&mut Text, With<Tooltip>>,
    mut visibility_query: Query<&mut Visibility, With<Tooltip>>,
) {
    for ev in ev_show_tooltip.read() {
        let mut text = text_query.single_mut();
        text.sections[0].value = ev.text.clone();
        for mut vis in visibility_query.iter_mut() {
            *vis = Visibility::Visible;
        }
    }
}

pub fn handle_hide_tooltip_event(
    mut ev_hide_tooltip: EventReader<HideTooltipEvent>,
    mut visibility_query: Query<&mut Visibility, With<Tooltip>>,
) {
    for _ev in ev_hide_tooltip.read() {
        for mut vis in visibility_query.iter_mut() {
            *vis = Visibility::Hidden;
        }
    }
}

pub fn create_tooltip(commands: &mut Commands, ui_assets: &UiAssets) -> Entity {
    let tooltip_text = commands
        .spawn((
            TextBundle {
                text: Text::from_section(
                    "Testing tooltips",
                    TextStyle {
                        font: ui_assets.font.clone(),
                        font_size: 16.0,
                        color: Color::rgb(0.9, 0.9, 0.9),
                    },
                ),
                style: Style {
                    max_width: Val::Px(210.0),
                    max_height: Val::Auto,
                    ..default()
                },
                visibility: Visibility::Hidden,
                ..default()
            },
            Name::new("Tooltip Text"),
            Tooltip,
        ))
        .id();

    commands
        .spawn((
            NodeBundle {
                style: Style {
                    width: Val::Px(220.0),
                    height: Val::Auto,
                    margin: UiRect {
                        right: Val::Px(5.0),
                        bottom: Val::Px(5.0),
                        ..default()
                    },
                    padding: UiRect::all(Val::Px(5.0)),
                    align_items: AlignItems::FlexStart,
                    ..default()
                },
                background_color: Color::rgba(0.1, 0.14, 0.25, 0.4).into(),
                visibility: Visibility::Hidden,
                ..default()
            },
            Name::new("Tooltip"),
            Tooltip,
        ))
        .add_child(tooltip_text)
        .id()
}
